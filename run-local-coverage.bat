echo off
cls

set COVERAGE_FILTERS=+[Core.ApplicationServices]* -[Core.ApplicationServices]Core.ApplicationServices.ExpressionHelpers +[Core.DomainModel]* -[Core.DomainModel]ApplicationUser +[Core.DomainServices]* +[Infrastructure.DataAccess]* +[Infrastructure.Dawa]* +[Presentation.Web]* -[Presentation.Web]Presentation.Web.App_Start* -[Presentation.Web]Presentation.Web.MvcApplication  -[Presentation.Web]Presentation.Web.Startup -[Presentation.Web]Presentation.Web.Models.Search.ResultsViewModel -[Presentation.Web]Presentation.Web.Helpers.AdHelper -[Presentation.Web]Presentation.Web.Helpers.AppSettings -[Presentation.Web]Presentation.Web.Controllers.BaseController  -[Presentation.Web]Presentation.Web.Models.CustomDateTime
set TEST_ASSEMBLIES=UnitTests\bin\Debug\UnitTests.dll IntegrationTests\bin\Debug\IntegrationTests.dll Core.ApplicationServices.UnitTests\bin\Debug\Core.ApplicationServices.UnitTests.dll

packages\OpenCover.4.6.166\tools\OpenCover.Console.exe -target:run-tests.bat -register:user -filter:"%COVERAGE_FILTERS%"
packages\ReportGenerator.2.3.1.0\tools\ReportGenerator.exe -reports:results.xml -targetdir:coverage

start .\coverage\index.htm
