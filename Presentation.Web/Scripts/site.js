﻿$(function () {

    // Initialize dataTables plugin on element.
    // Disable sorting on individual coloumns by adding class "no-sort" on the <th> element.
    // Function searches for specific class name in order to differentiate between "project" datatable and others.
    // To add other custom initializations, add class name to DOM datatable in view and add check for the corresponding name here

    function cbaInitializeDatatable(dataTable) {

        if (dataTable.length == 0)
            return;

        if (dataTable[0].classList.contains("defaultSearchProjectStatus")) {
            dataTable.DataTable(
            {
                order: [1, 'asc'],
                pagingType: "full_numbers",
                "iDisplayLength": 200,
                language: // Danish language
                {
                    url: "/Content/DataTables/language/DanishLangDataTable.json"
                }
            }).draw();
        } else {
            dataTable.DataTable(
            {
                order: [1, 'asc'],
                pagingType: "full_numbers",
                "iDisplayLength": 200,
                language: // Danish language
                {
                    url: "/Content/DataTables/language/DanishLangDataTable.json"
                }
            });
        }
    }

    // Focus first input
    $('.form-control:first').focus();

    // AWS urls
    var searchField = $("#address-search");
    var autoExpand = (searchField.attr('data-auto-expand') === '1');
    var onlyRoadNames = (searchField.attr('data-only-road-names') === '1');

    var insideAarhusUrl;
    var wholeCountryUrl;
    var addressIdUrl;
    var urlToUse;

    if (onlyRoadNames) {
        insideAarhusUrl = "http://dawa.aws.dk/vejnavne/autocomplete?kommunekode=751";
        wholeCountryUrl = "http://dawa.aws.dk/vejnavne/autocomplete?";
        addressIdUrl = "http://dawa.aws.dk/vejnavne/";
    } else {
        insideAarhusUrl = "http://dawa.aws.dk/adresser/autocomplete?kommunekode=751";
        wholeCountryUrl = "http://dawa.aws.dk/adresser/autocomplete?";
        addressIdUrl = "http://dawa.aws.dk/adresser/";
    }
    urlToUse = insideAarhusUrl;
    var await = false;

    setupAws(insideAarhusUrl);

    // If address search select list holds elements the address text is set and select2 attached
    searchField.find('option').each(function() {
        var option = $(this);
        var uid = option.val();

        if (uid !== null && uid !== '') {
            await = true;

            $.get(addressIdUrl + uid + "?srid=25832", null, function (data) {
                option.html(data.adressebetegnelse);
                    setupAws(insideAarhusUrl);
                });
        }
    });

    // Do not invoke select2 before default options has the correct
    // content (inner html). select2 will build some html from the
    // <select>, but only use values that are already present when processing.
    // If there is a default value, the it's address name must be
    // fetched from webservice first, otherwise it is random whether or not
    // it will be visible to the user (depending on the timing).
    if (!await)
        setupAws(insideAarhusUrl);

    // Setup select2 autocomplete with AWS data
    function setupAws(urlArg) {
        searchField.select2({
            ajax: {
                url: urlArg,
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term // search term
                    };
                },
                processResults: function (data) {
                    var array = [];

                    data.forEach(function (d) {
                        if (onlyRoadNames)
                            array.push({ id: d.tekst, text: d.tekst });
                        else
                            array.push({ id: d.adresse.id, text: d.tekst });
                    });

                    return {results: array};
                },
                cache: true
            },
            minimumInputLength: 1,
            escapeMarkup: function (markup) {
                return markup;
            },
            language: "da",
            allowClear: true,
            placeholder: ""
        });

        if (autoExpand)
            searchField.select2('open');
    }

    var containsAddress = false;

    // Select all address ids on page and replace the id with the address text from AWS
    // Usage:
    //   Set CSS class "aws-address-id" on element to hold address name
    //   Add data attribute "aws-id" on element with the AWS id
    //   Example: <p class="aws-address-id" data-aws-id="someAwsIdString">Temperary text removed when load is finished</p>

    var awsAddressCells = $(".aws-address-id");
    var length = awsAddressCells.length; // used for finding last iteration of each loop

    var dataTable = $('#data-table');

    awsAddressCells.each(function (index, element) {
        var id = $(this).data("aws-id");
        $.get(addressIdUrl + id, null, function (data) {
            element.innerHTML = data.adressebetegnelse;
            if (index === length-1) // dataTables must be initialized after dynamic manipulation of table for it to work properly
                cbaInitializeDatatable(dataTable);
        });
        containsAddress = true;
    });

    if (!containsAddress) // if not address, just init datatable
        cbaInitializeDatatable(dataTable);

    // On selection change the AWS url is changed
    $('#aarhusAddresses').change(function () {
        if ($(this).is(":checked")) {
            urlToUse = insideAarhusUrl;
        } else {
            urlToUse = wholeCountryUrl;
        }

        setupAws(urlToUse);
    });

    // Thousands seperator
    $("#budget-input").autoNumeric("init");

    // Set validator format
    $.validator.addMethod('date', function (value, element) {
        if (this.optional(element)) {
            return true;
        }
        var ok = true;
        try {
            $.datepicker.parseDate('dd-mm-yy', value);
        }
        catch (err) {
            ok = false;
        }
        return ok;
    });

    // Datepicker default setup for date
    $(".date-picker").each(function () {
        var options = {
            dayOfWeekStart: 1,
            timepicker: false,
            format: 'd-m-Y',
            scrollInput: false
        };
        $(this).datetimepicker(options);
    });

    // DateTimepicker default setup for datetime
    $(".datetime-picker").each(function () {
        var options = {
            dayOfWeekStart: 1,
            timepicker: true,
            format: 'd-m-Y H:i',
            scrollInput: false
        };
        $(this).datetimepicker(options);
    });

    $.datetimepicker.setLocale('da');

    // Auto reqsize textareas
    function autoResize(e) {
        $(e).css({ 'height': 'auto', 'overflow-y': 'hidden' }).height(e.scrollHeight);
    }
    $('textarea').each(function () {
        autoResize(this);
    }).on('input', function () {
        autoResize(this);
    });

    $(".tag-list").select2({
        tags: true,
        tokenSeparators: [","],
        dropdownCssClass: "hide",
        language: "da"
    });

    // set class map-link's href
    var mapLinkElement = $('.map-link');
    mapLinkElement.each(function (index, element) {
        var id = $(this).data("aws-id");
        var baseMapUrl = "http://webgis.aarhus.dk/webgis25/pageredirect.aspx?page=/trafikprojekter/kort.htm&q=";
        $.get(addressIdUrl + id, null, function(data) {
            // set href
            var fullUrl = baseMapUrl + data.adressebetegnelse;
            element.href = fullUrl;
        });
    });

    // PRIORITY TEXT FIELDS
    // If the user enters a digit (not zero), change the value of the text
    // field to this digit. If the user enters zero, append to existing value.
    // Ignore all values not in range 0-9.
    $('.priority-text').on('keypress', function (e) {
        var char = String.fromCharCode(e.which);

        if (e.which < 48 || e.which > 57)
            // Not between 0-9
            return false;

        if (char !== "0") {
            // Between 1-9
            $(this).val(char);
            return false;
        }
    });
});
