﻿// Default settings for autoNumeric jQuery plugin
// See https://github.com/BobKnothe/autoNumeric for options
$(document).ready(function () {
    $.extend($.fn.autoNumeric.defaults, {
        aSep: '.',
        aDec: ',',
        aPad: false,
        aForm: false,
        mDec: '0'
    });
});
