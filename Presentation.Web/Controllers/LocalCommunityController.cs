﻿#region Head
// <copyright file="LocalCommunityController.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using Core.DomainModel;
using Core.DomainServices;
using Presentation.Web.Attributes;
using Presentation.Web.Helpers;
using Presentation.Web.Models.LocalCommunity;

namespace Presentation.Web.Controllers
{
    [AuthorizeRoles(AdHelper.Role.Admin)]
    public class LocalCommunityController : BaseController
    {
        private readonly IGenericRepository<LocalCommunity> _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMappingEngine _mapper;

        public LocalCommunityController(IGenericRepository<LocalCommunity> repo, IUnitOfWork unitOfWork, IMappingEngine mapper)
        {
            _repo = repo;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        // GET: LocalCommunity
        public ActionResult Index()
        {
            var localCommunities = _repo.Get(a => !a.IsDeleted);

            var vms = _mapper.Map<IEnumerable<IndexLocalCommunityViewModel>>(localCommunities);

            return View(vms);
        }

        // GET: LocalCommunity/Create
        public ActionResult Create()
        {
            var vm = new CreateLocalCommunityViewModel();
            return View(vm);
        }

        // POST: LocalCommunity/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateLocalCommunityViewModel createLocalCommunityViewModel)
        {
            if (!ModelState.IsValid) return View(createLocalCommunityViewModel);

            var localCommunity = _mapper.Map<LocalCommunity>(createLocalCommunityViewModel);
            localCommunity.CreatedOn = DateTime.Now;
            localCommunity.CreatedBy = AdHelper.Name;

            _repo.Insert(localCommunity);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        // GET: LocalCommunity/Edit/{id}
        public ActionResult Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var localCommunity = _repo.GetByKey(id);
            if (localCommunity == null || localCommunity.IsDeleted) return HttpNotFound();
            SetCreatedModifiedInViewBag(localCommunity);

            var vm = _mapper.Map<EditLocalCommunityViewModel>(localCommunity);

            return View(vm);
        }

        // POST: LocalCommunityEdit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditLocalCommunityViewModel editLocalCommunityViewModel)
        {
            if (!ModelState.IsValid)
            {
                var logger = _repo.GetByKey(editLocalCommunityViewModel.Id) as Logger;
                SetCreatedModifiedInViewBag(logger);
                return View(editLocalCommunityViewModel);
            }

            var localCommunity = _mapper.Map<LocalCommunity>(editLocalCommunityViewModel);
            localCommunity.ModifiedBy = AdHelper.Name;
            localCommunity.ModifiedOn = DateTime.Now;

            _repo.Update(localCommunity);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        // GET: LocalCommunityDelete/5
        public ActionResult Delete(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var localCommunity = _repo.GetByKey(id);
            if (localCommunity == null || localCommunity.IsDeleted) return HttpNotFound();
            SetCreatedModifiedInViewBag(localCommunity);

            var vm = _mapper.Map<IndexLocalCommunityViewModel>(localCommunity);

            return View(vm);
        }

        // POST: LocalCommunityDelete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (id <= 0) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var localCommunity = _repo.GetByKey(id);
            if (localCommunity == null || localCommunity.IsDeleted) return HttpNotFound();

            localCommunity.IsDeleted = true;

            _repo.Update(localCommunity);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }
    }
}
