﻿#region Head
// <copyright file="AgendaPointController.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using Core.DomainModel;
using Core.DomainServices;
using Presentation.Web.Filters;
using Presentation.Web.Helpers;
using Presentation.Web.Models;
using Presentation.Web.Models.AgendaPoint;
using Presentation.Web.Models.Shared;

namespace Presentation.Web.Controllers
{
    public class AgendaPointController : BaseController
    {
        private readonly IGenericRepository<AgendaPoint> _agendaPointRepo;
        private readonly IGenericRepository<AgendaPointDocument> _documentRepository;
        private readonly IDocumentHelper _documentHelper;
        private readonly IGenericRepository<Project> _projectRepo;
        private readonly IGenericRepository<Meeting> _meetingRepo;
        private readonly IGenericRepository<Topic> _topicRepo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMappingEngine _mapper;
        private readonly IDateTime _dateTime;
        private readonly IGeoCoder _geoCoder;
        private readonly IDawaRepository _dawaRepo;
        private readonly PrivilegeHelper _privilegeHelper;

        public AgendaPointController(
            IGenericRepository<AgendaPoint> agendaPointRepo,
            IGenericRepository<Project> projectRepo,
            IGenericRepository<Meeting> meetingRepo,
            IGenericRepository<Topic> topicRepo,
            IUnitOfWork unitOfWork,
            IMappingEngine mapper,
            IDateTime dateTime,
            IGenericRepository<AgendaPointDocument> documentRepository,
            IDocumentHelper documentHelper, 
            IGeoCoder geoCoder,
            IDawaRepository dawaRepo)
        {
            _agendaPointRepo = agendaPointRepo;
            _projectRepo = projectRepo;
            _meetingRepo = meetingRepo;
            _topicRepo = topicRepo;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _dateTime = dateTime;
            _documentRepository = documentRepository;
            _documentHelper = documentHelper;
            _geoCoder = geoCoder;
            _dawaRepo = dawaRepo;
            _privilegeHelper = new PrivilegeHelper(_agendaPointRepo);
        }

        // GET: AgendaPoint
        public ActionResult Index()
        {
            var agendaPoints = _agendaPointRepo.Get(a => a.MeetingAgendaPoint.Number != 0);

            var agendaPointsVm = _mapper.Map<IEnumerable<IndexAgendaPointViewModel>>(agendaPoints).ToList();
            
            // set deleteable
            agendaPointsVm.ForEach(o => o.IsDeleableForCurrentUser = _privilegeHelper.IsAgendaPointIsDeleteableForCurrentUser(o.Id));

            return View(agendaPointsVm);
        }

        // GET: AgendaPoint/Create
        [PopulateTopics]
        public ActionResult Create()
        {
            var vm = new CreateAgendaPointViewModel
            {
                StreetType = StreetTypeViewModel.PublicStreet,
                AgendaPointStatus = AgendaPointStatusTypeViewModel.Draft,
                MeetingIds = new List<string>()
            };

            return View(vm);
        }

        // POST: AgendaPoint/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [PopulateTopics]
        public ActionResult Create(CreateAgendaPointViewModel agendaPointViewModel)
        {
            if (!ModelState.IsValid)
            {
                if (agendaPointViewModel.ProjectId != null && agendaPointViewModel.ProjectId != 0)
                {
                    var project = _projectRepo.GetByKey(agendaPointViewModel.ProjectId);
                    if (project != null)
                        ViewBag.ProjectTopic = project.Topic;
                    else
                        agendaPointViewModel.ProjectId = null;
                }

                if (agendaPointViewModel.MeetingIds == null)
                    agendaPointViewModel.MeetingIds = new List<string>();

                return View(agendaPointViewModel);
            }

            var agendaPoint = _mapper.Map<AgendaPoint>(agendaPointViewModel);

            // foreach guid in vm, get document from db and insert in created project
            var guidList = agendaPoint.Documents.Select(o => o.Guid).ToList();

            agendaPoint.Documents.Clear();
            foreach (var guid in guidList)
            {
                var guid1 = guid;
                var document = _documentRepository.Get(o => o.Guid == guid1).First();
                agendaPoint.Documents.Add(document);
            }

            agendaPoint.CreatedOn = _dateTime.Now;
            agendaPoint.CreatedBy = AdHelper.Name;

            agendaPoint.PreviousMeetings = CreateMeetingCollection(agendaPointViewModel.MeetingIds);

            var topic = _topicRepo.GetByKey(agendaPointViewModel.TopicId);
            if (topic != null)
            {
                agendaPoint.Topic = topic;
            }

            // Add spartial object
            agendaPoint.AddressLocation = _geoCoder.GetSpartialObject(agendaPointViewModel.AddressId);

            // Fetch address details from server and save to local DB
            var addr = _dawaRepo.GetAddress(agendaPointViewModel.AddressId);
            agendaPoint.AddressStreetName = addr.StreetName;
            agendaPoint.AddressNumber = addr.Number;

            _agendaPointRepo.Insert(agendaPoint);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult CopyAgendaPoint(CreateAgendaPointViewModel agendaPointViewModel)
        {
            var createdAgendaPoint = _agendaPointRepo.Create();
            var agendaPoint = _mapper.Map<AgendaPoint>(agendaPointViewModel);

            // copying vm data
            createdAgendaPoint.AddressId = agendaPoint.AddressId;
            createdAgendaPoint.AddressStreetName = agendaPoint.AddressStreetName;
            createdAgendaPoint.AddressNumber = agendaPoint.AddressNumber;
            createdAgendaPoint.ResponsiblePersonName = agendaPoint.ResponsiblePersonName;
            createdAgendaPoint.StreetType = agendaPoint.StreetType;
            createdAgendaPoint.TopicId = agendaPoint.TopicId;
            createdAgendaPoint.Description = agendaPoint.Description;
            createdAgendaPoint.Agreement = agendaPoint.Agreement;
            createdAgendaPoint.AgendaPointStatus = agendaPoint.AgendaPointStatus;
            createdAgendaPoint.ProjectId = agendaPoint.ProjectId;
            createdAgendaPoint.PreviousMeetings = agendaPoint.PreviousMeetings;
            createdAgendaPoint.MeetingAgendaPoint = agendaPoint.MeetingAgendaPoint;
            createdAgendaPoint.Documents = agendaPoint.Documents;
            createdAgendaPoint.Area = agendaPoint.Area;
            createdAgendaPoint.AgendaPointStatus = agendaPoint.AgendaPointStatus;
            createdAgendaPoint.DiscussedWithPartners = agendaPoint.DiscussedWithPartners;

            // foreach guid in vm, get document from db and insert in created project
            var guidList = createdAgendaPoint.Documents.Select(o => o.Guid).ToList();

            createdAgendaPoint.Documents.Clear();
            foreach (var guid in guidList)
            {
                var guid1 = guid;
                var document = _documentRepository.Get(o => o.Guid == guid1).First();
                createdAgendaPoint.Documents.Add(document);
            }

            createdAgendaPoint.CreatedOn = _dateTime.Now;
            createdAgendaPoint.CreatedBy = AdHelper.Name;

            createdAgendaPoint.PreviousMeetings = CreateMeetingCollection(agendaPointViewModel.MeetingIds);

            var topic = _topicRepo.GetByKey(agendaPointViewModel.TopicId);
            if (topic != null)
            {
                createdAgendaPoint.Topic = topic;
            }

            // Add spartial object
            createdAgendaPoint.AddressLocation = _geoCoder.GetSpartialObject(agendaPointViewModel.AddressId);


            _agendaPointRepo.Insert(createdAgendaPoint);
            _unitOfWork.Save();

            return RedirectToAction("Details", new { id = createdAgendaPoint.Id });
        }

        // GET: AgendaPoint/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var agendaPoint = _agendaPointRepo.GetByKey(id);
            if (agendaPoint == null) return HttpNotFound();

            SetCreatedModifiedInViewBag(agendaPoint);
            var agendaPointViewModel = _mapper.Map<DetailsAgendaPointViewModel>(agendaPoint);

            return View(agendaPointViewModel);
        }

        // GET: AgendaPoint/Edit/5
        [PopulateTopics]
        public ActionResult Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var agendaPoint = _agendaPointRepo.GetByKey(id);
            if (agendaPoint == null) return HttpNotFound();
            if (!agendaPoint.CanBeEdited) return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            SetCreatedModifiedInViewBag(agendaPoint);
            var agendaPointViewModel = _mapper.Map<EditAgendaPointViewModel>(agendaPoint);

            agendaPointViewModel.MeetingIds = new List<string>();

            var meetingIds = new List<string>();
            foreach (var meeting in agendaPoint.PreviousMeetings)
            {
                meetingIds.Add(meeting.Id.ToString(CultureInfo.CurrentCulture));
            }

            if (agendaPointViewModel.ProjectId != null && agendaPointViewModel.ProjectId != 0)
            {
                var project = _projectRepo.GetByKey(agendaPointViewModel.ProjectId);
                if (project != null)
                    ViewBag.ProjectTopic = project.Topic;
                else
                    agendaPointViewModel.ProjectId = null;
            }

            agendaPointViewModel.MeetingIds = meetingIds;

            return View(agendaPointViewModel);
        }

        // POST: AgendaPoint/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [PopulateTopics]
        public ActionResult Edit(EditAgendaPointViewModel agendaPointViewModel)
        {
            var agendaPoint = _agendaPointRepo.GetByKey(agendaPointViewModel.Id);
            if (agendaPoint == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
            {
                if (agendaPointViewModel.ProjectId != null && agendaPointViewModel.ProjectId != 0)
                {
                    var project = _projectRepo.GetByKey(agendaPointViewModel.ProjectId);
                    if (project != null)
                        ViewBag.ProjectTopic = project.Topic;
                    else
                        agendaPointViewModel.ProjectId = null;
                }

                if (agendaPointViewModel.MeetingIds == null)
                    agendaPointViewModel.MeetingIds = new List<string>();

                SetCreatedModifiedInViewBag(agendaPoint);
                return View(agendaPointViewModel);
            }

            if (agendaPointViewModel.Documents != null && agendaPointViewModel.Documents.Any())
            {
                // get guid for document(s) to remove
                var originalGuidsList = agendaPoint.Documents.Select(o => o.Guid).ToList();
                var guidsList = agendaPointViewModel.Documents.Select(o => o.Guid).ToList();
                var exceptGuids = originalGuidsList.Except(guidsList).ToList();

                // Delete removed documents
                foreach (var exceptGuid in exceptGuids)
                {
                    _documentRepository.DeleteByKey(exceptGuid);
                    DeleteFilesFromServer(exceptGuids.ToList());
                }
            }

            // Geocoding
            if (agendaPointViewModel.AddressId != agendaPoint.AddressId)
                agendaPointViewModel.AddressLocation = _geoCoder.GetSpartialObject(agendaPointViewModel.AddressId);

            // Write properties
            _mapper.Map(agendaPointViewModel, agendaPoint);
            agendaPoint.ModifiedOn = _dateTime.Now;
            agendaPoint.ModifiedBy = AdHelper.Name;

            var documentList = agendaPoint.Documents.ToList();

            agendaPoint.Documents.Clear();

            // foreach guid in vm, get document from db and insert in project
            foreach (var doc in documentList)
            {
                var guid1 = doc.Guid;
                var document = _documentRepository.Get(o => o.Guid == guid1).First();
                document.AgendaPointId = agendaPoint.Id;
                document.Comment = doc.Comment;

                // update document through documentRepository, because EF marks navigation properties of parent as unmodified
                _documentRepository.Update(document);

                agendaPoint.Documents.Add(document);
            }

            // Remove meetings from many-to-many relation
            if (agendaPointViewModel.RemovedMeetingIds != null)
            {
                foreach (
                    var removedMeetingId in
                        agendaPointViewModel.RemovedMeetingIds.Split(new[] { ' ' },
                            System.StringSplitOptions.RemoveEmptyEntries))
                {
                    int meetingIdInt;
                    if (!int.TryParse(removedMeetingId, out meetingIdInt)) continue;

                    var existingMeeting = agendaPoint.PreviousMeetings.FirstOrDefault(m => m.Id == meetingIdInt);

                    if (existingMeeting == null) continue;

                    agendaPoint.PreviousMeetings.Remove(existingMeeting);
                }
            }

            // Add new meetings to many-to-many relation
            foreach (var meeting in CreateMeetingCollection(agendaPointViewModel.MeetingIds))
            {
                agendaPoint.PreviousMeetings.Add(meeting);
            }

            // Fetch address details from server and save to local DB
            var addr = _dawaRepo.GetAddress(agendaPointViewModel.AddressId);
            agendaPoint.AddressStreetName = addr.StreetName;
            agendaPoint.AddressNumber = addr.Number;

            _agendaPointRepo.Update(agendaPoint);
            _unitOfWork.Save();

            return RedirectToAction("Details", new { id = agendaPoint.Id });
        }

        public ActionResult SaveUploadedFile()
        {
            var fileObj = _documentHelper.UploadFile(Request, Server);
            if (fileObj == null) return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);

            // create document
            var document = new AgendaPointDocument
            {
                Comment = Request.Form["comment"],
                Guid = fileObj.Guid,
                FileName = fileObj.File.FileName,
                FileSize = fileObj.File.ContentLength,
                AgendaPointId = fileObj.ForeignKeyId
            };

            _documentRepository.Insert(document);
            _unitOfWork.Save();

            return Json(new { Guid = fileObj.Guid }); ;
        }

        // GET: AgendaPoint/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var agendaPoint = _agendaPointRepo.GetByKey(id);
            SetCreatedModifiedInViewBag(agendaPoint);
            var vm = _mapper.Map<DeleteAgendaPointViewModel>(agendaPoint);

            return View(vm);
        }

        // POST: AgendaPoint/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            // return to index if not allowed to delete
            if (!_privilegeHelper.IsAgendaPointIsDeleteableForCurrentUser(id))
                return RedirectToAction("Index");

            var documents = _documentRepository.Get(o => o.AgendaPointId == id).ToList();
            // delete all attached documents from server
            DeleteFilesFromServer(documents.Select(o => o.Guid));

            // delete doc from db
            foreach (var doc in documents)
            {
                _documentRepository.DeleteByKey(doc.Guid);
            }

            _agendaPointRepo.DeleteByKey(id);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        /// <summary>
        /// For downloading files
        /// </summary>
        /// <param name="guid">The file id.</param>
        /// <returns>The found file.</returns>
        public FileResult Download(Guid guid)
        {
            return _documentHelper.Download(Server, guid);
        }

        [HttpPost]
        public ActionResult DeleteFilesFromServer(IEnumerable<Guid> fileGuidsToDelete)
        {
            if (_documentHelper.DeleteFiles(fileGuidsToDelete, Server))
                return new HttpStatusCodeResult(HttpStatusCode.OK);

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        #region Helpers
        /// <summary>
        /// Create collection of attached meetings from list of ids
        /// </summary>
        /// <param name="meetingIds">List of IDs as strings</param>
        /// <returns>Collection with attached meetings if any</returns>
        private ICollection<Meeting> CreateMeetingCollection(IEnumerable<string> meetingIds)
        {
            var meetingCollection = new List<Meeting>();

            if (meetingIds == null) return meetingCollection;

            foreach (var meetingId in meetingIds)
            {
                var meetingIdInt = int.Parse(meetingId, CultureInfo.CurrentCulture);
                var attachedMeeting = _meetingRepo.GetByKey(meetingIdInt);
                if (attachedMeeting == null) continue;

                meetingCollection.Add(attachedMeeting);
            }

            return meetingCollection;
        }
        #endregion
    }
}
