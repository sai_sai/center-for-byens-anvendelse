﻿#region Head
// <copyright file="CompanyController.cs" company="IT Minds" year="2016">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using Core.DomainModel;
using Core.DomainServices;
using Presentation.Web.Helpers;
using Presentation.Web.Models.Company;

namespace Presentation.Web.Controllers
{
    public class CompanyController : BaseController
    {
        private readonly IGenericRepository<Company> _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMappingEngine _mapper;

        public CompanyController(IGenericRepository<Company> repo, IUnitOfWork unitOfWork, IMappingEngine mapper)
        {
            _repo = repo;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        // GET: Company
        public ActionResult Index()
        {
            var companies = _repo.Get(a => !a.IsDeleted);

            var vms = _mapper.Map<IEnumerable<IndexCompanyViewModel>>(companies);

            return View(vms);
        }

        // GET: Company/Create
        public ActionResult Create()
        {
            var vm = new CreateCompanyViewModel();
            return View(vm);
        }

        // POST: Company/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateCompanyViewModel addCompanyViewModel)
        {
            if (!ModelState.IsValid) return View(addCompanyViewModel);

            var company = _mapper.Map<Company>(addCompanyViewModel);
            company.CreatedOn = DateTime.Now;
            company.CreatedBy = AdHelper.Name;

            _repo.Insert(company);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        // GET: Company/Edit/{id}
        public ActionResult Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var company = _repo.GetByKey(id);
            if (company == null || company.IsDeleted) return HttpNotFound();

            SetCreatedModifiedInViewBag(company);

            var vm = _mapper.Map<EditCompanyViewModel>(company);

            return View(vm);
        }

        // POST: Company/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditCompanyViewModel editCompanyViewModel)
        {
            if (!ModelState.IsValid)
            {
                var logger = _repo.GetByKey(editCompanyViewModel.Id) as Logger;
                SetCreatedModifiedInViewBag(logger);
                return View(editCompanyViewModel);
            }

            var company = _mapper.Map<Company>(editCompanyViewModel);
            company.ModifiedOn = DateTime.Now;
            company.ModifiedBy = AdHelper.Name;

            _repo.Update(company);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        // GET: Company/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var company = _repo.GetByKey(id);
            if (company == null || company.IsDeleted) return HttpNotFound();
            SetCreatedModifiedInViewBag(company);

            var vm = _mapper.Map<DeleteCompanyViewModel>(company);

            return View(vm);
        }

        // Delete: Company/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (id <= 0) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var company = _repo.GetByKey(id);
            if (company == null || company.IsDeleted) return HttpNotFound();

            company.IsDeleted = true;

            _repo.Update(company);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }
    }
}
