﻿#region Head
// <copyright file="MeetingController.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using Core.DomainModel;
using Core.DomainServices;
using Presentation.Web.Attributes;
using Presentation.Web.Filters;
using Presentation.Web.Helpers;
using Presentation.Web.Models;
using Presentation.Web.Models.Meeting;
using Presentation.Web.Models.Meeting.MeetingAgendaPoint;
using Presentation.Web.Models.MeetingAgendaPoint;

namespace Presentation.Web.Controllers
{
    public class MeetingController : BaseController
    {
        private readonly IGenericRepository<Meeting> _meetingRepo;
        private readonly IGenericRepository<MeetingAgendaPoint> _meetingAgendaPointRepo;
        private readonly IGenericRepository<AgendaPoint> _agendaPointRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMappingEngine _mapper;
        private readonly IDateTime _dateTime;

        public static Guid AgendaPointZeroAddressId;

        private readonly string _pointZeroDescription;

        public MeetingController(
            IGenericRepository<Meeting> meetingRepo,
            IGenericRepository<MeetingAgendaPoint> meetingAgendaPointRepo,
            IGenericRepository<AgendaPoint> agendaPointRepository,
            IUnitOfWork unitOfWork,
            IMappingEngine mapper,
            IDateTime dateTime,
            IAppSettings appSettings)
        {
            _meetingRepo = meetingRepo;
            _meetingAgendaPointRepo = meetingAgendaPointRepo;
            _agendaPointRepository = agendaPointRepository;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _dateTime = dateTime;

            AgendaPointZeroAddressId = new Guid(appSettings.AgendaPointZeroAddressId);
            _pointZeroDescription = appSettings.AgendaPointZeroDescription;

        }

        // GET: Meeting
        public ActionResult Index()
        {
            var meetings = _meetingRepo.Get();

            var meetingsViewModels = _mapper.Map<IEnumerable<IndexMeetingViewModel>>(meetings);

            return View(meetingsViewModels);
        }

        // GET: Meeting/Create
        [PopulateInvitedPeople]
        public ActionResult Create()
        {
            var openAgendaPoints = _agendaPointRepository.Get(a => a.MeetingAgendaPoint == null &&
                                                                   a.AgendaPointStatus == AgendaPointStatusType.Ready);

            var openAgendaPointVMs = _mapper.Map<IList<AgendaPointViewModel>>(openAgendaPoints);
            var meetingAgendaPoints = new List<MeetingAgendaPointViewModel>();

            foreach (var vm in openAgendaPointVMs)
            {
                var meetingAgendaPointVM = new MeetingAgendaPointViewModel
                {
                    Number = 1,
                    AgendaPoint = vm,
                    AgendaPointId = vm.Id
                };
                meetingAgendaPoints.Add(meetingAgendaPointVM);
            }

            var meetingViewModel = new CreateMeetingViewModel
            {
                MeetingAgendaPoints = meetingAgendaPoints,
                Date = _dateTime.Now.ToString()
            };

            return View(meetingViewModel);
        }

        // POST: Meeting/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [PopulateInvitedPeople]
        public ActionResult Create(CreateMeetingViewModel meetingViewModel)
        {
            if (meetingViewModel == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            if (!ModelState.IsValid) return View(meetingViewModel);

            var meeting = _mapper.Map<Meeting>(meetingViewModel);
            var selectedAgendaPointIds = new List<int>();

            if (meetingViewModel.MeetingAgendaPoints.Any())
                selectedAgendaPointIds = meetingViewModel.MeetingAgendaPoints.Where(o => o.AgendaPoint.IsAssignedToMeeting).Select(x => x.AgendaPoint.Id).ToList();
            // Using this as proxy to the actual created meeting for id reference
            var createdMeeting = _meetingRepo.Create();

            // copying vm data
            createdMeeting.Location = meeting.Location;
            createdMeeting.Date = meeting.Date;
            createdMeeting.InvitedPeople = meeting.InvitedPeople;
            createdMeeting.CreatedOn = _dateTime.Now;
            createdMeeting.CreatedBy = AdHelper.Name;

            // Add meetingpointzero
            var agendaPointZero = new AgendaPoint
            {
                Description = _pointZeroDescription,
                AddressId = AgendaPointZeroAddressId
            };
            var meetingAgendaPointZero = new MeetingAgendaPoint
            {
                MeetingId = createdMeeting.Id,
                AgendaPoint = agendaPointZero,
                Number = 0
            };
            createdMeeting.MeetingAgendaPoints.Add(meetingAgendaPointZero);

            // insert meeting
            _meetingRepo.Insert(createdMeeting);

            // insert all meetingagendapoints in db
            var number = 1;
            foreach (var selectedId in selectedAgendaPointIds)
            {
                var meetingAgendaPoint = new MeetingAgendaPoint
                {
                    Id = selectedId,
                    MeetingId = createdMeeting.Id,
                    Number = number++
                };
                _meetingAgendaPointRepo.Insert(meetingAgendaPoint);
            }
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        // GET: Meeting/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var meeting = _meetingRepo.GetByKey(id);
            if (meeting == null) return HttpNotFound();

            SetCreatedModifiedInViewBag(meeting);
            var meetingViewModel = _mapper.Map<DetailsMeetingViewModel>(meeting);

            if (meetingViewModel.MeetingAgendaPoints != null)
            {
                // sort meetingagendapoints by number
                var sortedMeetingAgendaPointsByNumber = meetingViewModel.MeetingAgendaPoints.OrderBy(o => o.Number);
                meetingViewModel.MeetingAgendaPoints = sortedMeetingAgendaPointsByNumber.ToList();
            }
            return View(meetingViewModel);
        }

        // GET: Meeting/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var meeting = _meetingRepo.GetByKey(id);
            if (meeting == null) return HttpNotFound();

            SetCreatedModifiedInViewBag(meeting);
            var editMeetingViewModel = _mapper.Map<EditMeetingViewModel>(meeting);

            // Get all open agendapoints
            var openAgendaPoints = _agendaPointRepository.Get(a => a.MeetingAgendaPoint == null &&
                                                                   a.AgendaPointStatus == AgendaPointStatusType.Ready);
            var openAgendaPointVMs = _mapper.Map<IEnumerable<AgendaPointViewModel>>(openAgendaPoints);

            // Convert openAgendaPointsVM to meetingAgendaPoint
            var openMeetingAgendaPoints = openAgendaPointVMs.Select(openAgendaPointVM => new MeetingAgendaPointViewModel
            {
                AgendaPoint = openAgendaPointVM,
                Id = openAgendaPointVM.Id,
                Number = 1
            }).ToList();

            // Dont show agenda point zero
            if (editMeetingViewModel.MeetingAgendaPoints != null)
            {
                foreach (var meetingAgendaPoint in editMeetingViewModel.MeetingAgendaPoints.ToList())
                {
                    if (meetingAgendaPoint.Number == 0)
                        editMeetingViewModel.MeetingAgendaPoints.Remove(meetingAgendaPoint);
                }

                // Add open agendapoints
                foreach (var openAgendaPoint in openMeetingAgendaPoints)
                {
                    editMeetingViewModel.MeetingAgendaPoints.Add(openAgendaPoint);
                }
            }

            return View(editMeetingViewModel);
        }

        // POST: Meeting/Edit/5
        // TODO: This method is absurdly insane
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditMeetingViewModel meetingViewModel)
        {
            if (meetingViewModel == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var meeting = _meetingRepo.GetByKey(meetingViewModel.Id);
            if (meeting == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
            {
                SetCreatedModifiedInViewBag(meeting);
                return View(meetingViewModel);
            }

            if (meetingViewModel.InvitedPeople == null) meetingViewModel.InvitedPeople = new List<string>();

            if (meeting.MeetingAgendaPoints.Any())
            {
                var meetingPointZero = meeting.MeetingAgendaPoints.Single(o => o.Number == 0);
                var meetingPointZeroVM = _mapper.Map<MeetingAgendaPointViewModel>(meetingPointZero);
                meetingViewModel.MeetingAgendaPoints.Add(meetingPointZeroVM); // add meeting point zero

                // Check if any agendapoints has been removed from original meeting
                var originalMeetingAgendaPointIds = meeting.MeetingAgendaPoints.Where(o => o.Number != 0).Select(o => o.Id);
                var vmSelectedMeetingAgendaPointIds = new List<int>();
                if (meetingViewModel.MeetingAgendaPoints.Any())
                    vmSelectedMeetingAgendaPointIds =
                        meetingViewModel.MeetingAgendaPoints.Where(
                            o => o.AgendaPoint.IsAssignedToMeeting || o.MeetingId != 0)
                            .Select(o => o.Id)
                            .ToList(); // meetingId is 0 if an openMeetingAgendaPoint not attached to a meeting
                var meetingAgendaPointIdsToBeRemoved = originalMeetingAgendaPointIds.Except(vmSelectedMeetingAgendaPointIds);

                // remove meetingAgendaPointToBeRemoved
                foreach (var idToRemove in meetingAgendaPointIdsToBeRemoved.ToList())
                {
                    _meetingAgendaPointRepo.DeleteByKey(idToRemove);
                }
            }

            _mapper.Map(meetingViewModel, meeting);

            meeting.ModifiedOn = _dateTime.Now;
            meeting.ModifiedBy = AdHelper.Name;

            // Add attached meetingagendapoints
            if (meetingViewModel.MeetingAgendaPoints.Any())
            {
                var assignedMeetingAgendaPoints = meetingViewModel.MeetingAgendaPoints.Where(o => o.AgendaPoint.IsAssignedToMeeting).ToList();

                meeting.MeetingAgendaPoints.Clear();
                var index = 0;
                foreach (var meetingAgendaPoint in GetAttachedMeetingAgendaPoints(meetingViewModel.MeetingAgendaPoints))
                {
                    if (meetingAgendaPoint.Id == 0) // meeting agendapoint doesnt exist
                    {
                        meetingAgendaPoint.Id = assignedMeetingAgendaPoints[index].AgendaPoint.Id;
                        meetingAgendaPoint.MeetingId = assignedMeetingAgendaPoints[index].AgendaPoint.Id;
                        meetingAgendaPoint.Number = 1;
                    }
                    meetingAgendaPoint.Number = assignedMeetingAgendaPoints[index].Number;
                    meeting.MeetingAgendaPoints.Add(meetingAgendaPoint);
                    index++;
                }
            }

            _meetingRepo.Update(meeting);
            _unitOfWork.Save();

            return RedirectToAction("Details", new { id = meeting.Id });
        }

        // GET: Meeting/CreateSummaries
        [PopulateDefaultPhrases]
        public ActionResult CreateSummaries(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var meeting = _meetingRepo.GetByKey(id);

            if (meeting == null)
            {
                return HttpNotFound();
            }

            var meetingVm = _mapper.Map<CreateSummariesMeetingViewModel>(meeting);

            SetCreatedModifiedInViewBag(meeting);

            if (meetingVm.MeetingAgendaPoints == null)
            {
                meetingVm.MeetingAgendaPoints = new List<MeetingAgendaPointSummariesViewModel>();
            }

            // sort meetingagendapoints by number
            var sortedMeetingAgendaPointsByNumber = meetingVm.MeetingAgendaPoints.OrderBy(o => o.Number);
            meetingVm.MeetingAgendaPoints = sortedMeetingAgendaPointsByNumber.ToList();
            return View(meetingVm);
        }

        // POST: Meeting/CreateSummaries
        [HttpPost]
        [ValidateAntiForgeryToken]
        [PopulateDefaultPhrases]
        public ActionResult CreateSummaries(CreateSummariesMeetingViewModel createSummariesMeetingViewModel)
        {
            if (createSummariesMeetingViewModel == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
                return View(createSummariesMeetingViewModel);

            var meeting = _mapper.Map<Meeting>(createSummariesMeetingViewModel);

            if (meeting.MeetingAgendaPoints.Any())
            {
                foreach (var agendaPoint in meeting.MeetingAgendaPoints.Select(o => o.AgendaPoint))
                {
                    _agendaPointRepository.Update(agendaPoint);
                }
            }

            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        // GET: Meeting/LockSummary/5
        [AuthorizeRoles(AdHelper.Role.Admin)]
        public ActionResult LockSummary(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var meeting = _meetingRepo.GetByKey(id);

            if (meeting == null)
            {
                return HttpNotFound();
            }

            meeting.IsSummaryLocked = true;

            _meetingRepo.Update(meeting);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        // GET: Meeting/UnlockSummary/5
        [AuthorizeRoles(AdHelper.Role.Admin)]
        public ActionResult UnlockSummary(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var meeting = _meetingRepo.GetByKey(id);

            if (meeting == null)
            {
                return HttpNotFound();
            }

            meeting.IsSummaryLocked = false;

            _meetingRepo.Update(meeting);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        #region Helpers
        /// <summary>
        /// Set Created and Modified information in ViewBag.
        /// </summary>
        /// <param name="meeting">The meeting.</param>
        private void SetCreatedModifiedInViewBag(Meeting meeting)
        {
            if (meeting == null)
            {
                return;
            }

            ViewBag.CreatedOn = meeting.CreatedOn.ToShortDateString();
            ViewBag.CreatedBy = meeting.CreatedBy;
            ViewBag.ModifiedOn = meeting.ModifiedOn;
            ViewBag.ModifiedBy = meeting.ModifiedBy;
        }

        /// <summary>
        /// Get attached meetingAgendaPoints from repository from a list of meetingAgendaPointViewModels.
        /// </summary>
        /// <param name="meetingAgendaPointsViewModels">List of MeetingAgendaPoints with IDs set</param>
        /// <returns>List of attached MeetingAgendaPoint found in repository</returns>
        private IEnumerable<MeetingAgendaPoint> GetAttachedMeetingAgendaPoints(IEnumerable<MeetingAgendaPointViewModel> meetingAgendaPointsViewModels)
        {
            var meetingAgendaPoints = new List<MeetingAgendaPoint>();

            foreach (var meetingAgendaPointViewModel in meetingAgendaPointsViewModels.Where(a => a.AgendaPoint.IsAssignedToMeeting))
            {
                var meetingAgendaPoint = _meetingAgendaPointRepo.GetByKey(meetingAgendaPointViewModel.Id) ??
                                         _meetingAgendaPointRepo.Create();

                meetingAgendaPoints.Add(meetingAgendaPoint);
            }

            return meetingAgendaPoints;
        }
        #endregion
    }
}
