﻿#region Head
// <copyright file="DefaultPhraseController.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using Core.DomainModel;
using Core.DomainServices;
using Presentation.Web.Attributes;
using Presentation.Web.Helpers;
using Presentation.Web.Models.DefaultPhrase;

namespace Presentation.Web.Controllers
{
    [AuthorizeRoles(AdHelper.Role.Admin)]
    public class DefaultPhraseController : BaseController
    {
        private readonly IGenericRepository<DefaultPhrase> _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMappingEngine _mapper;

        public DefaultPhraseController(IGenericRepository<DefaultPhrase> repo, IUnitOfWork unitOfWork, IMappingEngine mapper)
        {
            _repo = repo;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        // GET: DefaultPhrase
        public ActionResult Index()
        {
            var defaultPhrases = _repo.Get(p => !p.IsDeleted);

            var vms = _mapper.Map<IEnumerable<DefaultPhraseViewModel>>(defaultPhrases);

            return View(vms);
        }

        // GET: DefaultPhrase/Create
        public ActionResult Create()
        {
            var vm = new DefaultPhraseViewModel();
            return View(vm);
        }

        // POST: DefaultPhrase/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(DefaultPhraseViewModel defaultPhraseViewModel)
        {
            if (!ModelState.IsValid) return View(defaultPhraseViewModel);

            var defaultPhrase = _mapper.Map<DefaultPhrase>(defaultPhraseViewModel);
            defaultPhrase.CreatedOn = DateTime.Now;
            defaultPhrase.CreatedBy = AdHelper.Name;

            _repo.Insert(defaultPhrase);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        // GET: DefaultPhrase/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var defaultPhrase = _repo.GetByKey(id);
            if (defaultPhrase == null || defaultPhrase.IsDeleted) return HttpNotFound();

            SetCreatedModifiedInViewBag(defaultPhrase);

            var vm = _mapper.Map<DefaultPhraseViewModel>(defaultPhrase);

            return View(vm);
        }

        // POST: DefaultPhrase/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(DefaultPhraseViewModel defaultPhraseViewModel)
        {
            if (!ModelState.IsValid)
            {
                var logger = _repo.GetByKey(defaultPhraseViewModel.Id) as Logger;
                SetCreatedModifiedInViewBag(logger);
                return View(defaultPhraseViewModel);
            }

            var defaultPhrase = _mapper.Map<DefaultPhrase>(defaultPhraseViewModel);
            defaultPhrase.ModifiedBy = AdHelper.Name;
            defaultPhrase.ModifiedOn = DateTime.Now;

            _repo.Update(defaultPhrase);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        // GET: DefaultPhrase/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var defaultPhrase = _repo.GetByKey(id);
            if (defaultPhrase == null || defaultPhrase.IsDeleted) return HttpNotFound();
            SetCreatedModifiedInViewBag(defaultPhrase);

            var vm = _mapper.Map<DefaultPhraseViewModel>(defaultPhrase);

            return View(vm);
        }

        // POST: DefaultPhrase/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (id <= 0) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var defaultPhrase = _repo.GetByKey(id);
            if (defaultPhrase == null || defaultPhrase.IsDeleted) return HttpNotFound();

            defaultPhrase.IsDeleted = true;
            _repo.Update(defaultPhrase);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

    }
}
