﻿#region Head
// <copyright file="SearchController.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using AutoMapper;
using Core.ApplicationServices;
using Core.DomainModel;
using Core.DomainServices;
using Presentation.Web.Filters;
using Presentation.Web.Models.Search;

namespace Presentation.Web.Controllers
{
    public class SearchController : BaseController
    {
        private readonly IMappingEngine  _mapper;
        private readonly ISearchService _searchService;
        private readonly IGenericRepository<Project> _projectRepository;


        public SearchController(IMappingEngine mapper, ISearchService searchService, IGenericRepository<Project> projectRepository)
        {
            _mapper = mapper;
            _searchService = searchService;
            _projectRepository = projectRepository;
        }

        // GET: Search
        [PopulateTopics]
        [PopulateCouncilsRaw]
        [PopulateGroups]
        public ActionResult Index(SearchViewModel viewModel)
        {
            if (viewModel == null)
            {
                viewModel = new SearchViewModel();
            }

            return View(viewModel);
        }

        // POST: Search
        [HttpPost, ActionName("Index")]
        [PopulateTopics]
        [PopulateCouncilsRaw]
        [PopulateGroups]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> IndexPost(SearchViewModel viewModel)
        {
            if (viewModel == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (!viewModel.AnyParametersSet())
            {
                ViewBag.NoSearchParameters = true;
                return View(viewModel);
            }

            var results = new ResultsViewModel();

            try
            {
                if (viewModel.SearchInProjects && viewModel.TopicId == null)
                {
                    var searchParameters = _mapper.Map<ProjectSearchParameters>(viewModel);
                    var projects = await _searchService.GetProjectsAsync(searchParameters);
                    if (projects != null)
                        results.Projects = _mapper.Map<IEnumerable<ProjectViewModel>>(projects);
                }

                if (viewModel.SearchInAgendaPoints && viewModel.CouncilId == null && viewModel.GroupId == null)
                {
                    var searchParameters = _mapper.Map<AgendaPointSearchParameters>(viewModel);
                    var agendaPoints = await _searchService.GetAgendaPointsAsync(searchParameters);
                    if (agendaPoints != null)
                        results.AgendaPoints = _mapper.Map<IEnumerable<AgendaPointViewModel>>(agendaPoints);
                }
            }
            catch (TaskCanceledException)
            {
                ViewBag.TimeOut = true;
                return View("Index", viewModel);
            }

            return View("Results", results);
        }

        public ActionResult PrintSearchResults(IEnumerable<string> projectIds)
        {
            var searchedProjects = projectIds.Select(projectId => _projectRepository.GetByKey(int.Parse(projectId, CultureInfo.InvariantCulture))).ToList();
            var vms = _mapper.Map<IEnumerable<ProjectViewModel>>(searchedProjects);

            return View(vms);
        }
    }
}
