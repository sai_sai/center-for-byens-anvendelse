﻿#region Head
// <copyright file="ProjectController.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using Core.DomainModel;
using Core.DomainServices;
using Presentation.Web.Filters;
using Presentation.Web.Helpers;
using Presentation.Web.Models;
using Presentation.Web.Models.Project;
using Presentation.Web.Models.Shared;

namespace Presentation.Web.Controllers
{
    public class ProjectController : BaseController
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IGenericRepository<Project> _projectRepository;
        private readonly IMappingEngine _mapper;
        private readonly IGenericRepository<ProjectDocument> _documentRepository;
        private readonly IDocumentHelper _documentHelper;
        private readonly IDateTime _dateTime;
        private readonly IGeoCoder _geoCoder;
        private readonly IDawaRepository _dawaRepo;

        public ProjectController(IUnitOfWork unitOfWork,
            IGenericRepository<Project> projectRepository,
            IMappingEngine mapper,
            IGenericRepository<ProjectDocument> documentRepository,
            IDocumentHelper documentHelper,
            IDateTime dateTime,
            IGeoCoder geoCoder,
            IDawaRepository dawaRepo)
        {
            _unitOfWork = unitOfWork;
            _projectRepository = projectRepository;
            _mapper = mapper;
            _documentRepository = documentRepository;
            _documentHelper = documentHelper;
            _dateTime = dateTime;
            _geoCoder = geoCoder;
            _dawaRepo = dawaRepo;
        }

        // GET: Project
        public ActionResult Index(bool onlyAwaitingProjects = true)
        {
            var projects = _projectRepository.Get();

            if (onlyAwaitingProjects)
                projects = projects.Where(p => p.ProjectStatus == ProjectStatusType.Awaiting);

            var vms = _mapper.Map<IEnumerable<IndexProjectViewModel>>(projects);

            ViewBag.onlyAwaitingProjects = onlyAwaitingProjects;

            return View(vms);
        }

        // GET: Project/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var project = _projectRepository.GetByKey(id);
            if (project == null)
            {
                return HttpNotFound();
            }

            SetCreatedModifiedInViewBag(project);
            var vm = _mapper.Map<IndexProjectViewModel>(project);

            return View(vm);
        }

        // GET: Project/Create
        [PopulateAdvisors]
        [PopulateConstructionPrograms]
        [PopulateGroups]
        [PopulateProjectCategories]
        [PopulateLocalCommunities]
        [PopulateCouncils]
        [PopulateProjectLeaders]
        [PopulateCompanies]
        public ActionResult Create()
        {
            var vm = new CreateProjectViewModel();
            return View(vm);
        }

        // POST: Project/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [PopulateAdvisors]
        [PopulateConstructionPrograms]
        [PopulateGroups]
        [PopulateProjectCategories]
        [PopulateLocalCommunities]
        [PopulateCouncils]
        [PopulateProjectLeaders]
        [PopulateCompanies]
        public ActionResult Create(CreateProjectViewModel createProjectViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(createProjectViewModel);
            }

            var project = _mapper.Map<Project>(createProjectViewModel);

            if (project.Documents != null)
            {
                var guidList = project.Documents.Select(o => o.Guid).ToList();
                project.Documents.Clear();

                // foreach guid in vm, get document from db and insert in created project
                foreach (var guid in guidList)
                {
                    var guid1 = guid;
                    var document = _documentRepository.Get(o => o.Guid == guid1).First();
                    project.Documents.Add(document);
                }
            }
            project.CreatedBy = AdHelper.Name;
            project.CreatedOn = _dateTime.Now;

            // add spartialobject
            project.AddressLocation = _geoCoder.GetSpartialObject(project.AddressId);

            // Fetch address details from server and save to local DB
            var addr = _dawaRepo.GetAddress(createProjectViewModel.AddressId);
            project.AddressStreetName = addr.StreetName;
            project.AddressNumber = addr.Number;

            _projectRepository.Insert(project);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        // GET: Project/Edit/5
        [PopulateAdvisors]
        [PopulateConstructionPrograms]
        [PopulateGroups]
        [PopulateProjectCategories]
        [PopulateLocalCommunities]
        [PopulateCouncils]
        [PopulateProjectLeaders]
        [PopulateCompanies]
        public ActionResult Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var project = _projectRepository.GetByKey(id);
            if (project == null) return HttpNotFound();

            SetCreatedModifiedInViewBag(project);
            var vm = _mapper.Map<CreateProjectViewModel>(project);

            return View(vm);
        }

        // POST: Project/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [PopulateAdvisors]
        [PopulateConstructionPrograms]
        [PopulateGroups]
        [PopulateProjectCategories]
        [PopulateLocalCommunities]
        [PopulateCouncils]
        [PopulateProjectLeaders]
        [PopulateCompanies]
        public ActionResult Edit(CreateProjectViewModel createProjectViewModel)
        {
            var project = _projectRepository.GetByKey(createProjectViewModel.Id);

            if (!ModelState.IsValid)
            {
                SetCreatedModifiedInViewBag(project);
                return View(createProjectViewModel);
            }

            if (createProjectViewModel.Documents != null && createProjectViewModel.Documents.Any())
            {
                var originalGuidsList = project.Documents.Select(o => o.Guid).ToList();
                var guidsList = createProjectViewModel.Documents.Select(o => o.Guid).ToList();

                // get guids for document(s) to remove
                var exceptGuids = originalGuidsList.Except(guidsList).ToList();

                // Delete removed documents
                foreach (var exceptGuid in exceptGuids)
                {
                    _documentRepository.DeleteByKey(exceptGuid);
                    DeleteFilesFromServer(exceptGuids.ToList());
                }

            }

            // Geocoding
            if (createProjectViewModel.AddressId != project.AddressId)
                createProjectViewModel.AddressLocation = _geoCoder.GetSpartialObject(createProjectViewModel.AddressId);

            _mapper.Map(createProjectViewModel, project);

            if (project.Documents != null)
            {
                var documentList = project.Documents.ToList();
                project.Documents.Clear();

                // foreach guid in vm, get document from db and insert in project
                foreach (var doc in documentList)
                {
                    var guid1 = doc.Guid;
                    var document = _documentRepository.Get(o => o.Guid == guid1).First();
                    document.ProjectId = project.Id;
                    document.Comment = doc.Comment;

                    // update document through documentRepository, because EF marks navigation properties of parent as unmodified
                    _documentRepository.Update(document);

                    project.Documents.Add(document);
                }
            }

            project.ModifiedBy = AdHelper.Name;
            project.ModifiedOn = _dateTime.Now;

            // Fetch address details from server and save to local DB
            var addr = _dawaRepo.GetAddress(createProjectViewModel.AddressId);
            project.AddressStreetName = addr.StreetName;
            project.AddressNumber = addr.Number;

            _projectRepository.Update(project);
            _unitOfWork.Save();
            return RedirectToAction("Details", new { id = project.Id });
        }

        // GET: Project/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var project = _projectRepository.GetByKey(id);
            SetCreatedModifiedInViewBag(project);
            var vm = _mapper.Map<IndexProjectViewModel>(project);

            return View(vm);
        }

        // POST: Project/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var documents = _documentRepository.Get(o => o.ProjectId == id).ToList();
            // delete all attached documents from server
            DeleteFilesFromServer(documents.Select(o => o.Guid));

            // delete doc from db
            foreach (var doc in documents)
            {
                _documentRepository.DeleteByKey(doc.Guid);
            }

            _projectRepository.DeleteByKey(id);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Search project repository for projects.
        /// </summary>
        /// <param name="id">Project's id.</param>
        /// <returns>Json array with Project's matching id.</returns>
        public ActionResult GetProjectsById(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var projects = _projectRepository.Get(p => p.Id == id);
            var select2Dtos = projects.Select(p => new ProjectDto { Id = p.Id, Name = p.Topic });

            return Json(select2Dtos, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveUploadedFile()
        {
            var fileObj = _documentHelper.UploadFile(Request, Server);
            if (fileObj == null) return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);

            // create document
            var document = new ProjectDocument
            {
                Comment = Request.Form["comment"],
                Guid = fileObj.Guid,
                FileName = fileObj.File.FileName,
                FileSize = fileObj.File.ContentLength,
                ProjectId = fileObj.ForeignKeyId
            };

            _documentRepository.Insert(document);
            _unitOfWork.Save();

            return Json(new { Guid = fileObj.Guid});;
        }

        /// <summary>
        /// For downloading files
        /// </summary>
        /// <param name="guid">The file id.</param>
        /// <returns>The found file.</returns>
        public FileResult Download(Guid guid)
        {
            return _documentHelper.Download(Server, guid);
        }

        /// <summary>
        /// Delete file from server
        /// </summary>
        /// <param name="fileGuidsToDelete"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DeleteFilesFromServer(IEnumerable<Guid> fileGuidsToDelete)
        {
            if (_documentHelper.DeleteFiles(fileGuidsToDelete, Server))
                return new HttpStatusCodeResult(HttpStatusCode.OK);

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }
    }
}
