﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using Core.DomainModel;
using Core.DomainServices;
using Presentation.Web.Attributes;
using Presentation.Web.Helpers;
using Presentation.Web.Models.ConstructionProgram;

namespace Presentation.Web.Controllers
{
    [AuthorizeRoles(AdHelper.Role.Admin)]
    public class ConstructionProgramController : BaseController
    {
        private readonly IGenericRepository<ConstructionProgram> _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMappingEngine _mapper;

        public ConstructionProgramController(IGenericRepository<ConstructionProgram> repo, IUnitOfWork unitOfWork, IMappingEngine mapper)
        {
            _repo = repo;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        // GET: ConstructionProgram
        public ActionResult Index()
        {
            var constructionPrograms = _repo.Get(c => !c.IsDeleted);

            var vms = _mapper.Map<IEnumerable<IndexConstructionProgramViewModel>>(constructionPrograms);

            return View(vms);
        }

        // GET: ConstructionProgram/Create
        public ActionResult Create()
        {
            var vm = new CreateConstructionProgramViewModel();
            return View(vm);
        }

        // POST: ConstructionProgram/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateConstructionProgramViewModel constructionProgramViewModel)
        {
            if (!ModelState.IsValid) return View(constructionProgramViewModel);

            var constructionProgram = _mapper.Map<ConstructionProgram>(constructionProgramViewModel);
            constructionProgram.CreatedOn = DateTime.Now;
            constructionProgram.CreatedBy = AdHelper.Name;

            _repo.Insert(constructionProgram);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        // GET: ConstructionProgram/Edit/{id}
        public ActionResult Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var constructionProgram = _repo.GetByKey(id);
            if (constructionProgram == null || constructionProgram.IsDeleted) return HttpNotFound();

            SetCreatedModifiedInViewBag(constructionProgram);

            var vm = _mapper.Map<EditConstructionProgramViewModel>(constructionProgram);

            return View(vm);
        }

        // POST: ConstructionProgram/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditConstructionProgramViewModel constructionProgramViewModel)
        {
            if (!ModelState.IsValid)
            {
                var logger = _repo.GetByKey(constructionProgramViewModel.Id) as Logger;
                SetCreatedModifiedInViewBag(logger);
                return View(constructionProgramViewModel);                
            }

            var constructionProgram = _mapper.Map<ConstructionProgram>(constructionProgramViewModel);
            constructionProgram.ModifiedBy = AdHelper.Name;
            constructionProgram.ModifiedOn = DateTime.Now;

            _repo.Update(constructionProgram);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        // GET: ConstructionProgram/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var constructionProgram = _repo.GetByKey(id);
            if (constructionProgram == null || constructionProgram.IsDeleted) return HttpNotFound();

            SetCreatedModifiedInViewBag(constructionProgram);

            var vm = _mapper.Map<DeleteConstructionProgramViewModel>(constructionProgram);

            return View(vm);
        }

        // POST: ConstructionProgram/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (id <= 0) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var constructionProgram = _repo.GetByKey(id);
            if (constructionProgram == null || constructionProgram.IsDeleted) return HttpNotFound();

            constructionProgram.IsDeleted = true;
            _repo.Update(constructionProgram);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }
    }
}
