﻿#region Head
// <copyright file="CouncilController.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using Core.DomainModel;
using Core.DomainServices;
using Presentation.Web.Attributes;
using Presentation.Web.Filters;
using Presentation.Web.Helpers;
using Presentation.Web.Models.Council;

namespace Presentation.Web.Controllers
{
    [AuthorizeRoles(AdHelper.Role.Admin)]
    public class CouncilController : BaseController
    {
        private readonly IGenericRepository<Council> _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMappingEngine _mapper;

        public CouncilController(IGenericRepository<Council> repo, IUnitOfWork unitOfWork, IMappingEngine mapper)
        {
            _repo = repo;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        // GET: Council
        public ActionResult Index()
        {
            var advisors = _repo.Get(c => !c.IsDeleted);

            var vms = _mapper.Map<IEnumerable<IndexCouncilViewModel>>(advisors);

            return View(vms);
        }

        // GET: Council/Create
        [PopulateGroups]
        public ActionResult Create()
        {
            var vm = new CreateCouncilViewModel();
            return View(vm);
        }

        // POST: Council/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateCouncilViewModel createCouncilViewModel)
        {
            if (!ModelState.IsValid) return View(createCouncilViewModel);

            var council = _mapper.Map<Council>(createCouncilViewModel);
            council.CreatedOn = DateTime.Now;
            council.CreatedBy = AdHelper.Name;

            _repo.Insert(council);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        // GET: Council/Edit/{id}
        [PopulateGroups]
        public ActionResult Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var council = _repo.GetByKey(id);
            if (council == null || council.IsDeleted) return HttpNotFound();
            SetCreatedModifiedInViewBag(council);

            var vm = _mapper.Map<EditCouncilViewModel>(council);

            return View(vm);
        }

        // POST: Advisors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditCouncilViewModel editAdvisorViewModel)
        {
            if (!ModelState.IsValid)
            {
                var logger = _repo.GetByKey(editAdvisorViewModel.Id) as Logger;
                SetCreatedModifiedInViewBag(logger);
                return View(editAdvisorViewModel);
            }

            var council = _mapper.Map<Council>(editAdvisorViewModel);
            council.ModifiedBy = AdHelper.Name;
            council.ModifiedOn = DateTime.Now;

            _repo.Update(council);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        // GET: Advisors/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var council = _repo.GetByKey(id);
            if (council == null || council.IsDeleted) return HttpNotFound();

            SetCreatedModifiedInViewBag(council);

            var vm = _mapper.Map<IndexCouncilViewModel>(council);

            return View(vm);
        }

        // POST: Advisors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (id <= 0) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var council = _repo.GetByKey(id);
            if (council == null || council.IsDeleted) return HttpNotFound();

            council.IsDeleted = true;

            _repo.Update(council);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        public ActionResult CouncilsWithGroupId(int? groupId)
        {
            IEnumerable<Council> councils;

            if (groupId != null)
                councils = _repo.Get(o => o.GroupId == groupId);
            else
                councils = _repo.Get();

            var dtos = _mapper.Map<IEnumerable<CouncilDTO>>(councils);
            return Json(dtos, JsonRequestBehavior.AllowGet);
        }
    }
}
