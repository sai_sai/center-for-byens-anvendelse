﻿#region Head
// <copyright file="AdvisorController.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using Core.DomainModel;
using Core.DomainServices;
using Presentation.Web.Attributes;
using Presentation.Web.Filters;
using Presentation.Web.Helpers;
using Presentation.Web.Models.Advisor;

namespace Presentation.Web.Controllers
{
    [AuthorizeRoles(AdHelper.Role.Admin)]
    public class AdvisorController : BaseController
    {
        private readonly IGenericRepository<Advisor> _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMappingEngine _mapper;

        public AdvisorController(IGenericRepository<Advisor> repo, IUnitOfWork unitOfWork, IMappingEngine mapper)
        {
            _repo = repo;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        // GET: Advisor
        public ActionResult Index()
        {
            var advisors = _repo.Get(a => !a.IsDeleted);

            var vms = _mapper.Map<IEnumerable<IndexAdvisorViewModel>>(advisors);

            return View(vms);
        }

        // GET: Advisor/:companyId
        // Returns all advisors with company id
        public ActionResult AdvisorsWithCompanyId(int? companyId)
        {
            var advisors = _repo.Get(a => !a.IsDeleted && a.CompanyId == companyId);
            var advisorDTOs = _mapper.Map<IEnumerable<AdvisorDTO>>(advisors);
            return Json(advisorDTOs, JsonRequestBehavior.AllowGet);
        }

        // GET: Advisor/Create
        [PopulateCompanies]
        public ActionResult Create()
        {
            var vm = new CreateAdvisorViewModel();
            return View(vm);
        }

        // POST: Advisor/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [PopulateCompanies]
        public ActionResult Create(CreateAdvisorViewModel addAdvisorViewModel)
        {
            if (!ModelState.IsValid) return View(addAdvisorViewModel);

            var advisor = _mapper.Map<Advisor>(addAdvisorViewModel);
            advisor.CreatedOn = DateTime.Now;
            advisor.CreatedBy = AdHelper.Name;

            _repo.Insert(advisor);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        // GET: Advisor/Edit/{id}
        [PopulateCompanies]
        public ActionResult Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var advisor = _repo.GetByKey(id);
            if (advisor == null || advisor.IsDeleted) return HttpNotFound();

            SetCreatedModifiedInViewBag(advisor);

            var vm = _mapper.Map<EditAdvisorViewModel>(advisor);

            return View(vm);
        }

        // POST: Advisors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [PopulateCompanies]
        public ActionResult Edit(EditAdvisorViewModel editAdvisorViewModel)
        {
            if (!ModelState.IsValid)
            {
                var logger = _repo.GetByKey(editAdvisorViewModel.Id) as Logger;
                SetCreatedModifiedInViewBag(logger);
                return View(editAdvisorViewModel);
            }

            var advisor = _mapper.Map<Advisor>(editAdvisorViewModel);
            advisor.ModifiedOn = DateTime.Now;
            advisor.ModifiedBy = AdHelper.Name;

            _repo.Update(advisor);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        // GET: Advisors/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var advisor = _repo.GetByKey(id);
            if (advisor == null || advisor.IsDeleted) return HttpNotFound();
            SetCreatedModifiedInViewBag(advisor);

            var vm = _mapper.Map<DeleteAdvisorViewModel>(advisor);

            return View(vm);
        }

        // POST: Advisors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (id <= 0) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var advisor = _repo.GetByKey(id);
            if (advisor == null || advisor.IsDeleted) return HttpNotFound();

            advisor.IsDeleted = true;

            _repo.Update(advisor);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }
    }
}
