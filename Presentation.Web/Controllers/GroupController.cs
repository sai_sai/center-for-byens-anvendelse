﻿#region Head
// <copyright file="GroupController.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using Core.DomainModel;
using Core.DomainServices;
using Presentation.Web.Attributes;
using Presentation.Web.Helpers;
using Presentation.Web.Models.Group;

namespace Presentation.Web.Controllers
{
    [AuthorizeRoles(AdHelper.Role.Admin)]
    public class GroupController : BaseController
    {
        private readonly IGenericRepository<Group> _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMappingEngine _mapper;

        public GroupController(IGenericRepository<Group> repo, IUnitOfWork unitOfWork, IMappingEngine mapper)
        {
            _repo = repo;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        // GET: Group
        public ActionResult Index()
        {
            var groups = _repo.Get(g => !g.IsDeleted);

            var vm = _mapper.Map<IEnumerable<IndexGroupViewModel>>(groups);

            return View(vm);
        }

        // GET: Group/Create
        public ActionResult Create()
        {
            var vm = new CreateGroupViewModel();
            return View(vm);
        }

        // POST: Group/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateGroupViewModel createGroupViewModel)
        {
            if (!ModelState.IsValid) return View(createGroupViewModel);

            var group = _mapper.Map<Group>(createGroupViewModel);

            group.CreatedOn = DateTime.Now;
            group.CreatedBy = AdHelper.Name;
            _repo.Insert(group);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        // GET: Group/Edit/{id}
        public ActionResult Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var group = _repo.GetByKey(id);
            if (group == null || group.IsDeleted) return HttpNotFound();
            SetCreatedModifiedInViewBag(group);

            var vm = _mapper.Map<EditGroupViewModel>(group);

            return View(vm);
        }

        // POST: Groups/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditGroupViewModel editGroupViewModel)
        {
            if (!ModelState.IsValid)
            {
                var logger = _repo.GetByKey(editGroupViewModel.Id) as Logger;
                SetCreatedModifiedInViewBag(logger);
                return View(editGroupViewModel);
            }

            var group = _mapper.Map<Group>(editGroupViewModel);
            group.ModifiedBy = AdHelper.Name;
            group.ModifiedOn = DateTime.Now;

            _repo.Update(group);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        // GET: Groups/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var group = _repo.GetByKey(id);
            if (group == null || group.IsDeleted)
                return HttpNotFound();

            if (group.Councils.Any(c => c.IsDeleted == false))
                return View("DeleteError");

            SetCreatedModifiedInViewBag(group);

            var vm = _mapper.Map<DeleteGroupViewModel>(group);

            return View(vm);
        }

        // POST: Groups/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (id <= 0)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var group = _repo.GetByKey(id);
            if (group == null || group.IsDeleted)
                return HttpNotFound();

            if (group.Councils.Any(c => c.IsDeleted == false))
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            group.IsDeleted = true;
            _repo.Update(group);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }
    }
}