﻿#region Head
// <copyright file="ProjectLeaderController.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using Core.DomainModel;
using Core.DomainServices;
using Presentation.Web.Attributes;
using Presentation.Web.Helpers;
using Presentation.Web.Models.ProjectLeader;

namespace Presentation.Web.Controllers
{
    [AuthorizeRoles(AdHelper.Role.Admin)]
    public class ProjectLeaderController : BaseController
    {
        private readonly IGenericRepository<ProjectLeader> _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMappingEngine _mapper;

        public ProjectLeaderController(IGenericRepository<ProjectLeader> repo, IUnitOfWork unitOfWork, IMappingEngine mapper)
        {
            _repo = repo;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        // GET: ProjectLeader
        public ActionResult Index()
        {
            var projectLeaders = _repo.Get(a => !a.IsDeleted);

            var vms = _mapper.Map<IEnumerable<IndexProjectLeaderViewModel>>(projectLeaders);

            return View(vms);
        }

        // GET: ProjectLeader/Create
        public ActionResult Create()
        {
            var vm = new CreateProjectLeaderViewModel();
            return View(vm);
        }

        // POST: ProjectLeader/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateProjectLeaderViewModel projectLeaderViewModel)
        {
            if (!ModelState.IsValid) return View(projectLeaderViewModel);

            var projectLeader = _mapper.Map<ProjectLeader>(projectLeaderViewModel);
            projectLeader.CreatedOn = DateTime.Now;
            projectLeader.CreatedBy = AdHelper.Name;

            _repo.Insert(projectLeader);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        // GET: ProjectLeader/Edit/{id}
        public ActionResult Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var projectLeader = _repo.GetByKey(id);
            if (projectLeader == null || projectLeader.IsDeleted) return HttpNotFound();
            SetCreatedModifiedInViewBag(projectLeader);

            var vm = _mapper.Map<EditProjectLeaderViewModel>(projectLeader);

            return View(vm);
        }

        // POST: ProjectLeader/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditProjectLeaderViewModel editProjectLeaderViewModel)
        {
            if (!ModelState.IsValid)
            {
                var logger = _repo.GetByKey(editProjectLeaderViewModel.Id) as Logger;
                SetCreatedModifiedInViewBag(logger);
                return View(editProjectLeaderViewModel);
            }

            var projectLeader = _mapper.Map<ProjectLeader>(editProjectLeaderViewModel);
            projectLeader.ModifiedBy = AdHelper.Name;
            projectLeader.ModifiedOn = DateTime.Now;

            _repo.Update(projectLeader);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        // GET: ProjectLeader/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var projectLeader = _repo.GetByKey(id);
            if (projectLeader == null || projectLeader.IsDeleted) return HttpNotFound();
            SetCreatedModifiedInViewBag(projectLeader);

            var vm = _mapper.Map<IndexProjectLeaderViewModel>(projectLeader);

            return View(vm);
        }

        // POST: ProjectLeader/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (id <= 0) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var projectLeader = _repo.GetByKey(id);
            if (projectLeader == null || projectLeader.IsDeleted) return HttpNotFound();

            projectLeader.IsDeleted = true;

            _repo.Update(projectLeader);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }
    }
}
