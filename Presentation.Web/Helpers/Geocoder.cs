﻿using System;
using System.Data.Entity.Spatial;
using System.IO;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Presentation.Web.Helpers
{
    public class GeoCoder : IGeoCoder
    {

        /// <summary>
        /// Get the spartial object from an addressId
        /// </summary>
        /// <param name="addressId"></param>
        /// <returns></returns>
        public DbGeometry GetSpartialObject(Guid addressId)
        {
            var requestUrl = "http://dawa.aws.dk/adresser/" + addressId + "?srid=25832";

            // http get
            var response = MakeRequest(requestUrl);
            var jsonObj = JObject.Parse(response);

            // get the spartial object (adgangsadresse.adgangspunkt.koordinater)
            var point = jsonObj["adgangsadresse"]["adgangspunkt"]["koordinater"];

            // Formatting none for not converting . to ,
            var longitude = point[0].ToString(Formatting.None);
            var latitude = point[1].ToString(Formatting.None);

            var coordinates = ConvertLatLonToDDbGeometry(longitude, latitude);
            return coordinates;
        }

        private string MakeRequest(string requestUrl)
        {
            string url = requestUrl;
            var request = (System.Net.HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            //using GET - request.Headers.Add ("Authorization","Authorizaation value");
            request.ContentType = "application/json";

            request.PreAuthenticate = true;
            request.Credentials = new NetworkCredential("admin", "GaICtiyzJWWxvcOrheVfrawNNnDbek57");

            using (var response = request.GetResponse())
            {
                string responseText;
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    responseText = reader.ReadToEnd();
                    }
                return responseText;
            }
        }

        public static DbGeometry ConvertLatLonToDDbGeometry(string longitude, string latitude)
        {
            var point = string.Format("POINT({1} {0})", latitude, longitude);
            return DbGeometry.FromText(point);
        }
    }
}
