﻿using System.Web;

namespace Presentation.Web.Helpers
{
    public class HttpContextHelper : IHttpContext
    {
        public bool IsLocal()
        {
            return HttpContext.Current.Request.IsLocal;
        }
    }
}