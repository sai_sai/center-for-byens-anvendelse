﻿#region Head
// <copyright file="AdHelper.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Web.Helpers
{
    /// <summary>
    /// Active Directory helper to access the Identity framework.
    /// </summary>
    public static class AdHelper
    {
        /// <summary>
        /// Contains the values of user groups for the application.
        /// </summary>
        public enum Role
        {
            Admin,
            User
        }

        /// <summary>
        /// Determines whether the current user belongs in the specified role.
        /// </summary>
        /// <param name="role">The role.</param>
        /// <returns>True if user belongs in role otherwise false.</returns>
        public static bool InRole(Role role)
        {
            var roles = GetRolesFromAppSettings(role);
            return roles.Any(r => HttpContext.Current.User.IsInRole(r));
        }

        /// <summary>
        /// Gets a value that indicates whether a user has been authenticated.
        /// </summary>
        public static bool IsAuthenticated {
            get { return HttpContext.Current.User.Identity.IsAuthenticated; }
        }

        /// <summary>
        /// Gets the name of the current user.
        /// </summary>
        public static string Name
        {
            get { return HttpContext.Current.User.Identity.Name; }
        }

        /// <summary>
        /// Get AD roles as a comma separated list.
        /// </summary>
        /// <param name="role">The role group.</param>
        /// <returns>Comma separated list of role names.</returns>
        public static string GetRoles(Role role)
        {
            return String.Join(",", GetRolesFromAppSettings(role));
        }

        /// <summary>
        /// Get roles from <see cref="AppSettings"/>.
        /// </summary>
        /// <param name="role">The role group.</param>
        /// <returns>IEnumerable of strings with each role.</returns>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when the argument is out of range.</exception>
        private static IEnumerable<string> GetRolesFromAppSettings(Role role)
        {
            IEnumerable<string> roles;
            switch (role)
            {
                case Role.Admin:
                    roles = AppSettings.AdminRoles;
                    break;
                case Role.User:
                    roles = AppSettings.UserRoles;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("role");
            }
            return roles;
        }
    }
}
