﻿#region Head
// <copyright file="DocumentHelper.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.Abstractions;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;

namespace Presentation.Web.Helpers
{
    public class DocumentHelper : IDocumentHelper
    {
        private readonly IGenericRepository<Document> _documentRepository;
        private readonly IFileSystem _fileSystem;
        private readonly IAppSettings _appSettings;
        private readonly IHttpContext _HttpContext;

        public DocumentHelper(IGenericRepository<Document> documentRepository, IFileSystem fileSystem, IAppSettings appSettings, IHttpContext httpContext)
        {
            _documentRepository = documentRepository;
            _fileSystem = fileSystem;
            _appSettings = appSettings;
            _HttpContext = httpContext;
        }

        /// <summary>
        /// For uploading files: Saves file on server and creates it in db.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="server">The server property of the controller</param>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        public FileObject UploadFile(HttpRequestBase request, HttpServerUtilityBase server)
        {
            if (request == null)
                return null;

            var isSavedSuccessfully = false;
            var guid = new Guid();
            int id;
            HttpPostedFileBase file = null;

            // TODO: Don't suppress all exceptions here
            try
            {
                foreach (string fileName in request.Files)
                {
                    file = request.Files[fileName];
                    if (file == null)
                        continue;

                    //Save file content goes here
                    guid = Guid.NewGuid();

                    var path = GetUploadPathString(server, _appSettings);

                    if (!Directory.Exists(path))
                        Directory.CreateDirectory(path);

                    file.SaveAs(Path.Combine(path, guid.ToString()));
                    isSavedSuccessfully = true;
                }
            }
            catch (Exception ex)
            {
                isSavedSuccessfully = false;
            }
            if (!int.TryParse(request.Form["id"], out id))
                return new FileObject {Guid = guid, File = file, ForeignKeyId = null};

            return isSavedSuccessfully ? new FileObject { Guid = guid, File = file, ForeignKeyId = id} : null;
        }

        /// <summary>
        /// For downloading files
        /// </summary>
        /// <param name="server"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public FileResult Download(HttpServerUtilityBase server, Guid id)
        {
            var pathString = GetUploadPathString(server, _appSettings);
            var filePath = pathString + "/" + id;
            var fileBytes = _fileSystem.File.ReadAllBytes(filePath);
            var response = new FileContentResult(fileBytes, "application/octet-stream");

            // get filename from db
            var fileName = _documentRepository.Get(o => o.Guid == id).Select(o => o.FileName).First();
            response.FileDownloadName = fileName;
            return response;
        }

        public bool DeleteFiles(IEnumerable<Guid> fileGuidsToDelete, HttpServerUtilityBase server)
        {
            if (server == null)
            {
                throw new ArgumentNullException("server");
            }

            foreach (var guidToDelete in fileGuidsToDelete)
            {
                // Getting directory for uploads
                var pathString = GetUploadPathString(server, _appSettings);

                var filePath = pathString + "/" + guidToDelete;
                if (_fileSystem.File.Exists(filePath))
                {
                    _fileSystem.File.Delete(filePath);
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Gets the the upload path as string
        /// </summary>
        /// <param name="server"></param>
        /// <returns></returns>
        private string GetUploadPathString(HttpServerUtilityBase server, IAppSettings appSettings)
        {
            if (appSettings == null) return "";

            return Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
                                appSettings.UploadPath);
        }
    }
}
