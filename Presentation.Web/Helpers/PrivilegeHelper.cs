﻿using Core.DomainModel;
using Core.DomainServices;

namespace Presentation.Web.Helpers
{
    /// <summary>
    /// Helper for determing specific privileges for roles
    /// </summary>
    public class PrivilegeHelper
    {
        private readonly IGenericRepository<AgendaPoint> _agendapointRepository;

        public PrivilegeHelper(IGenericRepository<AgendaPoint> agendapointRepository)
        {
            _agendapointRepository = agendapointRepository;
        }

        /// <summary>
        /// For checking if an Agendapoint can be deleted by the current user.
        /// An agendapoint can be deleted if:
        /// 1: It is not attached to a meeting (MeetingAgendapoint should be zero)
        /// 2: The current user is the creater of the agendapoint (CreatedBy)
        /// 3: The current user is administrator
        /// </summary>
        /// <returns></returns>
        public bool IsAgendaPointIsDeleteableForCurrentUser(int agendaPointId)
        {
            var agendaPoint = _agendapointRepository.GetByKey(agendaPointId);
            var isAdmin = AdHelper.InRole(AdHelper.Role.Admin);

            // if agendapoint not attached to a meeting return true &&
            // if current user is creator || if current user admin, return true
            if (agendaPoint.MeetingAgendaPoint == null && (agendaPoint.CreatedBy == AdHelper.Name || isAdmin))
                return true;

            return false;
        }
    }
}