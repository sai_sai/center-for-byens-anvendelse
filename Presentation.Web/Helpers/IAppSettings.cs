﻿#region Head
// <copyright file="IAppSettings.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

namespace Presentation.Web.Helpers
{
    public interface IAppSettings
    {
        string AgendaPointZeroAddressId { get; }
        string AgendaPointZeroDescription { get; }
        string UploadPath { get; }
    }
}
