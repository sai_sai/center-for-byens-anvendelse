﻿using System;
using System.Web;

namespace Presentation.Web.Helpers
{
    public class FileObject
    {
        public Guid Guid { get; set; }
        public int? ForeignKeyId { get; set; }
        public HttpPostedFileBase File { get; set; }
    }
}