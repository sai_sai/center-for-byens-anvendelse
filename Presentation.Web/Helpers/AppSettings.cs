﻿#region Head
// <copyright file="AppSettings.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.Configuration;

namespace Presentation.Web.Helpers
{
    /// <summary>
    /// Access AppSettings configuration.
    /// </summary>
    public class AppSettings : IAppSettings
    {
        public static string AppName
        {
            get { return ConfigurationManager.AppSettings["appName"]; }
        }

        public static IEnumerable<string> AdminRoles
        {
            get { return ConfigurationManager.AppSettings["adAdminRoles"].Split(new []{';'}, StringSplitOptions.RemoveEmptyEntries); }
        }

        public static IEnumerable<string> UserRoles
        {
            get { return ConfigurationManager.AppSettings["adUserRoles"].Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries); }
        }

        public string AgendaPointZeroAddressId
        {
            get { return ConfigurationManager.AppSettings["AgendaPointZeroAddressId"]; }
        }

        public string AgendaPointZeroDescription
        {
            get { return ConfigurationManager.AppSettings["AgendaPointZeroDescription"]; }
        }

        public string UploadPath
        {
            get { return ConfigurationManager.AppSettings["UploadPath"]; }
        }
    }
}
