﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Web.Models.Shared
{
    public enum AreaTypeViewModel
    {
        Midt,
        Nord,
        Vest,
        Syd
    }
}