﻿#region Head
// <copyright file="StreetTypeViewModel.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System.ComponentModel.DataAnnotations;

namespace Presentation.Web.Models.Shared
{
    public enum StreetTypeViewModel
    {
        [Display(Name = "Privat vej")]
        PrivateStreet,

        [Display(Name = "Offentlig vej")]
        PublicStreet,

        [Display(Name = "Privat/fælles vej")]
        SharedStreet
    }
}
