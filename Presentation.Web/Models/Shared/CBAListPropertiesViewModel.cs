﻿using System.ComponentModel;

namespace Presentation.Web.Models.Shared
{
    /// <summary>
    /// A viewmodel for default list properties
    /// </summary>
    public abstract class CbaListPropertiesViewModel : LoggerViewModel
    {
        [DisplayName("Aktiv")]
        public bool IsActive { get; set; }
    }
}
