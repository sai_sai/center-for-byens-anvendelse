﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Presentation.Web.Models.Shared
{
    public abstract class LoggerViewModel
    {
        [DisplayName("Oprettet")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm:ss}")]
        public DateTime CreatedOn { get; set; }

        [DisplayName("Oprettet af")]
        public string CreatedBy { get; set; }

        [DisplayName("Redigeret")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm:ss}")]
        public DateTime? ModifiedOn { get; set; }

        [DisplayName("Redigeret af")]
        public string ModifiedBy { get; set; }
    }
}
