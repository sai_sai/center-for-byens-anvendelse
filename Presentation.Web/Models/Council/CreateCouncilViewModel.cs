﻿#region Head
// <copyright file="CreateCouncilViewModel.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Presentation.Web.Models.Shared;

namespace Presentation.Web.Models.Council
{
    public class CreateCouncilViewModel : CbaListPropertiesViewModel
    {
        public CreateCouncilViewModel()
        {
            IsActive = true;
        }
        [DisplayName("Navn*")]
        [Required(ErrorMessage = "Skriv navnet på fællesrådet.")]
        public string Name { get; set; }

        [DisplayName("Gruppe")]
        public int GroupId { get; set; }
    }
}
