﻿#region Head
// <copyright file="InvitedPersonViewModel.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System.ComponentModel.DataAnnotations;
using Presentation.Web.Models.Shared;

namespace Presentation.Web.Models.InvitedPerson
{
    public class InvitedPersonViewModel : CbaListPropertiesViewModel
    {
        public InvitedPersonViewModel()
        {
            IsActive = true;
        }
        public int Id { get; set; }
        [Display (Name = "Navn*")]
        [Required(ErrorMessage = "Navn er påkrævet.")]
        public string Name { get; set; }
    }
}
