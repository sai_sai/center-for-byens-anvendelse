﻿#region Head
// <copyright file="AdvisorDTO.cs" company="IT Minds" year="2016">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
namespace Presentation.Web.Models.Advisor
{
    public class AdvisorDTO
    {
        public int Id { get; set; }
        public string ResponsiblePersonName { get; set; }
    }
}
