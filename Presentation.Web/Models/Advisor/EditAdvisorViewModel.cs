﻿#region Head
// <copyright file="EditAdvisorViewModel.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Presentation.Web.Models.Shared;

namespace Presentation.Web.Models.Advisor
{
    public class EditAdvisorViewModel : CbaListPropertiesViewModel
    {
        public int Id { get; set; }

        [DisplayName("Virksomhed*")]
        [Required(ErrorMessage = "Virksomhedsnavn er påkrævet.")]
        public int CompanyId { get; set; }

        [DisplayName("Kontaktperson*")]
        [Required(ErrorMessage = "Kontaktperson er påkrævet.")]
        public string ResponsiblePersonName { get; set; }

        [DisplayName("Telefonnummer*")]
        [Required(ErrorMessage = "Telefonnummer er påkrævet.")]
        public string ResponsiblePersonPhoneNumber { get; set; }
    }
}
