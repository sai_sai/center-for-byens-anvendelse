﻿#region Head
// <copyright file="DefaultPhraseViewModel.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Presentation.Web.Models.Shared;

namespace Presentation.Web.Models.DefaultPhrase
{
    public class DefaultPhraseViewModel : CbaListPropertiesViewModel
    {
        public DefaultPhraseViewModel()
        {
            IsActive = true;
        }
        public int Id { get; set; }

        [DataType(DataType.MultilineText)]
        [DisplayName("Standardfrase*")]
        [Required(ErrorMessage = "Skriv en standardfrase.")]
        public string Phrase { get; set; }
    }
}
