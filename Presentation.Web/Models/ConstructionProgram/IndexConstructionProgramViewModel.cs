﻿#region Head
// <copyright file="IndexConstructionProgramViewModel.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System.ComponentModel;
using Presentation.Web.Models.Shared;

namespace Presentation.Web.Models.ConstructionProgram
{
    public class IndexConstructionProgramViewModel : CbaListPropertiesViewModel
    {
        public int Id { get; set; }

        [DisplayName("År med projekter involveret")]
        public int YearOfProjectInvolvement { get; set; }
    }
}
