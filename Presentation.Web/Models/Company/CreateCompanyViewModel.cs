﻿#region Head
// <copyright file="CreateCompanyViewModel.cs" company="IT Minds" year="2016">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using Presentation.Web.Models.Shared;

namespace Presentation.Web.Models.Company
{
    public class CreateCompanyViewModel : CbaListPropertiesViewModel
    {
        public CreateCompanyViewModel()
        {
            IsActive = true;
        }
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
    }
}