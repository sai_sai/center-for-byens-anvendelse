﻿#region Head
// <copyright file="ConstructionProgramViewModel.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System.ComponentModel;

namespace Presentation.Web.Models.Project
{
    public class ConstructionProgramViewModel
    {
        public int Id { get; set; }
        [DisplayName("År med projekt involveret")]
        public int YearOfProjectInvolvement { get; set; }
    }
}
