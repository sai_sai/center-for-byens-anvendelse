﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Presentation.Web.Models.Project
{
    public class ProjectDocumentViewModel
    {
        public Guid Guid { get; set; }
        public string FileName { get; set; }
        public float FileSize { get; set; }

        [DataType (DataType.MultilineText)]
        public string Comment { get; set; }
        public int? ProjectId { get; set; }
    }
}
