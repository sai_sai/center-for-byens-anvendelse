﻿#region Head
// <copyright file="CreateProjectViewModel.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Spatial;
using Presentation.Web.Filters;
using Presentation.Web.Models.Shared;
using Presentation.Web.Validators;

namespace Presentation.Web.Models.Project
{
    public class CreateProjectViewModel :   LoggerViewModel,
                                            ISelectedAdvisor,
                                            ISelectedConstructionProgram,
                                            ISelectedCouncil,
                                            ISelectedGroup,
                                            ISelectedLocalCommunity,
                                            ISelectedProjectCategory,
                                            ISelectedProjectLeader
    {
        public CreateProjectViewModel()
        {
            Documents = new List<ProjectDocumentViewModel>();
            SearchInAarhus = true;
            StreetType = StreetTypeViewModel.PublicStreet;
            ExpectedStartDate = DateTime.Now;
            ExpectedEndDate = DateTime.Now;

            OneYearReview = DateTime.Now;
            OneYearReview.AddYears(1);
            FiveYearReview = DateTime.Now;
            FiveYearReview.AddYears(5);
        }

        public int Id { get; set; }

        [Display(Name = "Emne*")]
        [Required(ErrorMessage = "Angiv venligst projektets emne.")]
        public string Topic { get; set; }

        [UIHint ("Date")]
        [Display(Name = "Forventet startdato*")]
        [Required(ErrorMessage = "Angiv venligst projektets startstidspunkt.")]
        public DateTime ExpectedStartDate { get; set; }

        [UIHint("Date")]
        [Display(Name = "Forventet slutdato*")]
        [Required(ErrorMessage = "Angiv venligst projektets slutstidspunkt.")]
        public DateTime ExpectedEndDate { get; set; }

        [Display(Name = "Beskrivelse")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [Display(Name = "Note")]
        [DataType(DataType.MultilineText)]
        public string Note { get; set; }

        [Display(Name = "Vejtype")]
        public StreetTypeViewModel StreetType { get; set; }

        [Display(Name = "Projektleder")]
        public int? ProjectLeaderId { get; set; }

        [Display(Name = "Rådsprioritet*")]
        [Required(ErrorMessage = "Angiv venligst projektets rådsprioritet.")]
        [Range(1, 10, ErrorMessage = "Prioriteten skal være mellem 1 og 10.")]
        public int CouncilPriority { get; set; }

        [Display(Name = "Kommuneprioritet*")]
        [Required(ErrorMessage = "Angiv venligst projektets kommuneprioritet.")]
        [Range(1, 10, ErrorMessage = "Prioriteten skal være mellem 1 og 10.")]
        public int MunicipalityPriority { get; set; }

        [Display(Name = "Adresse")]
        public Guid AddressId { get; set; }

        public DbGeometry AddressLocation { get; set; }

        [Display(Name = "Foreløbig adresse?")]
        public bool TemporaryAddress { get; set; }

        [Display(Name = "Søg i Aarhus?")]
        public bool SearchInAarhus { get; set; }

        [Display(Name = "Budget*")]
        [Required(ErrorMessage = "Angiv venligst projektets budget.")]
        [ThousandsNumber(ErrorMessage = "Brug punktum som seperator. Min/max værdi er 2.147.483.647")]
        public string Budget { get; set; }

        [Display(Name = "Status")]
        public ProjectStatusTypeViewModel ProjectStatus { get; set; }

        [Display(Name = "Rådgiver")]
        public int? AdvisorId { get; set; }

        [Display(Name = "Projektkategori")]
        public int? ProjectCategoryId { get; set; }

        [Display(Name = "Gruppe")]
        public int? GroupId { get; set; }

        [Display(Name = "Anlægsprogram")]
        public int? ConstructionProgramId { get; set; }

        [Display(Name = "Lokalsamfund")]
        public int? LocalCommunityId { get; set; }

        [Display(Name = "Fællesråd")]
        public int? CouncilId { get; set; }

        [UIHint("Date")]
        [Display(Name = "1 års gennemgang")]
        public DateTime OneYearReview { get; set; }

        [UIHint("Date")]
        [Display(Name = "5 års gennemgang")]
        public DateTime FiveYearReview { get; set; }

        public IList<ProjectDocumentViewModel> Documents { get; set; }
    }
}
