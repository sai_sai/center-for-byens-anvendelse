﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Presentation.Web.Models.Advisor;
using Presentation.Web.Models.Council;
using Presentation.Web.Models.Group;
using Presentation.Web.Models.LocalCommunity;
using Presentation.Web.Models.ProjectCategory;
using Presentation.Web.Models.ProjectLeader;
using Presentation.Web.Models.Shared;

namespace Presentation.Web.Models.Project
{
    public class IndexProjectViewModel : LoggerViewModel
    {
        [Display(Name = "Nr.")]
        public int Id { get; set; }

        [Display(Name = "Emne")]
        public string Topic { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}")]
        [Display(Name = "Forventede startsdato")]
        public DateTime ExpectedStartDate { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}")]
        [Display(Name = "Forventede slutsdato")]
        public DateTime ExpectedEndDate { get; set; }

        [Display(Name = "Beskrivelse")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [Display(Name = "Note")]
        [DataType(DataType.MultilineText)]
        public string Note { get; set; }

        [Display(Name = "Vejtype")]
        public StreetTypeViewModel StreetType { get; set; }

        [Display(Name = "Projektleder")]
        public string ProjectLeaderName { get; set; }

        [Display(Name = "Fællesrådsprioritet")]
        public int CouncilPriority { get; set; }

        [Display(Name = "Kommuneprioritet")]
        public int MunicipalityPriority { get; set; }

        [Display(Name = "Adresse")]
        public Guid AddressId { get; set; }

        [Display(Name = "Foreløbig adresse?")]
        public bool TemporaryAddress { get; set; }

        [Display(Name = "Budget")]
        [DisplayFormat(DataFormatString = "{0:N0}")]
        public int Budget { get; set; }

        [Display(Name = "Status")]
        public ProjectStatusTypeViewModel ProjectStatus { get; set; }

        [Display(Name = "Projektleder")]
        public IndexProjectLeaderViewModel ProjectLeader { get; set; }

        [Display(Name = "Rådgiver")]
        public IndexAdvisorViewModel Advisor { get; set; }

        [Display(Name = "Projektkategori")]
        public IndexProjectCategoryViewModel ProjectCategory { get; set; }

        [Display(Name = "Gruppe")]
        public IndexGroupViewModel Group { get; set; }

        [Display(Name = "Anlægsprogram")]
        public ConstructionProgramViewModel ConstructionProgram { get; set; }

        [Display(Name = "Lokalsamfund")]
        public IndexLocalCommunityViewModel LocalCommunity { get; set; }

        [Display(Name = "Fællesråd")]
        public IndexCouncilViewModel Council { get; set; }

        [Display(Name = "Dokumenter")]
        public IList<ProjectDocumentViewModel> Documents { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}")]
        [Display(Name = "1 års gennemgang")]
        public DateTime OneYearReview { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}")]
        [Display(Name = "5 års gennemgang")]
        public DateTime FiveYearReview { get; set; }
    }
}
