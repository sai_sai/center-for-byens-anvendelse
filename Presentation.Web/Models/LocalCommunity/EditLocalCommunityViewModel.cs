﻿#region Head
// <copyright file="IndexLocalCommunityViewModel.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Presentation.Web.Models.Shared;

namespace Presentation.Web.Models.LocalCommunity
{
    public class EditLocalCommunityViewModel : CbaListPropertiesViewModel
    {
        public int Id { get; set; }

        [DisplayName("Nummer*")]
        [Required(ErrorMessage = "Feltet Nummer må ikke være tomt.")]
        public int Number { get; set; }

        [DisplayName("Navn*")]
        [Required(ErrorMessage = "Feltet Navn må ikke være tomt.")]
        public string Name { get; set; }
    }
}
