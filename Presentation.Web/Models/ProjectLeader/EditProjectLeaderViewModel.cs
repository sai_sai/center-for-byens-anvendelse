﻿#region Head
// <copyright file="EditProjectLeaderViewModel.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Presentation.Web.Models.Shared;

namespace Presentation.Web.Models.ProjectLeader
{
    public class EditProjectLeaderViewModel : CbaListPropertiesViewModel
    {
        public int Id { get; set; }

        [DisplayName("Navn*")]
        [Required(ErrorMessage = "Navn er påkrævet")]
        public string Name { get; set; }

        [DisplayName("Initialer*")]
        [Required(ErrorMessage = "Initialer er påkrævet")]
        public string Initials { get; set; }
    }
}
