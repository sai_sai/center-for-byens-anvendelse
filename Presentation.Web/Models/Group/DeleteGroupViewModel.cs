#region Head
// <copyright file="DeleteGroupViewModel.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System.ComponentModel;
using Presentation.Web.Models.Shared;

namespace Presentation.Web.Models.Group
{
    public class DeleteGroupViewModel : CbaListPropertiesViewModel
    {
        [DisplayName("Gruppenavn")]
        public string Name { get; set; }
    }
}
