﻿using System.ComponentModel;

namespace Presentation.Web.Models.AgendaPoint
{
    public class AgendaPointMeetingAgendaPointViewModel
    {
        public int Id { get; set; }

        [DisplayName("Tilknyttet møde")]
        public int MeetingId { get; set; }
    }
}
