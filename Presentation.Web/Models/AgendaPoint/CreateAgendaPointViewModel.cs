﻿#region Head
// <copyright file="CreateAgendaPointViewModel.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Core.DomainModel;
using Presentation.Web.Models.Project;
using Presentation.Web.Models.Shared;
using Presentation.Web.Validators;

namespace Presentation.Web.Models.AgendaPoint
{
    public class CreateAgendaPointViewModel
    {
        [DisplayName("Repræsentativ adresse*")]
        [Required(ErrorMessage = "Repræsentativ adresse er påkrævet.")]
        public Guid AddressId { get; set; }

        [DisplayName("Ansvarlig")]
        public string ResponsiblePersonName { get; set; }

        [DisplayName("Vejtype*")]
        [Required(ErrorMessage = "Vejtype er påkrævet.")]
        public StreetTypeViewModel StreetType { get; set; }

        [DisplayName("Emne")]
        public int? TopicId { get; set; }

        [DisplayName("Beskrivelse*")]
        [Required(ErrorMessage = "En beskrivelse er påkrævet.")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [DisplayName("Aftale")]
        [DataType(DataType.MultilineText)]
        public string Agreement { get; set; }

        [DisplayName("Status")]
        public AgendaPointStatusTypeViewModel AgendaPointStatus { get; set; }

        [DisplayName("Dokumenter")]
        public IList<AgendaPointDocumentViewModel> Documents { get; set; }

        [DisplayName("Projekt")]
        public int? ProjectId { get; set; }

        [DisplayName("Tidligere behandlet på disse møder")]
        [MeetingId(ErrorMessage = "Følgende IDer kan ikke genkendes:")]
        public IEnumerable<string> MeetingIds { get; set; }

        [DisplayName("Område")]
        public AreaType Area { get; set; }

        [DisplayName("Drøftet med samarbejdspartnere")]
        [DataType(DataType.MultilineText)]
        public string DiscussedWithPartners { get; set; }
    }
}
