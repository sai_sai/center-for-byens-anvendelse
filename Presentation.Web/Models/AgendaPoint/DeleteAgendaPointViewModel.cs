﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Presentation.Web.Models.Project;
using Presentation.Web.Models.Shared;

namespace Presentation.Web.Models.AgendaPoint
{
    public class DeleteAgendaPointViewModel : LoggerViewModel
    {
        [DisplayName("Id")]
        public int Id { get; set; }

        [DisplayName("Repræsentativ adresse")]
        public Guid AddressId { get; set; }

        [DisplayName("Ansvarlig")]
        public string ResponsiblePersonName { get; set; }

        [DisplayName("Vejtype")]
        public StreetTypeViewModel StreetType { get; set; }

        [DisplayName("Emne")]
        public string TopicName { get; set; }

        [DisplayName("Beskrivelse")]
        public string Description { get; set; }

        [DisplayName("Afgørelse")]
        [DataType(DataType.MultilineText)]
        public string Conclusion { get; set; }

        [DisplayName("Aftale")]
        [DataType(DataType.MultilineText)]
        public string Agreement { get; set; }

        [DisplayName("Status")]
        public AgendaPointStatusTypeViewModel AgendaPointStatus { get; set; }

        public int? ProjectId { get; set; }

        [DisplayName("Projektemne")]
        public string ProjectTopic { get; set; }

        [DisplayName("Tidligere møder")]
        public IEnumerable<string> MeetingIds { get; set; }

        [DisplayName("Dokumenter")]
        public IList<AgendaPointDocumentViewModel> Documents { get; set; }

        [DisplayName("Tidligere behandlet på disse møder")]
        public IEnumerable<DetailsMeetingViewModel> PreviousMeetings { get; set; }
    }
}
