﻿#region Head
// <copyright file="DetailsAgendaPointViewModel.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Core.DomainModel;
using Presentation.Web.Models.Project;
using Presentation.Web.Models.Shared;

namespace Presentation.Web.Models.AgendaPoint
{
    public class DetailsAgendaPointViewModel
    {
        [DisplayName("Id")]
        public int Id { get; set; }

        [DisplayName("Repræsentativ adresse")]
        public Guid AddressId { get; set; }

        [DisplayName("Ansvarlig")]
        public string ResponsiblePersonName { get; set; }

        [DisplayName("Vejtype")]
        public StreetTypeViewModel StreetType { get; set; }

        [DisplayName("Emne")]
        public string TopicName { get; set; }

        public int? TopicId { get; set; }

        [DisplayName("Beskrivelse")]
        public string Description { get; set; }

        [DisplayName("Afgørelse")]
        public string Conclusion { get; set; }

        [DisplayName("Aftale")]
        public string Agreement { get; set; }

        [DisplayName("Status")]
        public AgendaPointStatusTypeViewModel AgendaPointStatus { get; set; }

        [DisplayName("Dokumenter")]
        public IList<AgendaPointDocumentViewModel> Documents { get; set; }

        public int? ProjectId { get; set; }

        [DisplayName("Projekt")]
        public string ProjectTopic { get; set; }

        [DisplayName("Tidligere behandlet på disse møder")]
        public IEnumerable<DetailsMeetingViewModel> PreviousMeetings { get; set; }

        public IEnumerable<string> MeetingIds { get; set; }

        public AgendaPointMeetingAgendaPointViewModel MeetingAgendaPoint { get; set; }

        [DisplayName("Område")]
        public AreaType Area { get; set; }

        [DisplayName("Drøftet med samarbejdspartnere")]
        [DataType(DataType.MultilineText)]
        public string DiscussedWithPartners { get; set; }

        public bool CanBeEdited { get; set; }
    }
}
