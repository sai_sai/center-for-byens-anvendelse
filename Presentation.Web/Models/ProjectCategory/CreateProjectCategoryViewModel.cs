﻿#region Head
// <copyright file="CreateProjectCategoryViewModel.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Presentation.Web.Models.Shared;

namespace Presentation.Web.Models.ProjectCategory
{
    public class CreateProjectCategoryViewModel : CbaListPropertiesViewModel
    {
        public CreateProjectCategoryViewModel()
        {
            IsActive = true;
        }
        [Required(ErrorMessage = "Kategorinavnet er påkrævet.")]
        [DisplayName("Kategorinavn*")]
        public string Category { get; set; }
    }
}
