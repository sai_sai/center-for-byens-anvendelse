﻿#region Head
// <copyright file="IndexProjectCategoryViewModel.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System.ComponentModel;
using Presentation.Web.Models.Shared;

namespace Presentation.Web.Models.ProjectCategory
{
    public class IndexProjectCategoryViewModel : CbaListPropertiesViewModel
    {
        public int Id { get; set; }

        [DisplayName ("Kategori")]
        public string Category { get; set; }
    }
}
