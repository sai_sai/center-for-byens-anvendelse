﻿#region Head
// <copyright file="AgendaPointViewModel.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Presentation.Web.Models.Shared;
namespace Presentation.Web.Models.Search
{
    public class AgendaPointViewModel : LoggerViewModel
    {
        public int Id { get; set; }

        [DisplayName("Repræsentativ adresse")]
        public Guid AddressId { get; set; }

        [DisplayName("Ansvarlig")]
        public string ResponsiblePersonName { get; set; }

        [DisplayName("Vejtype")]
        public StreetTypeViewModel StreetType { get; set; }

        [DisplayName("Emne")]
        public string TopicName { get; set; }

        [DisplayName("Beskrivelse")]
        public string Description { get; set; }

        [DisplayName("Afgørelse")]
        [DataType(DataType.MultilineText)]
        public string Conclusion { get; set; }

        [DisplayName("Aftale")]
        [DataType(DataType.MultilineText)]
        public string Agreement { get; set; }

        [DisplayName("Status")]
        public AgendaPointStatusTypeViewModel Status { get; set; }

        public int? ProjectId { get; set; }

        [DisplayName("Projektemne")]
        public string ProjectTopic { get; set; }

        [DisplayName("Tidligere møder")]
        public IEnumerable<string> MeetingIds { get; set; }

        /// <summary>
        /// Determing wheter the point can be deleted by the current user
        /// </summary>
        [DisplayName("Kan slettes?")]
        public bool IsDeleableForCurrentUser { get; set; }
    }
}
