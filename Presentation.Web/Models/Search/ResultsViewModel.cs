﻿#region Head
// <copyright file="ResultsSearchViewModel.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Presentation.Web.Models.Search
{
    public class ResultsViewModel
    {
        public ResultsViewModel()
        {
            Projects = new List<ProjectViewModel>();
            AgendaPoints = new List<AgendaPointViewModel>();
        }

        [DisplayName("Projekter")]
        public IEnumerable<ProjectViewModel> Projects { get; set; }

        [DisplayName("Færdselsmødepunkter")]
        public IEnumerable<AgendaPointViewModel> AgendaPoints { get; set; }

        /// <summary>
        /// Get summed count of project and agenda point lists.
        /// </summary>
        /// <returns>Summed count of lists</returns>
        public int Count()
        {
            return Projects.Count() + AgendaPoints.Count();
        }
    }
}
