﻿#region Head
// <copyright file="SearchViewModel.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Presentation.Web.Models.Shared;

namespace Presentation.Web.Models.Search
{
    public class SearchViewModel
    {
        public SearchViewModel()
        {
            SearchInAgendaPoints = true;
            SearchInProjects = true;
        }

        [DisplayName("Vejnavn")]
        public string Address { get; set; }

        [DisplayName("Projekt ID")]
        public int? ProjectId { get; set; }

        [DisplayName("Møde ID")]
        public int? MeetingId { get; set; }

        [DisplayName("Projekt status")]
        public ProjectStatusTypeViewModel? ProjectStatus { get; set; }

        [DisplayName("Færdselsmødepunkts status")]
        public AgendaPointStatusTypeViewModel? AgendaPointStatus { get; set; }

        [DisplayName("Færdselsmødepunkts emne")]
        public int? TopicId { get; set; }

        [DisplayName("Projekt fællesråd")]
        public int? CouncilId { get; set; }

        [DisplayName("Projekt gruppe")]
        public int? GroupId { get; set; }

        [DisplayName("Datointerval")]
        [UIHint("Date")]
        public DateTime? StartDate { get; set; }

        [UIHint("Date")]
        public DateTime? EndDate { get; set; }

        [DisplayName("Færdselsmødepunkter")]
        public bool SearchInAgendaPoints { get; set; }

        [DisplayName("Projekter")]
        public bool SearchInProjects { get; set; }

        public bool AnyParametersSet()
        {
            if (String.IsNullOrWhiteSpace(Address) &&
                ProjectId == null &&
                MeetingId == null &&
                TopicId == null &&
                ProjectStatus == null &&
                AgendaPointStatus == null &&
                CouncilId == null &&
                GroupId == null &&
                StartDate == null &&
                EndDate == null ||
                !SearchInAgendaPoints &&
                !SearchInProjects)
            {
                return false;
            }

            return true;
        }
    }
}
