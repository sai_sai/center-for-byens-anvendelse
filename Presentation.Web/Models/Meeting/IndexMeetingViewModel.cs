﻿#region Head
// <copyright file="IndexMeetingViewModel.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Presentation.Web.Models.Meeting.MeetingAgendaPoint;
using Presentation.Web.Models.Shared;

namespace Presentation.Web.Models.Meeting
{
    public class IndexMeetingViewModel : LoggerViewModel
    {
        [DisplayName("Id")]
        public int Id { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:MM}")]
        [DisplayName("Dato")]
        public string Date { get; set; }

        [DisplayName("Lokation")]
        public string Location { get; set; }

        public IList<MeetingAgendaPointViewModel> MeetingAgendaPoints { get; set; }

        public bool IsSummaryLocked { get; set; }

        public bool IsMeetingAgendaPointSummaryStringsEmpty()
        {
            if(this.IsMeetingAgendaPointsEmpty())
                return true;;

            return this.MeetingAgendaPoints.Any(
                meetingAgendaPoint => String.IsNullOrEmpty(meetingAgendaPoint.AgendaPoint.Conclusion) ||
                    String.IsNullOrEmpty(meetingAgendaPoint.AgendaPoint.Agreement));
        }

        public bool IsMeetingAgendaPointsEmpty()
        {
            if (this.MeetingAgendaPoints == null)
                return true;

            return !this.MeetingAgendaPoints.Any();
        }
    }
}
