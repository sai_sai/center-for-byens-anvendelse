﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Presentation.Web.Models.Meeting
{
    public class MeetingProjectViewModel
    {
        [Display (Name = "Projekt ID")]
        public int Id { get; set; }

        [Display(Name = "Projektemne")]
        public string Topic { get; set; }

        [Display(Name = "Projektadresse")]
        public Guid AddressId { get; set; }

        [DisplayName("Dokumenter tilknyttet Projekt")]
        public IList<MeetingDocumentViewModel> Documents { get; set; }
    }
}