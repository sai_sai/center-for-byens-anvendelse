﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Presentation.Web.Models.Meeting
{
    public class MeetingDocumentViewModel
    {
        public Guid Guid { get; set; }

        [DisplayName("Filnavn: ")]
        public string FileName { get; set; }

        [DisplayName("FilStørrelse: ")]
        public float FileSize { get; set; }

        [DisplayName("Kommentar: ")]
        [DataType(DataType.MultilineText)]
        public string Comment { get; set; }
        public int? ProjectId { get; set; }
    }
}