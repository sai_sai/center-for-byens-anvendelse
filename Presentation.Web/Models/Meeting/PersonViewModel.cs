﻿using System.ComponentModel.DataAnnotations;

namespace Presentation.Web.Models.Meeting
{
    public class PersonViewModel
    {
        public int Id { get; set; }

        [Display (Name = "Ansvarlig person")]
        public string Name { get; set; }
    }
}