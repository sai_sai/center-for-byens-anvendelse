﻿#region Head
// <copyright file="CreateMeetingViewModel.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Presentation.Web.Models.Meeting.MeetingAgendaPoint;

namespace Presentation.Web.Models.Meeting
{
    public class CreateMeetingViewModel
    {
        public CreateMeetingViewModel()
        {
            MeetingAgendaPoints = new List<MeetingAgendaPointViewModel>();
            InvitedPeople = new List<string>();
        }

        [DisplayName("Dato*")]
        [Required(ErrorMessage = "Vælg en dato for afholdelse af mødet.")]
        public string Date { get; set; }

        [DisplayName("Lokation*")]
        [Required(ErrorMessage = "En lokation for mødet er påkrævet.")]
        public string Location { get; set; }

        [DisplayName("Færdselsmødepunkter til næste Færdselsmøde:")]
        public IList<MeetingAgendaPointViewModel> MeetingAgendaPoints { get; set; }

        [DisplayName("Inviterede personer")]
        [UIHint("TagList")]
        public IEnumerable<string> InvitedPeople { get; set; }
    }
}
