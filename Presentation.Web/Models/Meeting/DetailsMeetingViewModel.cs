﻿#region Head
// <copyright file="DetailsMeetingViewModel.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Presentation.Web.Models.Meeting.MeetingAgendaPoint;

namespace Presentation.Web.Models.Meeting
{
    public class DetailsMeetingViewModel
    {
        [DisplayName("Id")]
        public int Id { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy HH:mm}")]
        [DisplayName("Dato")]
        public string Date { get; set; }

        [DisplayName("Lokation")]
        public string Location { get; set; }

        [DisplayName("Færdselsmødepunkter")]
        public IList<MeetingAgendaPointViewModel> MeetingAgendaPoints { get; set; }

        [DisplayName("Mødereferat status")]
        public bool IsSummaryLocked { get; set; }

        [DisplayName("Inviterede personer")]
        public IEnumerable<string> InvitedPeople { get; set; }

        [DisplayName("Godkendelsesdato")]
        public DateTime? AcceptedOn { get; set; }

        [DisplayName("Link til eDoc")]
        public string EDocsLink { get; set; }
    }
}
