﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Presentation.Web.Models.MeetingAgendaPoint;

namespace Presentation.Web.Models.Meeting
{
    public class CreateSummariesMeetingViewModel
    {
        public int Id { get; set; }

        [DisplayName("Færdselsmødepunkter")]
        public IList<MeetingAgendaPointSummariesViewModel> MeetingAgendaPoints { get; set; }

        [DisplayName("Dato")]
        public DateTime Date { get; set; }

        [DisplayName("Lokation")]
        public string Location { get; set; }
        public bool IsSummaryLocked { get; set; }

        [DisplayName("Inviterede personer")]
        public IEnumerable<string> InvitedPeople { get; set; }
    }
}
