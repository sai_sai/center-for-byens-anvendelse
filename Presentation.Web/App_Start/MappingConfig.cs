﻿#region Head
// <copyright file="MappingConfig.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using AutoMapper;
using Core.ApplicationServices;
using Core.DomainModel;
using Presentation.Web.App_Start;
using Presentation.Web.Models.Advisor;
using Presentation.Web.Models.AgendaPoint;
using Presentation.Web.Models.Company;
using Presentation.Web.Models.Council;
using Presentation.Web.Models.DefaultPhrase;
using Presentation.Web.Models.InvitedPerson;
using Presentation.Web.Models.ConstructionProgram;
using Presentation.Web.Models.LocalCommunity;
using Presentation.Web.Models.Meeting;
using Presentation.Web.Models.Meeting.MeetingAgendaPoint;
using Presentation.Web.Models.MeetingAgendaPoint;
using Presentation.Web.Models.Search;
using Presentation.Web.Models.Shared;
using Presentation.Web.Models.ProjectLeader;
using Presentation.Web.Models.Topic;
using Presentation.Web.Models.Group;
using Presentation.Web.Models.ProjectCategory;
using Presentation.Web.Models.Project;
using AgendaPointViewModel = Presentation.Web.Models.Search.AgendaPointViewModel;
using CreateAgendaPointViewModel = Presentation.Web.Models.AgendaPoint.CreateAgendaPointViewModel;
using CreateTopicViewModel = Presentation.Web.Models.Topic.CreateTopicViewModel;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(MappingConfig), "Start")]

namespace Presentation.Web.App_Start
{
    /// <summary>
    /// Automapper is used to map attributes from your viewmodels to your domain models and vice versa,
    /// depending on the functionality. Using the same names for attributes is highly recommended, but
    /// sometimes more complex configrations are needed, which can also be configured here.
    /// </summary>
    [SuppressMessage("Microsoft.Maintainability", "CA1506", Justification = "Automapper mapping bootstrapper")]
    public static class MappingConfig
    {
        [SuppressMessage("Microsoft.Maintainability", "CA1506", Justification = "Automapper mapping bootstrapper")]
        public static void Start()
        {
            // Write your AutoMapper configurations here.

            // ViewModel Mappings
            #region ProjectController
            Mapper.CreateMap<Project, IndexProjectViewModel>()
                .ReverseMap();
            Mapper.CreateMap<ConstructionProgram, ConstructionProgramViewModel>()
                .ReverseMap();
            Mapper.CreateMap<Project, CreateProjectViewModel>()
                .ForMember(dest => dest.Budget, opts => opts.MapFrom(src => src.Budget.ToString("N0")))
                .ReverseMap()
                .ForMember(dest => dest.Budget, opts => opts.MapFrom(src => int.Parse(src.Budget.Replace(".", ""))));
            Mapper.CreateMap<ProjectDocument, ProjectDocumentViewModel>()
                .ReverseMap();
            #endregion
            #region TopicController
            Mapper.CreateMap<Topic, IndexTopicViewModel>()
                .ReverseMap();
            Mapper.CreateMap<Topic, CreateTopicViewModel>()
                .ReverseMap();
            Mapper.CreateMap<Topic, EditTopicViewModel>()
                .ReverseMap();
            Mapper.CreateMap<Topic, DeleteTopicViewModel>()
                .ReverseMap();
            #endregion
            #region AdvisorController
            Mapper.CreateMap<Advisor, IndexAdvisorViewModel>()
                .ReverseMap();
            Mapper.CreateMap<Advisor, CreateAdvisorViewModel>()
                .ReverseMap();
            Mapper.CreateMap<Advisor, EditAdvisorViewModel>()
                .ReverseMap();
            Mapper.CreateMap<Advisor, DeleteAdvisorViewModel>()
                .ReverseMap();
            Mapper.CreateMap<Advisor, AdvisorDTO>()
                .ReverseMap();
            #endregion
            #region GroupController
            Mapper.CreateMap<Group, IndexGroupViewModel>()
                .ReverseMap();
            Mapper.CreateMap<Group, CreateGroupViewModel>()
                .ReverseMap();
            Mapper.CreateMap<Group, EditGroupViewModel>()
                .ReverseMap();
            Mapper.CreateMap<Group, DeleteGroupViewModel>()
                .ReverseMap();
            #endregion
            #region ProjectCategory
            Mapper.CreateMap<ProjectCategory, CreateProjectCategoryViewModel>()
                .ReverseMap();
            Mapper.CreateMap<ProjectCategory, DeleteProjectCategoryViewModel>()
                .ReverseMap();
            Mapper.CreateMap<ProjectCategory, EditProjectCategoryViewModel>()
                .ReverseMap();
            Mapper.CreateMap<ProjectCategory, IndexProjectCategoryViewModel>()
                .ReverseMap();
            #endregion
            #region ConstructionProgram
            Mapper.CreateMap<ConstructionProgram, IndexConstructionProgramViewModel>()
                .ReverseMap();
            Mapper.CreateMap<ConstructionProgram, CreateConstructionProgramViewModel>()
                .ReverseMap();
            Mapper.CreateMap<ConstructionProgram, EditConstructionProgramViewModel>()
                .ReverseMap();
            Mapper.CreateMap<ConstructionProgram, DeleteConstructionProgramViewModel>()
                .ReverseMap();
            #endregion
            #region LocalCommunity
            Mapper.CreateMap<LocalCommunity, IndexLocalCommunityViewModel>()
                .ReverseMap();
            Mapper.CreateMap<LocalCommunity, CreateLocalCommunityViewModel>()
                .ReverseMap();
            Mapper.CreateMap<LocalCommunity, EditLocalCommunityViewModel>()
                .ReverseMap();
            #endregion
            #region AgendaPointController
            Mapper.CreateMap<Topic, Models.AgendaPoint.CreateTopicViewModel>()
                .ReverseMap();
            Mapper.CreateMap<AgendaPoint, DetailsAgendaPointViewModel>()
                .ReverseMap();
            Mapper.CreateMap<AgendaPoint, CreateAgendaPointViewModel>()
                .ReverseMap();
            Mapper.CreateMap<AgendaPoint, EditAgendaPointViewModel>()
                .ReverseMap();
            Mapper.CreateMap<AgendaPoint, IndexAgendaPointViewModel>()
                .ReverseMap();
            Mapper.CreateMap<StreetType, StreetTypeViewModel>()
                .ReverseMap();
            Mapper.CreateMap<AgendaPointStatusType, AgendaPointStatusTypeViewModel>()
                .ReverseMap();
            Mapper.CreateMap<ProjectStatusType, ProjectStatusTypeViewModel>()
                .ReverseMap();
            Mapper.CreateMap<Meeting, Models.AgendaPoint.DetailsMeetingViewModel>()
                .ReverseMap();
            Mapper.CreateMap<AgendaPointDocument, AgendaPointDocumentViewModel>()
                .ReverseMap();
            Mapper.CreateMap<ProjectDocument, MeetingDocumentViewModel>()
                .ReverseMap();
            Mapper.CreateMap<DeleteAgendaPointViewModel, AgendaPoint>()
                .ReverseMap();
            Mapper.CreateMap<MeetingAgendaPoint, AgendaPointMeetingAgendaPointViewModel>()
                .ReverseMap();
            #endregion
            #region InvitedPerson
            Mapper.CreateMap<InvitedPerson, InvitedPersonViewModel>().ReverseMap();
            #endregion
            #region DefaultPhrase
            Mapper.CreateMap<DefaultPhrase, DefaultPhraseViewModel>().ReverseMap();
            #endregion
            #region MeetingController
            const char invitedPeopleSeparator = ',';
            Mapper.CreateMap<AgendaPoint, Models.Meeting.AgendaPointViewModel>()
                .ForMember(dest => dest.IsAssignedToMeeting, opts => opts.MapFrom(src => src.MeetingAgendaPoint != null))
                .ForMember(dest => dest.IsAssignedBeforeChange, opts => opts.MapFrom(src => src.MeetingAgendaPoint != null))
                .ReverseMap();
            Mapper.CreateMap<Meeting, CreateMeetingViewModel>()
                .ForMember(dest => dest.InvitedPeople, opts => opts.MapFrom(src => src.InvitedPeople.Split(new[] {invitedPeopleSeparator}, StringSplitOptions.RemoveEmptyEntries)))
                .ReverseMap()
                .ForMember(dest => dest.InvitedPeople, opts => opts.MapFrom(src => String.Join(invitedPeopleSeparator.ToString(CultureInfo.CurrentCulture), src.InvitedPeople)));
            Mapper.CreateMap<Meeting, IndexMeetingViewModel>()
                .ReverseMap();
            Mapper.CreateMap<Meeting, Models.Meeting.DetailsMeetingViewModel>()
                .ForMember(dest => dest.InvitedPeople, opts => opts.MapFrom(src => src.InvitedPeople.Split(new[] { invitedPeopleSeparator }, StringSplitOptions.RemoveEmptyEntries)))
                .ReverseMap()
                .ForMember(dest => dest.InvitedPeople, opts => opts.MapFrom(src => String.Join(invitedPeopleSeparator.ToString(CultureInfo.CurrentCulture), src.InvitedPeople)));
            Mapper.CreateMap<Meeting, EditMeetingViewModel>()
                .ForMember(dest => dest.InvitedPeople, opts => opts.MapFrom(src => src.InvitedPeople.Split(new[] { invitedPeopleSeparator }, StringSplitOptions.RemoveEmptyEntries)))
                .ReverseMap()
                .ForMember(dest => dest.InvitedPeople, opts => opts.MapFrom(src => String.Join(invitedPeopleSeparator.ToString(CultureInfo.CurrentCulture), src.InvitedPeople)));
            Mapper.CreateMap<Meeting, CreateSummariesMeetingViewModel>()
                .ForMember(dest => dest.InvitedPeople, opts => opts.MapFrom(src => src.InvitedPeople.Split(new[] { invitedPeopleSeparator }, StringSplitOptions.RemoveEmptyEntries)))
                .ReverseMap()
                .ForMember(dest => dest.InvitedPeople, opts => opts.MapFrom(src => String.Join(invitedPeopleSeparator.ToString(CultureInfo.CurrentCulture), src.InvitedPeople)));
            Mapper.CreateMap<Person, PersonViewModel>()
                .ReverseMap();
            Mapper.CreateMap<Project, MeetingProjectViewModel>()
                .ReverseMap();
            #endregion
            #region ProjectLeader
            Mapper.CreateMap<IndexProjectLeaderViewModel, ProjectLeader>()
                .ReverseMap();
            Mapper.CreateMap<CreateProjectLeaderViewModel, ProjectLeader>()
                .ReverseMap();
            Mapper.CreateMap<EditProjectLeaderViewModel, ProjectLeader>()
                .ReverseMap();
            #endregion
            #region Council
            Mapper.CreateMap<IndexCouncilViewModel, Council>()
                .ReverseMap();
            Mapper.CreateMap<CreateCouncilViewModel, Council>()
                .ReverseMap();
            Mapper.CreateMap<EditCouncilViewModel, Council>()
                .ReverseMap();
            Mapper.CreateMap<CouncilDTO, Council>()
                .ReverseMap();
            #endregion
            #region MeetingAgendaPoint
            Mapper.CreateMap<MeetingAgendaPoint, MeetingAgendaPointViewModel>()
                .ReverseMap();
            Mapper.CreateMap<MeetingAgendaPoint, MeetingAgendaPointSummariesViewModel>()
                .ReverseMap();
            #endregion
            #region SearchController
            Mapper.CreateMap<Project, ProjectViewModel>();
            Mapper.CreateMap<AgendaPoint, AgendaPointViewModel>();
            Mapper.CreateMap<SearchViewModel, ProjectSearchParameters>();
            Mapper.CreateMap<SearchViewModel, AgendaPointSearchParameters>();
            #endregion
            #region Logger
            Mapper.CreateMap<Logger, LoggerViewModel>()
                .ReverseMap();
            #endregion

            #region Company
            Mapper.CreateMap<Company, IndexCompanyViewModel>()
                .ReverseMap();
            Mapper.CreateMap<Company, CreateCompanyViewModel>()
                .ReverseMap();
            Mapper.CreateMap<Company, EditCompanyViewModel>()
                .ReverseMap();
            Mapper.CreateMap<Company, DeleteCompanyViewModel>()
                .ReverseMap();
            #endregion
        }
    }
}
