﻿using System.Web.Optimization;

namespace Presentation.Web.App_Start
{
    public static class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            // CBA scripts and CSS
            bundles.Add(new ScriptBundle("~/bundles/cba").Include(
                "~/Scripts/select2/select2.full.min.js",
                "~/Scripts/select2/da.js",
                "~/Scripts/DataTables/jquery.dataTables.js",
                "~/Scripts/DataTables/dataTables.responsive.js",
                "~/Scripts/autoNumeric/autoNumeric.js",
                "~/Scripts/autoNumeric/options.js",
                "~/Scripts/jquery-ui-{version}.js",
                "~/Scripts/site.js",
                "~/Scripts/jquery.datetimepicker.js",
                "~/Scripts/dropzone/dropzone.js"));

            bundles.Add(new StyleBundle("~/Content/cba").Include(
                "~/Content/select2.min.css",
                "~/Content/DataTables/css/jquery.dataTables.css",
                "~/Content/DataTables/css/responsive.dataTables.css",
                "~/Scripts/dropzone/dropzone.css",
                "~/Scripts/dropzone/basic.css",
                "~/Content/themes/base/all.css",
                "~/Content/jquery.datetimepicker.css",
                "~/Content/bootstrap.css",
                "~/Content/printing.css",
                "~/Content/site.css"));

            // Set EnableOptimizations to false for debugging. For more information,
            // visit http://go.microsoft.com/fwlink/?LinkId=301862
            BundleTable.EnableOptimizations = false;
        }
    }
}
