#region Head
// <copyright file="NinjectWebCommon.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Diagnostics.CodeAnalysis;
using System.IO.Abstractions;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Core.ApplicationServices;
using Core.DomainServices;
using Infrastructure.DataAccess;
using Infrastructure.Dawa;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Ninject;
using Ninject.Web.Common;
using Ninject.Web.Mvc.FilterBindingSyntax;
using Presentation.Web.App_Start;
using Presentation.Web.Filters;
using Presentation.Web.Helpers;
using Presentation.Web.Models;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(NinjectWebCommon), "Stop")]

namespace Presentation.Web.App_Start
{
    [SuppressMessage("Microsoft.Naming", "CA1704", Justification = "Ninject is the name of the package")]
    public static class NinjectWebCommon
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        [SuppressMessage("Microsoft.Maintainability", "CA1506", Justification = "Ninject needs dependencies.")]
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<CbaContext>().ToSelf().InRequestScope();
            kernel.Bind<IUnitOfWork>().To<UnitOfWork>();
            // Binding trick to avoid having to bind all usages of IGenericRepository. This binds them all!
            kernel.Bind(typeof(IGenericRepository<>)).To(typeof(GenericRepository<>));

            // AutoMapper
            kernel.Bind<IMappingEngine>().ToMethod(context => Mapper.Engine);

            // System.IO.File
            kernel.Bind<IFileSystem>().To<FileSystem>();

            // DocumentHelper
            kernel.Bind<IDocumentHelper>().To<DocumentHelper>();

            // HttpContextHelper
            kernel.Bind<IHttpContext>().To<HttpContextHelper>();

            // GeoCoder
            kernel.Bind<IGeoCoder>().To<GeoCoder>();

            // System.DateTime wrapper
            kernel.Bind<IDateTime>().To<CustomDateTime>();

            kernel.Bind<IAppSettings>().To<AppSettings>();

            kernel.Bind<ISearchService>().To<SearchService>();
            kernel.Bind<IDawaRepository>().To<DawaRepository>();

            // Populating filters
            kernel.BindFilter<PopulateGroupsFilter>(FilterScope.Action, 0)
                .WhenActionMethodHas<PopulateGroupsAttribute>();

            kernel.BindFilter<PopulateAdvisorsFilter>(FilterScope.Action, 0)
                .WhenActionMethodHas<PopulateAdvisorsAttribute>();

            kernel.BindFilter<PopulateConstructionProgramsFilter>(FilterScope.Action, 0)
                .WhenActionMethodHas<PopulateConstructionProgramsAttribute>();

            kernel.BindFilter<PopulateProjectCategoriesFilter>(FilterScope.Action, 0)
                .WhenActionMethodHas<PopulateProjectCategoriesAttribute>();

            kernel.BindFilter<PopulateLocalCommunitiesFilter>(FilterScope.Action, 0)
                .WhenActionMethodHas<PopulateLocalCommunitiesAttribute>();

            kernel.BindFilter<PopulateTopicsFilter>(FilterScope.Action, 0)
                .WhenActionMethodHas<PopulateTopicsAttribute>();

            kernel.BindFilter<PopulateProjectLeadersFilter>(FilterScope.Action, 0)
                .WhenActionMethodHas<PopulateProjectLeadersAttribute>();

            kernel.BindFilter<PopulateInvitedPeopleFilter>(FilterScope.Action, 0)
                .WhenActionMethodHas<PopulateInvitedPeopleAttribute>();

            kernel.BindFilter<PopulateCouncilsFilter>(FilterScope.Action, 0)
                .WhenActionMethodHas<PopulateCouncilsAttribute>();

            kernel.BindFilter<PopulateCouncilsRawFilter>(FilterScope.Action, 0)
                .WhenActionMethodHas<PopulateCouncilsRawAttribute>();

            kernel.BindFilter<PopulateDefaultPhrasesFilter>(FilterScope.Action, 0)
                .WhenActionMethodHas<PopulateDefaultPhrasesAttribute>();

            kernel.BindFilter<PopulateCompaniesFilter>(FilterScope.Action, 0)
                .WhenActionMethodHas<PopulateCompaniesAttribute>();
        }
    }
}
