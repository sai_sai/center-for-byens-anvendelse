﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Mvc;

namespace Presentation.Web.Validators
{
    /// <summary>
    /// For validation of min value
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class MinValueAttribute : ValidationAttribute, IClientValidatable
    {
        public int MinValue { get; private set; }

        public MinValueAttribute(int minValue)
        {
            MinValue = minValue;
            ErrorMessage = "Enter a value greater or equal than " + minValue;
        }

        public override bool IsValid(object value)
        {
            return Convert.ToInt32(value, CultureInfo.CurrentCulture) >= MinValue;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule();
            rule.ErrorMessage = ErrorMessage;
            rule.ValidationParameters.Add("min", MinValue);
            rule.ValidationParameters.Add("max", Double.MaxValue);
            rule.ValidationType = "range";
            yield return rule;
        }
    }
}
