﻿#region Head
// <copyright file="SummarySetValidatorAttribute.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Presentation.Web.Models.Meeting.MeetingAgendaPoint;

namespace Presentation.Web.Validators
{
    /// <summary>
    /// Validator for checking if summary is set for a originally assigned meetingpoint getting unchecked
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class SummarySetValidatorAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            var meetingAgendaPoints = value as List<MeetingAgendaPointViewModel>;

            if (meetingAgendaPoints == null) return true;
            foreach (var meetingAgendaPoint in meetingAgendaPoints)
            {
                if (!meetingAgendaPoint.AgendaPoint.IsAssignedBeforeChange
                    || meetingAgendaPoint.AgendaPoint.IsAssignedToMeeting)
                    continue;

                // if both conclusion and agreement is not empty, you can't uncheck the point
                if (!String.IsNullOrEmpty(meetingAgendaPoint.AgendaPoint.Conclusion)
                    || !String.IsNullOrEmpty(meetingAgendaPoint.AgendaPoint.Agreement))
                {
                    return false;
                }
            }
            return true;
        }
    }
}
