﻿#region Head
// <copyright file="MeetingIdAttribute.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.ComponentModel.DataAnnotations;

namespace Presentation.Web.Validators
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class ThousandsNumberAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null) return new ValidationResult(ErrorMessage);

            var numberString = value as string;
            if (numberString == null) return new ValidationResult(ErrorMessage);

            var numberNoSeparator = numberString.Replace(".", "");

            int number;

            if (!int.TryParse(numberNoSeparator, out number)) return new ValidationResult(ErrorMessage);

            return ValidationResult.Success;
        }
    }
}
