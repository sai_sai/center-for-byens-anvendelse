﻿#region Head
// <copyright file="PopulateAdvisorsFilter.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;

namespace Presentation.Web.Filters
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class PopulateAdvisorsAttribute : Attribute
    {
    }

    public class PopulateAdvisorsFilter : IActionFilter
    {
        private readonly IGenericRepository<Advisor> _advisorRepository;

        public PopulateAdvisorsFilter(IGenericRepository<Advisor> advisorRepository)
        {
            _advisorRepository = advisorRepository;
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {

        }

        [SuppressMessage("Microsoft.Globalization", "CA1303:Do not pass literals as localized parameters", MessageId = "System.Web.Mvc.SelectListItem.set_Text(System.String)", Justification = "Generated view output."),
         SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "Slettet", Justification = "Danish word.")]
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var viewModel = filterContext.Controller.ViewData.Model as ISelectedAdvisor;
            var viewBag = filterContext.Controller.ViewBag;

            IEnumerable<Advisor> advisors;
            if (viewModel != null && viewModel.AdvisorId != null)
            {
                advisors = _advisorRepository.Get(a => a.IsDeleted == false || a.Id == viewModel.AdvisorId);
            }
            else
            {
                advisors = _advisorRepository.Get(a => a.IsDeleted == false);
            }

            var advisorListItems = new List<SelectListItem>();

            foreach (var advisor in advisors)
            {
                var advisorItem = new SelectListItem
                {
                    Text = advisor.ResponsiblePersonName,
                    Value = advisor.Id.ToString(CultureInfo.CurrentCulture)
                };

                if (advisor.IsDeleted)
                {
                    advisorItem.Text += " (Slettet)";
                    viewBag.IsAdvisorDeleted = true;
                }

                advisorListItems.Add(advisorItem);
            }

            viewBag.AdvisorList = advisorListItems;
        }
    }
}
