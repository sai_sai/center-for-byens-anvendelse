﻿#region Head
// <copyright file="PopulateDefaultPhrasesFilter.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;

namespace Presentation.Web.Filters
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class PopulateDefaultPhrasesAttribute : Attribute
    {
    }

    public class PopulateDefaultPhrasesFilter : IActionFilter
    {
        private readonly IGenericRepository<DefaultPhrase> _defaultPhraseRepository;

        public PopulateDefaultPhrasesFilter(IGenericRepository<DefaultPhrase> defaultPhraseRepository)
        {
            _defaultPhraseRepository = defaultPhraseRepository;
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
        }

        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var defaultPhrases = _defaultPhraseRepository.Get(a => a.IsDeleted == false);

            var defaultPhraseListItems = new List<SelectListItem>();

            foreach (var defaultPhrase in defaultPhrases)
            {
                var defaultPhraseItem = new SelectListItem
                {
                    Text = defaultPhrase.Phrase,
                    Value = defaultPhrase.Id.ToString(CultureInfo.CurrentCulture)
                };

                defaultPhraseListItems.Add(defaultPhraseItem);
            }

            filterContext.Controller.ViewBag.DefaultPhraseList = defaultPhraseListItems;
        }
    }
}
