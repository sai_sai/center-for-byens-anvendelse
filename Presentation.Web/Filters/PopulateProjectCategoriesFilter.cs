﻿#region Head
// <copyright file="PopulateProjectCategoriesFilter.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;

namespace Presentation.Web.Filters
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class PopulateProjectCategoriesAttribute : Attribute
    {
    }

    public class PopulateProjectCategoriesFilter : IActionFilter
    {
        private readonly IGenericRepository<ProjectCategory> _projectCategoryRepository;

        public PopulateProjectCategoriesFilter(IGenericRepository<ProjectCategory> projectCategoryRepository)
        {
            _projectCategoryRepository = projectCategoryRepository;
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {

        }

        [SuppressMessage("Microsoft.Globalization", "CA1303:Do not pass literals as localized parameters", MessageId = "System.Web.Mvc.SelectListItem.set_Text(System.String)", Justification = "Generated view output."),
         SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "Slettet", Justification = "Danish word.")]
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var viewModel = filterContext.Controller.ViewData.Model as ISelectedProjectCategory;
            var viewBag = filterContext.Controller.ViewBag;

            IEnumerable<ProjectCategory> projectCategories;
            if (viewModel != null && viewModel.ProjectCategoryId != null)
            {
                projectCategories = _projectCategoryRepository.Get(a => a.IsDeleted == false || a.Id == viewModel.ProjectCategoryId);
            }
            else
            {
                projectCategories = _projectCategoryRepository.Get(a => a.IsDeleted == false);
            }

            var projectCategoryListItems = new List<SelectListItem>();

            foreach (var projectCategory in projectCategories)
            {
                var projectCategoryItem = new SelectListItem
                {
                    Text = projectCategory.Category,
                    Value = projectCategory.Id.ToString(CultureInfo.CurrentCulture)
                };

                if (projectCategory.IsDeleted)
                {
                    projectCategoryItem.Text += " (Slettet)";
                    viewBag.IsProjectCategoryDeleted = true;
                }

                projectCategoryListItems.Add(projectCategoryItem);
            }

            viewBag.ProjectCategoryList = projectCategoryListItems;
        }
    }
}
