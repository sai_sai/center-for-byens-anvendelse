﻿#region Head
// <copyright file="PopulateInvitedPeopleFilter.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;

namespace Presentation.Web.Filters
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class PopulateInvitedPeopleAttribute : Attribute
    {

    }

    public class PopulateInvitedPeopleFilter : IActionFilter
    {
        private readonly IGenericRepository<InvitedPerson> _repo;

        public PopulateInvitedPeopleFilter(IGenericRepository<InvitedPerson> repo)
        {
            _repo = repo;
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {

        }

        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var viewModel = filterContext.Controller.ViewData.Model as ISelectedInvitedPerson;
            var viewBag = filterContext.Controller.ViewBag;

            IEnumerable<InvitedPerson> people;
            if (viewModel != null && !String.IsNullOrEmpty(viewModel.Name))
            {
                people = _repo.Get(p => !p.IsDeleted || p.Name == viewModel.Name);
            }
            else
            {
                people = _repo.Get(p => !p.IsDeleted);
            }

            var peopleListItems = new List<SelectListItem>();

            foreach (var person in people)
            {
                var groupItem = new SelectListItem
                {
                    Selected = true,
                    Text = person.Name,
                    Value = person.Name
                };

                peopleListItems.Add(groupItem);
            }

            viewBag.InvitedPeopleList = peopleListItems;
        }
    }
}
