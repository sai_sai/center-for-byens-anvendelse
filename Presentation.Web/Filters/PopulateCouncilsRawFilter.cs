﻿#region Head
// <copyright file="PopulateCouncilsFilter.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;

namespace Presentation.Web.Filters
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class PopulateCouncilsRawAttribute : Attribute
    {
    }

    public class PopulateCouncilsRawFilter : IActionFilter
    {
        private readonly IGenericRepository<Council> _councilRepository;

        public PopulateCouncilsRawFilter(IGenericRepository<Council> councilRepository)
        {
            _councilRepository = councilRepository;
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {

        }

        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var viewModel = filterContext.Controller.ViewData.Model as ISelectedCouncil;
            var viewBag = filterContext.Controller.ViewBag;

            IEnumerable<Council> councils;

            if (viewModel != null && viewModel.CouncilId != null)
                councils = _councilRepository.Get(a => a.IsDeleted == false || a.Id == viewModel.CouncilId);
            else
                councils = _councilRepository.Get(a => a.IsDeleted == false);

            viewBag.Councils = councils;
        }
    }
}
