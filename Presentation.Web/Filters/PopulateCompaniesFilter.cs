﻿#region Head
// <copyright file="PopulateCompaniesFilter.cs" company="IT Minds" year="2016">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;

namespace Presentation.Web.Filters
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class PopulateCompaniesAttribute : Attribute
    {
    }

    public class PopulateCompaniesFilter : IActionFilter
    {
        private readonly IGenericRepository<Company> _companyRepository;

        public PopulateCompaniesFilter(IGenericRepository<Company> companyRepository)
        {
            _companyRepository = companyRepository;
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {

        }

        [SuppressMessage("Microsoft.Globalization", "CA1303:Do not pass literals as localized parameters", MessageId = "System.Web.Mvc.SelectListItem.set_Text(System.String)", Justification = "Generated view output."),
         SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "Slettet", Justification = "Danish word.")]
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var viewModel = filterContext.Controller.ViewData.Model as ISelectedCompany;
            var viewBag = filterContext.Controller.ViewBag;

            IEnumerable<Company> companies;
            if (viewModel != null && viewModel.CompanyId != null)
            {
                companies = _companyRepository.Get(a => a.IsDeleted == false || a.Id == viewModel.CompanyId);
            }
            else
            {
                companies = _companyRepository.Get(a => a.IsDeleted == false);
            }

            var companyListItems = new List<SelectListItem>();

            foreach (var company in companies)
            {
                var companyItem = new SelectListItem
                {
                    Text = company.Name,
                    Value = company.Id.ToString(CultureInfo.CurrentCulture)
                };

                if (company.IsDeleted)
                {
                    companyItem.Text += " (Slettet)";
                    viewBag.IsCompanyDeleted = true;
                }

                companyListItems.Add(companyItem);
            }

            viewBag.CompanyList = companyListItems;
        }
    }
}
