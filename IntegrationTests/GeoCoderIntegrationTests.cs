﻿using System;
using System.Data.Entity.Spatial;
using System.Net;
using Presentation.Web.Helpers;
using Xunit;

namespace IntegrationTests
{
    public class GeoCoderIntegrationTests
    {
        private string _addressId;
        private readonly IGeoCoder _geoCoder;

        public GeoCoderIntegrationTests()
        {
            _geoCoder = new GeoCoder();
        }

        [Fact]
        public void GetDbGeometry_ValidRequest_ReturnsDbGeograpyForAddress()
        {
            // Arrange
            _addressId = "0255b942-f3ac-4969-a963-d2c4ed9ab943";
            var guid = new Guid(_addressId);

            // Act
            var coordinates = _geoCoder.GetSpartialObject(guid);

            // Assert
            Assert.IsAssignableFrom<DbGeometry>(coordinates);
            Assert.NotEqual(null, coordinates);
        }

        [Fact]
        public void GetDbGeometry_InvalidRequest_ThrowsWebException()
        {
            // Arrange

            // Act
            var dele = new Func<DbGeometry>(() => _geoCoder.GetSpartialObject(new Guid()));

            // Assert
            Assert.Throws<WebException>(dele);
        }
    }
}
