#region Head
// <copyright file="ProjectSearchParameters.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
namespace Core.ApplicationServices
{
    using System;
    using DomainModel;

    /// <summary>
    /// Search parameters for projects.
    /// </summary>
    public class ProjectSearchParameters
    {
        /// <summary>
        /// Gets or sets agenda point address
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets associated project id
        /// </summary>
        public int? ProjectId { get; set; }

        /// <summary>
        /// Gets or sets agenda point status
        /// </summary>
        public ProjectStatusType? ProjectStatus { get; set; }

        /// <summary>
        /// Gets or sets associated council id
        /// </summary>
        public int? CouncilId { get; set; }

        /// <summary>
        /// Gets or sets associated group id
        /// </summary>
        public int? GroupId { get; set; }

        /// <summary>
        /// Gets or sets date interval beginning. Search is including this date.
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Gets or sets date interval end. Search is including this date.
        /// </summary>
        public DateTime? EndDate { get; set; }
    }
}
