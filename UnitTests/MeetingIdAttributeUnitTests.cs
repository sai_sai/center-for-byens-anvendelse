﻿using System.Collections.Generic;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;
using NSubstitute;
using Presentation.Web.Validators;
using Xunit;

namespace UnitTests
{
    public class MeetingIdAttributeUnitTests
    {
        private readonly MeetingIdAttribute _meetingIdAttribute;
        private readonly IGenericRepository<Meeting> _repo;
        private readonly IDependencyResolver _resolver;

        public MeetingIdAttributeUnitTests()
        {
            _repo = Substitute.For<IGenericRepository<Meeting>>();
            _resolver = Substitute.For<IDependencyResolver>();
            _resolver.GetService<IGenericRepository<Meeting>>().Returns(_repo);
            DependencyResolver.SetResolver(_resolver);

            _meetingIdAttribute = new MeetingIdAttribute();
        }

        [Fact]
        public void IsValid_ValueNull_ReturnTrue()
        {
            // Arrange
            // Act
            var ret = _meetingIdAttribute.IsValid(null);

            // Assert
            Assert.IsType<bool>(ret);
            Assert.True(ret);
        }

        [Fact]
        public void IsValid_ValidMeetingId_ReturnTrue()
        {
            // Arrange
            const string value = "123";
            _repo.GetByKey(Arg.Any<int>()).Returns(new Meeting());

            // Act
            var ret = _meetingIdAttribute.IsValid(value);

            // Assert
            Assert.IsType<bool>(ret);
            Assert.True(ret);
        }

        [Fact]
        public void IsValid_SeveralValidMeetingId_ReturnTrue()
        {
            // Arrange
            var ids = new List<string> {"123", "1234"};

            _repo.GetByKey(Arg.Any<int>()).Returns(new Meeting());

            // Act
            var ret = _meetingIdAttribute.IsValid(ids);

            // Assert
            Assert.IsType<bool>(ret);
            Assert.True(ret);
        }

        [Fact]
        public void IsValid_NonValidMeetingId_ReturnTrue()
        {
            // Arrange
            var ids = new List<string> { "123" };
            _repo.GetByKey(Arg.Any<int>()).Returns((Meeting)null);

            // Act
            var ret = _meetingIdAttribute.IsValid(ids);

            // Assert
            Assert.IsType<bool>(ret);
            Assert.False(ret);
        }

        [Fact]
        public void IsValid_SeveralNonValidMeetingId_ReturnTrue()
        {
            // Arrange
            var ids = new List<string> { "123", "1234" };
            _repo.GetByKey(Arg.Any<int>()).Returns((Meeting)null);

            // Act
            var ret = _meetingIdAttribute.IsValid(ids);

            // Assert
            Assert.IsType<bool>(ret);
            Assert.False(ret);
        }

        [Fact]
        public void IsValid_NonValidMeetingIdNonInt_ReturnTrue()
        {
            // Arrange
            var ids = new List<string> { "1a" };
            _repo.GetByKey(Arg.Any<int>()).Returns((Meeting)null);

            // Act
            var ret = _meetingIdAttribute.IsValid(ids);

            // Assert
            Assert.IsType<bool>(ret);
            Assert.False(ret);
        }

        [Fact]
        public void IsValid_SeveralNonValidMeetingIdNonInt_ReturnTrue()
        {
            // Arrange
            var ids = new List<string> { "1a", "11" };
            _repo.GetByKey(Arg.Any<int>()).Returns((Meeting)null);

            // Act
            var ret = _meetingIdAttribute.IsValid(ids);

            // Assert
            Assert.IsType<bool>(ret);
            Assert.False(ret);
        }
    }
}
