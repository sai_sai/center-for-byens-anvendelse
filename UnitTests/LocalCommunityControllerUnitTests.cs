﻿#region Head
// <copyright file="LocalCommunityControllerUnitTests.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using Core.DomainModel;
using Core.DomainServices;
using NSubstitute;
using Presentation.Web.Controllers;
using Presentation.Web.Models.LocalCommunity;
using Xunit;

namespace UnitTests
{
    public class LocalCommunityControllerUnitTests
    {
        private readonly LocalCommunityController _localCommunityController;
        private readonly IGenericRepository<LocalCommunity> _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMappingEngine _mapper;

        public LocalCommunityControllerUnitTests()
        {
            _repo = Substitute.For<IGenericRepository<LocalCommunity>>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _mapper = Substitute.For<IMappingEngine>();

            new UserMock().LogOn(); // setting user mock
            _mapper.Map<LocalCommunity>(Arg.Any<CreateLocalCommunityViewModel>())
                .Returns(new LocalCommunity());
            _mapper.Map<LocalCommunity>(Arg.Any<EditLocalCommunityViewModel>())
                .Returns(new LocalCommunity());

            _localCommunityController = new LocalCommunityController(_repo, _unitOfWork, _mapper);
        }

        #region Index
        [Fact]
        public void Index_Call_ReturnsView()
        {
            // Arrange
            // Act
            var ret = _localCommunityController.Index();

            // Assert
            Assert.IsType(typeof(ViewResult), ret);
        }

        [Fact]
        public void Index_Call_ReturnsIndexLocalCommunityViewModel()
        {
            // Arrange
            var localCommunityList = new List<LocalCommunity>
            {
                new LocalCommunity(),
                new LocalCommunity()
            };
            _repo.Get().Returns(localCommunityList);

            // Act
            var ret = _localCommunityController.Index() as ViewResult;

            // Assert
            Assert.NotNull(ret);
            Assert.IsAssignableFrom<IEnumerable<IndexLocalCommunityViewModel>>(ret.Model);
        }

        [Fact]
        public void Index_Call_CallsLocalCommunitiesInRepo()
        {
            // Arrange
            // Act
            _localCommunityController.Index();

            // Assert
            _repo.Received(1).Get(Arg.Any<Expression<Func<LocalCommunity, bool>>>());
        }
        #endregion

        #region Create Get
        [Fact]
        public void Create_Call_ReturnsView()
        {
            // Arrange
            // Act
            var ret = _localCommunityController.Create();

            // Assert
            Assert.IsType(typeof(ViewResult), ret);
        }

        [Fact]
        public void Create_Call_ReturnsViewModel()
        {
            // Arrange
            // Act
            var ret = _localCommunityController.Create() as ViewResult;

            // Assert
            Assert.NotNull(ret);
            Assert.NotNull(ret.Model);
        }
        #endregion

        #region Create Post
        [Fact]
        public void CreatePost_ModelValid_RedirectAction()
        {
            // Arrange
            var vm = new CreateLocalCommunityViewModel();

            // Act
            var ret = _localCommunityController.Create(vm);

            // Assert
            Assert.IsType(typeof(RedirectToRouteResult), ret);
        }

        [Fact]
        public void CreatePost_ModelInvalid_ReturnView()
        {
            // Arrange
            var vm = new CreateLocalCommunityViewModel();
            _localCommunityController.ModelState.AddModelError("error", "error");

            // Act
            var ret = _localCommunityController.Create(vm) as ViewResult;

            // Assert
            Assert.NotNull(ret);
            Assert.IsType<CreateLocalCommunityViewModel>(ret.Model);
        }

        [Fact]
        public void CreatePost_ModelValid_CallCreateInRepo()
        {
            // Arrange
            var vm = new CreateLocalCommunityViewModel();

            // Act
            _localCommunityController.Create(vm);

            // Assert
            _repo.Received(1).Insert(Arg.Any<LocalCommunity>());
            _unitOfWork.Received(1).Save();
        }
        #endregion

        #region Edit Get
        [Fact]
        public void Edit_IdValid_ReturnView()
        {
            // Arrange
            var localCommunity = new LocalCommunity();
            int? id = 1;
            _repo.GetByKey(id).Returns(localCommunity);

            // Act
            var ret = _localCommunityController.Edit(id);

            // Assert
            Assert.IsAssignableFrom<ViewResult>(ret);
        }

        [Fact]
        public void Edit_IdValid_ReturnViewModel()
        {
            // Arrange
            var localCommunity = new LocalCommunity();
            int? id = 1;
            _repo.GetByKey(id).Returns(localCommunity);
            _mapper.Map<EditLocalCommunityViewModel>(localCommunity)
                .Returns(new EditLocalCommunityViewModel());

            // Act
            var ret = _localCommunityController.Edit(id) as ViewResult;

            // Assert
            Assert.IsType<EditLocalCommunityViewModel>(ret.Model);
        }

        [Fact]
        public void Edit_IdNull_ReturnBadRequest()
        {
            // Arrange
            int? id = null;
            const int expectedStatusCode = 400;

            // Act
            var ret = _localCommunityController.Edit(id) as HttpStatusCodeResult;

            // Assert
            Assert.NotNull(ret);
            Assert.Equal(expectedStatusCode, ret.StatusCode);
        }

        [Fact]
        public void Edit_IdValid_CallGetByKeyInRepo()
        {
            // Arrange
            int? id = 1;

            // Act
            _localCommunityController.Edit(id);

            // Assert
            _repo.Received(1).GetByKey(id);
        }

        [Fact]
        public void EditLocalCommuniy_UpdateCommunity_UpdateCalledInRepo()
        {
            // Arrange
            var vm = new EditLocalCommunityViewModel();

            // Act
            _localCommunityController.Edit(vm);

            // Assert
            _repo.Received().Update(Arg.Any<LocalCommunity>());
            _unitOfWork.Received().Save();
        }

        [Fact]
        public void Edit_IdValidCallGetByKeyInRepoReturnsNull_ReturnNotFound()
        {
            // Arrange
            int? id = 1;
            _repo.GetByKey(id).Returns((LocalCommunity)null);

            // Act
            var ret = _localCommunityController.Edit(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void Edit_IdValidLocalCommunityDeleted_ReturnNotFound()
        {
            // Arrange
            int? id = 1;
            var localCommunity = new LocalCommunity { IsDeleted = true };
            _repo.GetByKey(id).Returns(localCommunity);

            // Act
            var ret = _localCommunityController.Edit(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }
        #endregion

        #region Edit Post
        [Fact]
        public void EditPost_ModelValid_RedirectAction()
        {
            // Arrange
            var vm = new EditLocalCommunityViewModel();

            // Act
            var ret = _localCommunityController.Edit(vm);

            // Assert
            Assert.IsType<RedirectToRouteResult>(ret);
        }

        [Fact]
        public void EditPost_ModelInvalid_ReturnView()
        {
            // Arrange
            var vm = new EditLocalCommunityViewModel();
            _localCommunityController.ModelState.AddModelError("error", "error");

            // Act
            var ret = _localCommunityController.Edit(vm) as ViewResult;

            // Assert
            Assert.NotNull(ret);
            Assert.IsType<EditLocalCommunityViewModel>(ret.Model);
        }

        [Fact]
        public void EditPost_ModelValid_CallUpdateInRepo()
        {
            // Arrange
            var vm = new EditLocalCommunityViewModel();

            // Act
            _localCommunityController.Edit(vm);

            // Assert
            _repo.Received(1).Update(Arg.Any<LocalCommunity>());
            _unitOfWork.Received(1).Save();
        }
        #endregion

        #region Delete
        [Fact]
        public void Delete_IdValid_ReturnView()
        {
            // Arrange
            var localCommunity = new LocalCommunity();
            int? id = 1;
            _repo.GetByKey(id).Returns(localCommunity);

            // Act
            var ret = _localCommunityController.Delete(id);

            // Assert
            Assert.IsType<ViewResult>(ret);
        }

        [Fact]
        public void Delete_IdValid_ReturnViewModel()
        {
            // Arrange
            var localCommunity = new LocalCommunity();
            int? id = 1;
            _repo.GetByKey(id).Returns(localCommunity);
            _mapper.Map<IndexLocalCommunityViewModel>(localCommunity)
                .Returns(new IndexLocalCommunityViewModel());

            // Act
            var ret = _localCommunityController.Delete(id) as ViewResult;

            // Assert
            Assert.IsType<IndexLocalCommunityViewModel>(ret.Model);
        }

        [Fact]
        public void Delete_IdValid_CallGetByKeyInRepo()
        {
            // Arrange
            var localCommunity = new LocalCommunity();
            int? id = 1;
            _repo.GetByKey(id).Returns(localCommunity);

            // Act
            _localCommunityController.Delete(id);

            // Assert
            _repo.Received(1).GetByKey(id);
        }

        [Fact]
        public void Delete_IdValidRepoGetByKeyReturnsNull_ReturnHttpNotFound()
        {
            // Arrange
            int? id = 1;
            _repo.GetByKey(id).Returns((LocalCommunity)null);

            // Act
            var ret = _localCommunityController.Delete(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void Delete_IdValidLocalCommunityDeleted_ReturnHttpNotFound()
        {
            // Arrange
            const int id = 1;
            var localCommunity = new LocalCommunity { IsDeleted = true };
            _repo.GetByKey(id).Returns(localCommunity);

            // Act
            var ret = _localCommunityController.Delete(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void Delete_IdNull_ReturnBadRequest()
        {
            // Arrange
            int? id = null;

            // Act
            var ret = _localCommunityController.Delete(id) as HttpStatusCodeResult;

            // Assert
            _repo.DidNotReceive().GetByKey(Arg.Any<int?>());
            Assert.Equal((int)HttpStatusCode.BadRequest, ret.StatusCode);
        }
        #endregion

        #region DeleteConfirmed
        [Fact]
        public void DeleteConfirmed_IdValid_RepoUpdateCalled()
        {
            // Arrange
            const int id = 1;
            var localCommunity = new LocalCommunity();
            _repo.GetByKey(id).Returns(localCommunity);

            // Act
            _localCommunityController.DeleteConfirmed(id);

            // Assert
            _repo.Received(1).Update(localCommunity);
            _unitOfWork.Received(1).Save();
            Assert.True(localCommunity.IsDeleted);
        }

        [Fact]
        public void DeleteConfirmed_IdValid_RedirectAction()
        {
            // Arrange
            const int id = 1;
            var localCommunity = new LocalCommunity();
            _repo.GetByKey(id).Returns(localCommunity);

            // Act
            var ret = _localCommunityController.DeleteConfirmed(id);

            // Assert
            Assert.IsType<RedirectToRouteResult>(ret);
        }

        [Fact]
        public void DeleteConfirmed_IdNotValid_ReturnBadRequest()
        {
            // Arrange
            const int id = 0;
            _repo.GetByKey(id).Returns((LocalCommunity)null);

            // Act
            var ret = _localCommunityController.DeleteConfirmed(id) as HttpStatusCodeResult;

            // Assert
            Assert.Equal((int)HttpStatusCode.BadRequest, ret.StatusCode);
        }

        [Fact]
        public void DeleteConfirmed_IdNotFound_ReturnNotFound()
        {
            // Arrange
            const int id = 1;
            _repo.GetByKey(id).Returns((LocalCommunity)null);

            // Act
            var ret = _localCommunityController.DeleteConfirmed(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void DeleteConfirmed_IdFoundLocalCommunityDeleted_ReturnNotFound()
        {
            // Arrange
            const int id = 1;
            var localCommunity = new LocalCommunity { IsDeleted = true };
            _repo.GetByKey(id).Returns(localCommunity);

            // Act
            var ret = _localCommunityController.DeleteConfirmed(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }
        #endregion
    }
}
