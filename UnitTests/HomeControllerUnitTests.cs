﻿using System.Web.Mvc;
using Presentation.Web.Controllers;
using Xunit;

namespace UnitTests
{
    public class HomeControllerUnitTests
    {
        private readonly HomeController _homeController;
        public HomeControllerUnitTests()
        {
            _homeController = new HomeController();
        }

        #region Index
        [Fact]
        public void Index_Call_ReturnsView()
        {
            // Arrange
            // Act
            var ret = _homeController.Index();

            // Assert
            Assert.IsType<ViewResult>(ret);
        }
        #endregion
    }
}
