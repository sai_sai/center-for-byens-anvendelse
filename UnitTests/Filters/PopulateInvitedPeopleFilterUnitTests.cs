﻿#region Head
// <copyright file="PopulateInvitedPeopleFilterUnitTests.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;
using NSubstitute;
using Presentation.Web.Filters;
using Xunit;

namespace UnitTests.Filters
{
    public class PopulateInvitedPeopleFilterUnitTests
    {
        private readonly Controller _controller;
        private readonly ActionExecutingContext _executingContext;
        private readonly ActionExecutedContext _executedContext;
        private readonly IGenericRepository<InvitedPerson> _repo;
        private readonly ISelectedInvitedPerson _selectedInvitedPerson;
        private readonly PopulateInvitedPeopleFilter _populateInvitedPeopleFilter;

        public PopulateInvitedPeopleFilterUnitTests()
        {
            _controller = Substitute.For<Controller>();
            _executingContext = Substitute.For<ActionExecutingContext>();
            _executedContext = Substitute.For<ActionExecutedContext>();
            _repo = Substitute.For<IGenericRepository<InvitedPerson>>();
            _selectedInvitedPerson = Substitute.For<ISelectedInvitedPerson>();

            _controller.ViewData.Model = _selectedInvitedPerson;
            _executedContext.Controller = _controller;

            _populateInvitedPeopleFilter = new PopulateInvitedPeopleFilter(_repo);
        }

        [Fact]
        public void OnActionExecuted_ListWith1Item_ListNotNull()
        {
            // Arrange
            var invitedPerson = new InvitedPerson { Id = 1, Name = "Name" };
            var invitedPeople = new List<InvitedPerson> { invitedPerson };

            _repo.Get(Arg.Any<Expression<Func<InvitedPerson, bool>>>()).Returns(invitedPeople);

            // Act
            _populateInvitedPeopleFilter.OnActionExecuted(_executedContext);

            // Assert
            Assert.NotNull(_controller.ViewBag.InvitedPeopleList as List<SelectListItem>);
        }

        [Fact]
        public void OnActionExecuted_ListWith1Item_ListContaining1Element()
        {
            // Arrange
            var invitedPerson = new InvitedPerson { Id = 1, Name = "Name" };
            var invitedPeople = new List<InvitedPerson> { invitedPerson };

            _repo.Get(Arg.Any<Expression<Func<InvitedPerson, bool>>>()).Returns(invitedPeople);

            var expectedListItem = new SelectListItem { Text = invitedPerson.Name, Value = invitedPerson.Name, Selected = true };
            var expectedList = new List<SelectListItem> { expectedListItem };

            // Act
            _populateInvitedPeopleFilter.OnActionExecuted(_executedContext);

            // Assert
            var viewBagInvitedPersonList = _controller.ViewBag.InvitedPeopleList as List<SelectListItem>;
            Assert.Equal(expectedList, viewBagInvitedPersonList, new SelectListItemComparer());
        }

        [Fact]
        public void OnActionExecuted_ListWith2Items1Selected_ListContaining2Elements()
        {
            // Arrange
            var invitedPerson1 = new InvitedPerson { Id = 1, Name = "Name" };
            var invitedPerson2 = new InvitedPerson { Id = 1, Name = "Name" };
            var invitedPeople = new List<InvitedPerson> { invitedPerson1, invitedPerson2 };

            _repo.Get(Arg.Any<Expression<Func<InvitedPerson, bool>>>()).Returns(invitedPeople);

            var expectedListItem1 = new SelectListItem { Text = invitedPerson1.Name, Value = invitedPerson1.Name, Selected = true };
            var expectedListItem2 = new SelectListItem { Text = invitedPerson2.Name, Value = invitedPerson2.Name, Selected = true };
            var expectedList = new List<SelectListItem> { expectedListItem1, expectedListItem2 };

            _selectedInvitedPerson.Name = invitedPerson1.Name;

            // Act
            _populateInvitedPeopleFilter.OnActionExecuted(_executedContext);

            // Assert
            var viewBagInvitedPersonList = _controller.ViewBag.InvitedPeopleList as List<SelectListItem>;
            Assert.Equal(expectedList, viewBagInvitedPersonList, new SelectListItemComparer());
        }

        [Fact]
        public void OnActionExecuted_RepoReturnsEmptyList_ListContaining0Element()
        {
            // Arrange
            var emptyList = new List<InvitedPerson>();
            _repo.Get(Arg.Any<Expression<Func<InvitedPerson, bool>>>()).Returns(emptyList);

            // Act
            _populateInvitedPeopleFilter.OnActionExecuted(_executedContext);
            var viewBagInvitedPersonList = _controller.ViewBag.InvitedPeopleList as List<SelectListItem>;

            // Assert
            Assert.Equal(0, viewBagInvitedPersonList.Count);
        }

        [Fact]
        public void OnActionExecuting_Call_ForCoverage()
        {
            // Arrange
            // Act
            _populateInvitedPeopleFilter.OnActionExecuting(_executingContext);

            // Assert
        }
    }
}
