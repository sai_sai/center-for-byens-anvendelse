﻿#region Head
// <copyright file="PopulateCouncilsFilterUnitTests.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;
using NSubstitute;
using Presentation.Web.Filters;
using Xunit;

namespace UnitTests.Filters
{
    public class PopulateCouncilsFilterUnitTests
    {
        private readonly Controller _controller;
        private readonly ActionExecutingContext _executingContext;
        private readonly ActionExecutedContext _executedContext;
        private readonly IGenericRepository<Council> _repo;
        private readonly ISelectedCouncil _selectedCouncil;
        private readonly PopulateCouncilsFilter _populateCouncilsFilter;

        public PopulateCouncilsFilterUnitTests()
        {
            _controller = Substitute.For<Controller>();
            _executingContext = Substitute.For<ActionExecutingContext>();
            _executedContext = Substitute.For<ActionExecutedContext>();
            _repo = Substitute.For<IGenericRepository<Council>>();
            _selectedCouncil = Substitute.For<ISelectedCouncil>();

            _controller.ViewData.Model = _selectedCouncil;
            _executedContext.Controller = _controller;

            _populateCouncilsFilter = new PopulateCouncilsFilter(_repo);
        }

        [Fact]
        public void OnActionExecuted_ListWith1Item_ListContaining1Element()
        {
            // Arrange
            var advisor = new Council {Id = 1, Name = "Name"};
            var advisors = new List<Council> { advisor };

            _repo.Get(Arg.Any<Expression<Func<Council, bool>>>()).Returns(advisors);

            var expectedListItem = new SelectListItem {Text = advisor.Name, Value = advisor.Id.ToString()};
            var expectedList = new List<SelectListItem> { expectedListItem };

            // Act
            _populateCouncilsFilter.OnActionExecuted(_executedContext);

            // Assert
            var viewBagCouncilList = _controller.ViewBag.CouncilList as List<SelectListItem>;
            Assert.Equal(expectedList, viewBagCouncilList, new SelectListItemComparer());
        }

        [Fact]
        public void OnActionExecuted_ListWith2Items1SelectedIsDeleted_IsCouncilDeletedSet()
        {
            // Arrange
            var advisor1 = new Council { Id = 1, Name = "Name" };
            var advisor2 = new Council { Id = 1, Name = "Name", IsDeleted = true };
            var advisors = new List<Council> { advisor1, advisor2 };

            _repo.Get(Arg.Any<Expression<Func<Council, bool>>>()).Returns(advisors);

            // Act
            _populateCouncilsFilter.OnActionExecuted(_executedContext);

            // Assert
            Assert.True(_controller.ViewBag.IsCouncilDeleted);
        }

        [Fact]
        public void OnActionExecuted_ListWith2Items1Selected_ListContaining2Elements()
        {
            // Arrange
            var advisor1 = new Council { Id = 1, Name = "Name" };
            var advisor2 = new Council { Id = 1, Name = "Name" };
            var advisors = new List<Council> { advisor1, advisor2 };

            _repo.Get(Arg.Any<Expression<Func<Council, bool>>>()).Returns(advisors);

            var expectedListItem1 = new SelectListItem { Text = advisor1.Name, Value = advisor1.Id.ToString() };
            var expectedListItem2 = new SelectListItem { Text = advisor2.Name, Value = advisor2.Id.ToString() };
            var expectedList = new List<SelectListItem> { expectedListItem1, expectedListItem2 };

            _selectedCouncil.CouncilId = advisor1.Id;

            // Act
            _populateCouncilsFilter.OnActionExecuted(_executedContext);

            // Assert
            var viewBagCouncilList = _controller.ViewBag.CouncilList as List<SelectListItem>;
            Assert.Equal(expectedList, viewBagCouncilList, new SelectListItemComparer());
        }

        [Fact]
        public void OnActionExecuted_RepoReturnsEmptyList_ListContaining0Element()
        {
            // Arrange
            var emptyList = new List<Council>();
            _repo.Get(Arg.Any<Expression<Func<Council, bool>>>()).Returns(emptyList);

            // Act
            _populateCouncilsFilter.OnActionExecuted(_executedContext);
            var viewBagCouncilList = _controller.ViewBag.CouncilList as List<SelectListItem>;

            // Assert
            Assert.Equal(0, viewBagCouncilList.Count);
        }

        [Fact]
        public void OnActionExecuting_Call_ForCoverage()
        {
            // Arrange
            // Act
            _populateCouncilsFilter.OnActionExecuting(_executingContext);

            // Assert
        }
    }
}
