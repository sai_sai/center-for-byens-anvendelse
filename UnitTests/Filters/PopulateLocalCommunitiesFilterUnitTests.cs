﻿#region Head
// <copyright file="PopulateLocalCommunitiesFilterUnitTests.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;
using NSubstitute;
using Presentation.Web.Filters;
using Xunit;

namespace UnitTests.Filters
{
    public class PopulateLocalCommunitiesFilterUnitTests
    {
        private readonly Controller _controller;
        private readonly ActionExecutingContext _executingContext;
        private readonly ActionExecutedContext _executedContext;
        private readonly IGenericRepository<LocalCommunity> _repo;
        private readonly ISelectedLocalCommunity _selectedLocalCommunity;
        private readonly PopulateLocalCommunitiesFilter _populateLocalCommunitiesFilter;

        public PopulateLocalCommunitiesFilterUnitTests()
        {
            _controller = Substitute.For<Controller>();
            _executingContext = Substitute.For<ActionExecutingContext>();
            _executedContext = Substitute.For<ActionExecutedContext>();
            _repo = Substitute.For<IGenericRepository<LocalCommunity>>();
            _selectedLocalCommunity = Substitute.For<ISelectedLocalCommunity>();

            _controller.ViewData.Model = _selectedLocalCommunity;
            _executedContext.Controller = _controller;

            _populateLocalCommunitiesFilter = new PopulateLocalCommunitiesFilter(_repo);
        }

        [Fact]
        public void OnActionExecuted_ListWith1Item_ListContaining1Element()
        {
            // Arrange
            var localCommunity = new LocalCommunity { Id = 1, Name = "Name" };
            var localCommunities = new List<LocalCommunity> { localCommunity };

            _repo.Get(Arg.Any<Expression<Func<LocalCommunity, bool>>>()).Returns(localCommunities);

            var expectedListItem = new SelectListItem { Text = localCommunity.Name, Value = localCommunity.Id.ToString() };
            var expectedList = new List<SelectListItem> { expectedListItem };

            // Act
            _populateLocalCommunitiesFilter.OnActionExecuted(_executedContext);

            // Assert
            var viewBagLocalCommunityList = _controller.ViewBag.LocalCommunityList as List<SelectListItem>;
            Assert.Equal(expectedList, viewBagLocalCommunityList, new SelectListItemComparer());
        }

        [Fact]
        public void OnActionExecuted_ListWith2Items1SelectedIsDeleted_IsLocalCommunityDeletedSet()
        {
            // Arrange
            var localCommunity1 = new LocalCommunity { Id = 1, Name = "Name" };
            var localCommunity2 = new LocalCommunity { Id = 1, Name = "Name", IsDeleted = true };
            var localCommunities = new List<LocalCommunity> { localCommunity1, localCommunity2 };

            _repo.Get(Arg.Any<Expression<Func<LocalCommunity, bool>>>()).Returns(localCommunities);

            // Act
            _populateLocalCommunitiesFilter.OnActionExecuted(_executedContext);

            // Assert
            Assert.True(_controller.ViewBag.IsLocalCommunityDeleted);
        }

        [Fact]
        public void OnActionExecuted_ListWith2Items1Selected_ListContaining2Elements()
        {
            // Arrange
            var localCommunity1 = new LocalCommunity { Id = 1, Name = "Name" };
            var localCommunity2 = new LocalCommunity { Id = 1, Name = "Name" };
            var localCommunities = new List<LocalCommunity> { localCommunity1, localCommunity2 };

            _repo.Get(Arg.Any<Expression<Func<LocalCommunity, bool>>>()).Returns(localCommunities);

            var expectedListItem1 = new SelectListItem { Text = localCommunity1.Name, Value = localCommunity1.Id.ToString() };
            var expectedListItem2 = new SelectListItem { Text = localCommunity2.Name, Value = localCommunity2.Id.ToString() };
            var expectedList = new List<SelectListItem> { expectedListItem1, expectedListItem2 };

            _selectedLocalCommunity.LocalCommunityId = localCommunity1.Id;

            // Act
            _populateLocalCommunitiesFilter.OnActionExecuted(_executedContext);

            // Assert
            var viewBagLocalCommunityList = _controller.ViewBag.LocalCommunityList as List<SelectListItem>;
            Assert.Equal(expectedList, viewBagLocalCommunityList, new SelectListItemComparer());
        }

        [Fact]
        public void OnActionExecuted_RepoReturnsEmptyList_ListContaining0Element()
        {
            // Arrange
            var emptyList = new List<LocalCommunity>();
            _repo.Get(Arg.Any<Expression<Func<LocalCommunity, bool>>>()).Returns(emptyList);

            // Act
            _populateLocalCommunitiesFilter.OnActionExecuted(_executedContext);
            var viewBagLocalCommunityList = _controller.ViewBag.LocalCommunityList as List<SelectListItem>;

            // Assert
            Assert.Equal(0, viewBagLocalCommunityList.Count);
        }

        [Fact]
        public void OnActionExecuting_Call_ForCoverage()
        {
            // Arrange
            // Act
            _populateLocalCommunitiesFilter.OnActionExecuting(_executingContext);

            // Assert
        }
    }
}
