﻿#region Head
// <copyright file="SearchControllerUnitTests.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using AutoMapper;
using Core.ApplicationServices;
using Core.DomainModel;
using Core.DomainServices;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using Presentation.Web.Controllers;
using Presentation.Web.Models.Search;
using Xunit;

namespace UnitTests
{
    public class SearchControllerUnitTests
    {
        private readonly SearchController _searchController;
        private readonly ISearchService _searchService;
        private readonly IMappingEngine _mapper;

        public SearchControllerUnitTests()
        {
            _searchService = Substitute.For<ISearchService>();
            _mapper = Substitute.For<IMappingEngine>();

            var projectReposity = Substitute.For<IGenericRepository<Project>>();
            _searchController = new SearchController(_mapper, _searchService, projectReposity);
        }

        #region Index
        [Fact]
        public void Index_ViewModelNull_ViewWithEmptyViewModel()
        {
            // Arrange
            SearchViewModel vm = null;

            // Act
            var ret = _searchController.Index(vm) as ViewResult;
            var model = ret.Model as SearchViewModel;

            // Assert
            Assert.False(model.AnyParametersSet());
        }

        [Fact]
        public void Index_ViewModelValid_ViewWithViewModel()
        {
            // Arrange
            var vm = new SearchViewModel();

            // Act
            var ret = _searchController.Index(vm) as ViewResult;
            var model = ret.Model as SearchViewModel;

            // Assert
            Assert.Equal(vm, model);
        }
        #endregion

        #region Index Post
        [Fact]
        public void IndexPost_ViewModelNull_ReturnBadRequest()
        {
            // Arrange
            SearchViewModel vm = null;

            // Act
            var ret = _searchController.IndexPost(vm).Result as HttpStatusCodeResult;

            // Assert
            Assert.Equal((int)HttpStatusCode.BadRequest, ret.StatusCode);
        }

        [Fact]
        public void IndexPost_EmptyViewModel_ViewBagNoSearchParametersSet()
        {
            // Arrange
            var vm = new SearchViewModel();

            // Act
            var ret = _searchController.IndexPost(vm).Result as ViewResult;

            // Assert
            Assert.True(ret.ViewBag.NoSearchParameters);
        }

        [Fact]
        public void IndexPost_EmptyViewModel_ReturnViewModel()
        {
            // Arrange
            var vm = new SearchViewModel();

            // Act
            var ret = _searchController.IndexPost(vm).Result as ViewResult;

            // Assert
            Assert.Equal(vm, ret.Model);
        }

        [Fact]
        public void IndexPost_SearchProjects_SearchServiceCalled()
        {
            // Arrange
            var vm = new SearchViewModel
            {
                SearchInProjects = true,
                SearchInAgendaPoints = false,
                Address = "SomeText"
            };
            var searchParameters = new ProjectSearchParameters();
            _mapper.Map<ProjectSearchParameters>(vm)
                .Returns(searchParameters);

            // Act
            _searchController.IndexPost(vm).Wait();

            // Assert
            _searchService.Received(1).GetProjectsAsync(searchParameters);
        }

        [Fact]
        public void IndexPost_SearchAgendaPoints_SearchServiceCalled()
        {
            // Arrange
            var vm = new SearchViewModel
            {
                SearchInProjects = false,
                SearchInAgendaPoints = true,
                Address = "SomeText"
            };
            var searchParameters = new AgendaPointSearchParameters();
            _mapper.Map<AgendaPointSearchParameters>(vm)
                .Returns(searchParameters);

            // Act
            _searchController.IndexPost(vm).Wait();

            // Assert
            _searchService.Received(1).GetAgendaPointsAsync(searchParameters);
        }

        [Fact]
        public void IndexPost_SearchProjects_ReturnViewWithResults()
        {
            // Arrange
            var vm = new SearchViewModel
            {
                SearchInProjects = true,
                SearchInAgendaPoints = false,
                Address = "SomeText"
            };
            var results = new List<Project>();
            var vmResults = new List<ProjectViewModel>();

            var searchParameters = new ProjectSearchParameters();
            _searchService.GetProjectsAsync(searchParameters)
                .Returns(Task.FromResult<IEnumerable<Project>>(results));

            _mapper.Map<ProjectSearchParameters>(vm)
                .Returns(searchParameters);
            _mapper.Map<IEnumerable<ProjectViewModel>>(results)
                .Returns(vmResults);

            // Act
            var ret = _searchController.IndexPost(vm).Result as ViewResult;
            var model = ret.Model as ResultsViewModel;

            // Assert
            Assert.Equal("Results", ret.ViewName);
            Assert.Equal(vmResults, model.Projects);
        }

        [Fact]
        public void IndexPost_SearchProjects_ReturnViewWithNoResults()
        {
            // Arrange
            var vm = new SearchViewModel
            {
                SearchInProjects = true,
                SearchInAgendaPoints = false,
                Address = "SomeText"
            };

            var searchParameters = new ProjectSearchParameters();
            _searchService.GetProjectsAsync(searchParameters)
                .Returns(Task.FromResult<IEnumerable<Project>>(null));

            _mapper.Map<ProjectSearchParameters>(vm)
                .Returns(searchParameters);

            // Act
            var ret = _searchController.IndexPost(vm).Result as ViewResult;
            var model = ret.Model as ResultsViewModel;

            // Assert
            Assert.Equal("Results", ret.ViewName);
            Assert.Empty(model.Projects);
        }

        [Fact]
        public void IndexPost_SearchAgendaPoints_ReturnViewWithResults()
        {
            // Arrange
            var vm = new SearchViewModel
            {
                SearchInProjects = false,
                SearchInAgendaPoints = true,
                Address = "SomeText"
            };
            var results = new List<AgendaPoint>();
            var vmResults = new List<AgendaPointViewModel>();

            var searchParameters = new AgendaPointSearchParameters();
            _searchService.GetAgendaPointsAsync(searchParameters)
                .Returns(Task.FromResult<IEnumerable<AgendaPoint>>(results));

            _mapper.Map<AgendaPointSearchParameters>(vm)
                .Returns(searchParameters);
            _mapper.Map<IEnumerable<AgendaPointViewModel>>(results)
                .Returns(vmResults);

            // Act
            var ret = _searchController.IndexPost(vm).Result as ViewResult;
            var model = ret.Model as ResultsViewModel;

            // Assert
            Assert.Equal("Results", ret.ViewName);
            Assert.Equal(vmResults, model.AgendaPoints);
        }

        [Fact]
        public void IndexPost_SearchAgendaPoints_ReturnViewWithNoResults()
        {
            // Arrange
            var vm = new SearchViewModel
            {
                SearchInProjects = false,
                SearchInAgendaPoints = true,
                Address = "SomeText"
            };

            var searchParameters = new AgendaPointSearchParameters();
            _searchService.GetAgendaPointsAsync(searchParameters)
                .Returns(Task.FromResult<IEnumerable<AgendaPoint>>(null));

            _mapper.Map<AgendaPointSearchParameters>(vm)
                .Returns(searchParameters);

            // Act
            var ret = _searchController.IndexPost(vm).Result as ViewResult;
            var model = ret.Model as ResultsViewModel;

            // Assert
            Assert.Equal("Results", ret.ViewName);
            Assert.Empty(model.AgendaPoints);
        }

        [Fact]
        public void IndexPost_TaskCanceledExceptionThrown_ViewBagTimeoutSet()
        {
            // Arrange
            var vm = new SearchViewModel
            {
                SearchInProjects = false,
                SearchInAgendaPoints = true,
                Address = "SomeText"
            };

            _searchService.GetAgendaPointsAsync(Arg.Any<AgendaPointSearchParameters>())
                .Returns(e => { throw new TaskCanceledException(); });

            // Act
            _searchController.IndexPost(vm).Wait();

            // Assert
            Assert.True(_searchController.ViewBag.TimeOut);
        }

        #endregion
    }
}
