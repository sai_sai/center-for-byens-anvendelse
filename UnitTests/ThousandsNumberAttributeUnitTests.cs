﻿#region Head
// <copyright file="ThousandsNumberAttributeUnitTests.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using Presentation.Web.Validators;
using Xunit;

namespace UnitTests
{
    public class ThousandsNumberAttributeUnitTests
    {
        private readonly ThousandsNumberAttribute _thousandsNumberAttribute;

        public ThousandsNumberAttributeUnitTests()
        {
            _thousandsNumberAttribute = new ThousandsNumberAttribute();
        }

        [Fact]
        public void IsValid_ValueNull_ReturnFalse()
        {
            // Arrange
            // Act
            var ret = _thousandsNumberAttribute.IsValid(null);

            // Assert
            Assert.IsType<bool>(ret);
            Assert.False(ret);
        }

        [Fact]
        public void IsValid_ValueNotString_ReturnFalse()
        {
            // Arrange
            const int value = 123;

            // Act
            var ret = _thousandsNumberAttribute.IsValid(value);

            // Assert
            Assert.IsType<bool>(ret);
            Assert.False(ret);
        }

        [Fact]
        public void IsValid_ValidNumberNoSeperator_ReturnTrue()
        {
            // Arrange
            const string value = "1000";

            // Act
            var ret = _thousandsNumberAttribute.IsValid(value);

            // Assert
            Assert.IsType<bool>(ret);
            Assert.True(ret);
        }

        [Fact]
        public void IsValid_ValidNumber2Seperators_ReturnTrue()
        {
            // Arrange
            const string value = "1.000.000";

            // Act
            var ret = _thousandsNumberAttribute.IsValid(value);

            // Assert
            Assert.IsType<bool>(ret);
            Assert.True(ret);
        }

        [Fact]
        public void IsValid_NonValidNumberWrongSeperator_ReturnFalse()
        {
            // Arrange
            const string value = "1,000,000";

            // Act
            var ret = _thousandsNumberAttribute.IsValid(value);

            // Assert
            Assert.IsType<bool>(ret);
            Assert.False(ret);
        }

        [Fact]
        public void IsValid_NonValidNumberCharsInNumber_ReturnError()
        {
            // Arrange
            const string value = "1ab000";

            // Act
            var ret = _thousandsNumberAttribute.IsValid(value);

            // Assert
            Assert.IsType<bool>(ret);
            Assert.False(ret);
        }

        [Fact]
        public void IsValid_EmptyString_ReturnError()
        {
            // Arrange
            const string value = "";

            // Act
            var ret = _thousandsNumberAttribute.IsValid(value);

            // Assert
            Assert.IsType<bool>(ret);
            Assert.False(ret);
        }
    }
}
