#region Head
// <copyright file="UserMock.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.IO;
using System.Security.Principal;
using System.Web;

namespace UnitTests
{
    /// <summary>
    /// Mock current user context
    /// </summary>
    public class UserMock
    {
        private string[] _roles;

        /// <summary>
        /// Initialize a new instance of the <see cref="UserMock"/> class.
        /// </summary>
        public UserMock()
        {
            Name = "User";
            SetRoles(new [] {"Admin"});

            HttpContext.Current = new HttpContext(
                new HttpRequest("", "http://tempuri.org", ""),
                new HttpResponse(new StringWriter())
            );
        }

        /// <summary>
        /// Gets or sets current user name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Sets current users roles.
        /// </summary>
        public void SetRoles(string[] roles)
        {
            _roles = roles;
        }

        /// <summary>
        /// Gest current users roles.
        /// </summary>
        public string[] GetRoles()
        {
            return _roles;
        }

        /// <summary>
        /// Log user on.
        /// </summary>
        public void LogOn()
        {
            HttpContext.Current.User = new GenericPrincipal(
                new GenericIdentity(Name),
                _roles
            );
        }

        /// <summary>
        /// Log user off.
        /// </summary>
        public void LogOff()
        {
            HttpContext.Current.User = new GenericPrincipal(
                new GenericIdentity(String.Empty),
                new string[0]
            );
        }
    }
}
