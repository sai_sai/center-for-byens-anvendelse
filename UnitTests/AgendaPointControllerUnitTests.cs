﻿#region Head
// <copyright file="AgendaPointControllerUnitTests.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Core.DomainModel;
using Core.DomainServices;
using NSubstitute;
using Presentation.Web.Controllers;
using Presentation.Web.Helpers;
using Presentation.Web.Models;
using Presentation.Web.Models.AgendaPoint;
using Presentation.Web.Models.Shared;
using Xunit;

namespace UnitTests
{
    public class AgendaPointControllerUnitTests
    {
        private readonly AgendaPointController _agendaPointController;

        private readonly IGenericRepository<AgendaPoint> _agendaPointRepo;
        private readonly IGenericRepository<Project> _projectRepo;
        private readonly IGenericRepository<Meeting> _meetingRepo;
        private readonly IGenericRepository<AgendaPointDocument> _agendaPointDocumentRepository;
        private readonly IGenericRepository<Topic> _topicRepo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMappingEngine _mapper;
        private readonly IDateTime _dateTime;
        private readonly IDawaRepository _dawaRepo;
        private readonly IDocumentHelper _documentHelper;
        private readonly UserMock _userMock;

        private readonly CreateAgendaPointViewModel _createAgendaPointViewModel;
        private readonly EditAgendaPointViewModel _editAgendaPointViewModel;
        private readonly DetailsAgendaPointViewModel _detailsAgendaPointViewModel;
        private readonly IndexAgendaPointViewModel _indexAgendaPointViewModel;
        private readonly CreateTopicViewModel _createTopicViewModel;
        private readonly AgendaPoint _agendaPoint;

        public AgendaPointControllerUnitTests()
        {
            _agendaPointRepo = Substitute.For<IGenericRepository<AgendaPoint>>();
            _agendaPointDocumentRepository = Substitute.For<IGenericRepository<AgendaPointDocument>>();
            _projectRepo = Substitute.For<IGenericRepository<Project>>();
            _meetingRepo = Substitute.For<IGenericRepository<Meeting>>();
            _topicRepo = Substitute.For<IGenericRepository<Topic>>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _mapper = Substitute.For<IMappingEngine>();
            _dateTime = Substitute.For<IDateTime>();
            _dawaRepo = Substitute.For<IDawaRepository>();
            _documentHelper = Substitute.For<IDocumentHelper>();

            var documentRepository = Substitute.For<IGenericRepository<Document>>();

            // fill with dummy data
            var documentList = new List<AgendaPointDocument>
            {
                new AgendaPointDocument
                {
                    FileName = "lala"
                }
            };

            _createAgendaPointViewModel = new CreateAgendaPointViewModel();
            _editAgendaPointViewModel = new EditAgendaPointViewModel();
            _detailsAgendaPointViewModel = new DetailsAgendaPointViewModel();
            _indexAgendaPointViewModel = new IndexAgendaPointViewModel();
            _createTopicViewModel = new CreateTopicViewModel();
            _agendaPoint = new AgendaPoint();

            _mapper.Map<CreateAgendaPointViewModel>(Arg.Any<AgendaPoint>())
                .Returns(_createAgendaPointViewModel);
            _mapper.Map(Arg.Any<EditAgendaPointViewModel>(), Arg.Any<AgendaPoint>())
                .Returns(_agendaPoint);
            _mapper.Map<EditAgendaPointViewModel>(Arg.Any<AgendaPoint>())
                .Returns(_editAgendaPointViewModel);
            _mapper.Map<DetailsAgendaPointViewModel>(Arg.Any<AgendaPoint>())
                .Returns(_detailsAgendaPointViewModel);
            _mapper.Map<IndexAgendaPointViewModel>(Arg.Any<AgendaPoint>())
                .Returns(_indexAgendaPointViewModel);
            _mapper.Map<IEnumerable<CreateTopicViewModel>>(Arg.Any<IEnumerable<Topic>>())
                .Returns(new List<CreateTopicViewModel> { _createTopicViewModel });
            _mapper.Map<AgendaPoint>(_createAgendaPointViewModel)
                .Returns(_agendaPoint);
            documentRepository.Get(Arg.Any<Expression<Func<Document, bool>>>())
                .Returns(documentList);

            var geoCoder = Substitute.For<IGeoCoder>();

            _userMock = new UserMock();
            _userMock.LogOn();
            _agendaPointController = new AgendaPointController(
                _agendaPointRepo,
                _projectRepo,
                _meetingRepo,
                _topicRepo,
                _unitOfWork,
                _mapper,
                _dateTime,
                _agendaPointDocumentRepository,
                _documentHelper,
                geoCoder,
                _dawaRepo);
        }

        #region Index
        [Fact]
        public void Index_Call_ReturnsView()
        {
            // Arrange
            // Act
            var ret = _agendaPointController.Index();

            // Assert
            Assert.IsType(typeof(ViewResult), ret);
        }

        [Fact]
        public void Index_Call_ReturnsIndexViewModel()
        {
            // Arrange
            // Act
            var ret = _agendaPointController.Index() as ViewResult;

            // Assert
            Assert.IsAssignableFrom<IEnumerable<IndexAgendaPointViewModel>>(ret.Model);
        }

        [Fact]
        public void Index_Call_CallsAgendaPointsInRepo()
        {
            // Arrange
            // Act
            _agendaPointController.Index();

            // Assert
            _agendaPointRepo.Received(1).Get(Arg.Any<Expression<Func<AgendaPoint, bool>>>());
        }
        #endregion

        #region Create Get
        [Fact]
        public void Create_Call_ReturnsView()
        {
            // Arrange
            // Act
            var ret = _agendaPointController.Create();

            // Assert
            Assert.IsType<ViewResult>(ret);
        }

        [Fact]
        public void Create_Call_ReturnsViewModel()
        {
            // Arrange
            // Act
            var model = _agendaPointController.Create() as ViewResult;

            // Assert
            Assert.IsType<CreateAgendaPointViewModel>(model.Model);
        }

        [Fact]
        public void Create_Call_StreetTypeIsPublicStreet()
        {
            // Arrange
            // Act
            var view = _agendaPointController.Create() as ViewResult;
            var model = view.Model as CreateAgendaPointViewModel;

            // Assert
            Assert.IsType<StreetTypeViewModel>(model.StreetType);
            Assert.Equal(StreetTypeViewModel.PublicStreet, model.StreetType);
        }

        [Fact]
        public void Create_Call_StatusIsAwaiting()
        {
            // Arrange
            // Act
            var view = _agendaPointController.Create() as ViewResult;
            var model = view.Model as CreateAgendaPointViewModel;

            // Assert
            Assert.IsType<AgendaPointStatusTypeViewModel>(model.AgendaPointStatus);
            Assert.Equal(AgendaPointStatusTypeViewModel.Draft, model.AgendaPointStatus);
        }

        [Fact]
        public void Create_Call_MeetingsidListInitialised()
        {
            // Arrange
            // Act
            var view = _agendaPointController.Create() as ViewResult;
            var model = view.Model as CreateAgendaPointViewModel;

            // Assert
            Assert.IsAssignableFrom<IEnumerable<string>>(model.MeetingIds);
            Assert.Equal(0, model.MeetingIds.Count());
        }
        #endregion

        #region Create Post
        [Fact]
        public void CreatePost_ModelValid_RedirectAction()
        {
            // Arrange
            // Act
            var ret = _agendaPointController.Create(_createAgendaPointViewModel);

            // Assert
            Assert.IsType<RedirectToRouteResult>(ret);
        }

        [Fact]
        public void CreatePost_ModelValid_PropertyCreatedOnSet()
        {
            // Arrange
            var dateTime = new DateTime(2015, 1, 1);
            _dateTime.Now.Returns(dateTime);

            // Act
            _agendaPointController.Create(_createAgendaPointViewModel);

            // Assert
            _agendaPointRepo.Received(1).Insert(Arg.Is<AgendaPoint>(a => a.CreatedOn.Equals(dateTime)));
        }

        [Fact]
        public void CreatePost_ModelValid1MeetingId_PreviousMeetingHoldsId()
        {
            // Arrange
            const string id = "1234";
            int idInt = int.Parse(id);
            var meeting = new Meeting { Id = idInt };
            _createAgendaPointViewModel.MeetingIds = new List<string> { id };
            _meetingRepo.GetByKey(idInt).Returns(meeting);

            // Act
            _agendaPointController.Create(_createAgendaPointViewModel);

            // Assert
            _agendaPointRepo.Received(1).Insert(Arg.Is<AgendaPoint>(a => a.PreviousMeetings.Count.Equals(1)));
            _agendaPointRepo.Received(1).Insert(Arg.Is<AgendaPoint>(a => a.PreviousMeetings.First().Id.Equals(idInt)));
        }

        [Fact]
        public void CreatePost_ModelValid2MeetingId_PreviousMeetingHoldsLastId()
        {
            // Arrange
            const string id1 = "1234";
            const string id2 = "4321";
            _createAgendaPointViewModel.MeetingIds = new List<string> { id1, id2 };
            var id1Int = int.Parse(id1);
            var id2Int = int.Parse(id2);
            var meeting2 = new Meeting { Id = id2Int };
            _meetingRepo.GetByKey(id1Int).Returns((Meeting)null);
            _meetingRepo.GetByKey(id2Int).Returns(meeting2);

            // Act
            _agendaPointController.Create(_createAgendaPointViewModel);

            // Assert
            _agendaPointRepo.Received(1).Insert(Arg.Is<AgendaPoint>(a => a.PreviousMeetings.Count.Equals(1)));
            _agendaPointRepo.Received(1).Insert(Arg.Is<AgendaPoint>(a => a.PreviousMeetings.Last().Id.Equals(id2Int)));
        }

        [Fact]
        public void CreatePost_ModelValid_CallCreateInRepo()
        {
            // Arrange
            // Act
            _agendaPointController.Create(_createAgendaPointViewModel);

            // Assert
            _agendaPointRepo.Received(1).Insert(_agendaPoint);
            _unitOfWork.Received(1).Save();
        }

        [Fact]
        public void CreatePost_ModelValidTopicAttached_AttachedTopicOnModel()
        {
            // Arrange
            var topic = new Topic { Id = 1 };
            _createAgendaPointViewModel.TopicId = topic.Id;
            _topicRepo.GetByKey(topic.Id)
                .Returns(topic);

            // Act
            _agendaPointController.Create(_createAgendaPointViewModel);

            // Assert
            Assert.Equal(topic, _agendaPoint.Topic);
        }

        [Fact]
        public void CreatePost_ModelNotvalid_ReturnViewModel()
        {
            // Arrange
            _agendaPointController.ModelState.AddModelError("error", "error");

            // Act
            var ret = _agendaPointController.Create(_createAgendaPointViewModel) as ViewResult;

            // Assert
            Assert.IsType<CreateAgendaPointViewModel>(ret.Model);
            Assert.Equal(_createAgendaPointViewModel, ret.Model);
        }

        [Fact]
        public void CreatePost_ModelNotValid_MeetingsidListInitialised()
        {
            // Arrange
            _agendaPointController.ModelState.AddModelError("error", "error");

            // Act
            var view = _agendaPointController.Create(_createAgendaPointViewModel) as ViewResult;
            var model = view.Model as CreateAgendaPointViewModel;

            // Assert
            Assert.IsAssignableFrom<IEnumerable<string>>(model.MeetingIds);
            Assert.Equal(0, model.MeetingIds.Count());
        }

        [Fact]
        public void CreatePost_ModelNotValidProjectIdSet_ProjectNameRetreived()
        {
            // Arrange
            const int id = 1234;
            var project = new Project();
            _projectRepo.GetByKey(1234).Returns(project);
            _createAgendaPointViewModel.ProjectId = id;
            _agendaPointController.ModelState.AddModelError("error", "error");

            // Act
            _agendaPointController.Create(_createAgendaPointViewModel);

            // Assert
            _projectRepo.Received(1).GetByKey(id);
        }

        [Fact]
        public void CreatePost_ModelNotValidProjectIdSetRepoReturnsNull_ReturnedViewModelProjectIdNull()
        {
            // Arrange
            const int id = 1;
            _projectRepo.GetByKey(id).Returns((Project)null);
            _createAgendaPointViewModel.ProjectId = id;
            _agendaPointController.ModelState.AddModelError("error", "error");

            // Act
            var ret = _agendaPointController.Create(_createAgendaPointViewModel) as ViewResult;
            var model = ret.Model as CreateAgendaPointViewModel;

            // Assert
            Assert.Null(model.ProjectId);
        }
        #endregion

        #region Details
        [Fact]
        public void Details_IdNull_ReturnsBadRequest()
        {
            // Arrange
            int? id = null;

            // Act
            var ret = _agendaPointController.Details(id);
            var httpStatusCode = ret as HttpStatusCodeResult;

            // Assert
            Assert.IsType<HttpStatusCodeResult>(ret);
            Assert.Equal((int)HttpStatusCode.BadRequest, httpStatusCode.StatusCode);
        }

        [Fact]
        public void Details_IdNonExistingInRepo_ReturnsNotFound()
        {
            // Arrange
            int? id = 1;
            _agendaPointRepo.GetByKey(id).Returns((AgendaPoint)null);

            // Act
            var ret = _agendaPointController.Details(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void Details_IdValidRepoReturnsAgendaPoint_ReturnsViewModel()
        {
            // Arrange
            int? id = 1;
            _agendaPointRepo.GetByKey(id).Returns(_agendaPoint);

            // Act
            var model = _agendaPointController.Details(id) as ViewResult;

            // Assert
            Assert.IsType<DetailsAgendaPointViewModel>(model.Model);
        }
        #endregion

        #region Edit Get
        [Fact]
        public void Edit_IdNull_ReturnsBadRequest()
        {
            // Arrange
            int? id = null;

            // Act
            var ret = _agendaPointController.Edit(id);
            var httpStatusCode = ret as HttpStatusCodeResult;

            // Assert
            Assert.IsType<HttpStatusCodeResult>(ret);
            Assert.Equal((int)HttpStatusCode.BadRequest, httpStatusCode.StatusCode);
        }

        [Fact]
        public void Edit_IdNonExistingInRepo_ReturnsNotFound()
        {
            // Arrange
            int? id = 1;
            _agendaPointRepo.GetByKey(id).Returns((AgendaPoint)null);

            // Act
            var ret = _agendaPointController.Edit(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void Edit_IdValidRepoReturnsAgendaPoint_ReturnsViewModel()
        {
            // Arrange
            int? id = 1;
            _agendaPointRepo.GetByKey(id).Returns(_agendaPoint);

            // Act
            var model = _agendaPointController.Edit(id) as ViewResult;

            // Assert
            Assert.IsType<EditAgendaPointViewModel>(model.Model);
        }

        [Fact]
        public void Edit_IdValidRepoReturnsAgendaPointWithPriviousMeetings_MeetingsidListInitialised()
        {
            // Arrange
            int? id = 1;
            var meetings = new List<Meeting>()
            {
                new Meeting {Id = 1},
                new Meeting {Id = 2}
            };
            _agendaPoint.PreviousMeetings = meetings;
            _agendaPointRepo.GetByKey(id).Returns(_agendaPoint);

            // Act
            var view = _agendaPointController.Edit(id) as ViewResult;
            var model = view.Model as EditAgendaPointViewModel;

            // Assert
            Assert.IsAssignableFrom<IEnumerable<string>>(model.MeetingIds);
            Assert.Equal(meetings.Count, model.MeetingIds.Count());
        }

        [Fact]
        public void Edit_IdValidRepoReturnsAgendaPointWithValidProjectId_ProjectIdSet()
        {
            // Arrange
            int? id = 1;
            const int projectId = 1;
            var project = new Project { Id = projectId };
            _agendaPoint.ProjectId = projectId;
            _agendaPointRepo.GetByKey(id).Returns(_agendaPoint);
            _projectRepo.GetByKey(projectId).Returns(project);
            _editAgendaPointViewModel.ProjectId = projectId;

            // Act
            var view = _agendaPointController.Edit(id) as ViewResult;
            var model = view.Model as EditAgendaPointViewModel;

            // Assert
            Assert.Equal(project.Id, model.ProjectId);
        }

        [Fact]
        public void Edit_IdValidRepoReturnsAgendaPointWithNonValidProjectId_ProjectIdNull()
        {
            // Arrange
            int? id = 1;
            const int projectId = 1;
            _agendaPoint.ProjectId = projectId;
            _agendaPointRepo.GetByKey(id).Returns(_agendaPoint);
            _projectRepo.GetByKey(projectId).Returns((Project)null);
            _editAgendaPointViewModel.ProjectId = projectId;

            // Act
            var view = _agendaPointController.Edit(id) as ViewResult;
            var model = view.Model as EditAgendaPointViewModel;

            // Assert
            Assert.Null(model.ProjectId);
        }
        #endregion

        #region Edit Post
        [Fact]
        public void EditPost_ModelValidRepoReturnsNoAgendaPoint_ReturnHttpBadRequest()
        {
            // Arrange
            const int id = 1;
            _editAgendaPointViewModel.Id = id;
            _agendaPointRepo.GetByKey(id).Returns((AgendaPoint)null);

            // Act
            var ret = _agendaPointController.Edit(_editAgendaPointViewModel);
            var httpStatusCode = ret as HttpStatusCodeResult;

            // Assert
            Assert.IsType<HttpStatusCodeResult>(ret);
            Assert.Equal((int)HttpStatusCode.BadRequest, httpStatusCode.StatusCode);
        }

        [Fact]
        public void EditPost_ModelValid_RedirectAction()
        {
            // Arrange
            const int id = 1;
            _editAgendaPointViewModel.Id = id;
            _agendaPointRepo.GetByKey(id).Returns(_agendaPoint);

            // Act
            var ret = _agendaPointController.Edit(_editAgendaPointViewModel);

            // Assert
            Assert.IsType<RedirectToRouteResult>(ret);
        }

        [Fact]
        public void EditPost_ModelValid_CallCreateInRepo()
        {
            // Arrange
            const int id = 1;
            _editAgendaPointViewModel.Id = id;
            _agendaPointRepo.GetByKey(id).Returns(_agendaPoint);

            // Act
            _agendaPointController.Edit(_editAgendaPointViewModel);

            // Assert
            _agendaPointRepo.Received(1).Update(_agendaPoint);
            _unitOfWork.Received(1).Save();
        }

        [Fact]
        public void EditPost_ModelNotValid_ReturnViewModel()
        {
            // Arrange
            const int id = 1;
            _editAgendaPointViewModel.Id = id;
            _agendaPointRepo.GetByKey(id).Returns(_agendaPoint);
            _agendaPointController.ModelState.AddModelError("error", "error");

            // Act
            var ret = _agendaPointController.Edit(_editAgendaPointViewModel) as ViewResult;

            // Assert
            Assert.IsType<EditAgendaPointViewModel>(ret.Model);
            Assert.Equal(_editAgendaPointViewModel, ret.Model);
        }

        [Fact]
        public void EditPost_ModelNotValidMeetingIdsListNull_MeetingsidListInitialised()
        {
            // Arrange
            const int id = 1;
            _editAgendaPointViewModel.Id = id;
            _agendaPointRepo.GetByKey(id).Returns(_agendaPoint);
            _agendaPointController.ModelState.AddModelError("error", "error");

            // Act
            var view = _agendaPointController.Edit(_editAgendaPointViewModel) as ViewResult;
            var model = view.Model as EditAgendaPointViewModel;

            // Assert
            Assert.IsAssignableFrom<IEnumerable<string>>(model.MeetingIds);
            Assert.Equal(0, model.MeetingIds.Count());
        }

        [Fact]
        public void EditPost_ModelNotValidProjectIdSet_ProjectNameRetreived()
        {
            // Arrange
            const int projectId = 1;
            var project = new Project { Id = projectId };
            _projectRepo.GetByKey(projectId)
                .Returns(project);
            _editAgendaPointViewModel.Id = 1;
            _editAgendaPointViewModel.ProjectId = projectId;
            _agendaPointRepo.GetByKey(_editAgendaPointViewModel.Id)
                .Returns(_agendaPoint);

            _agendaPointController.ModelState.AddModelError("error", "error");

            // Act
            _agendaPointController.Edit(_editAgendaPointViewModel);

            // Assert
            _projectRepo.Received(1).GetByKey(projectId);
        }

        [Fact]
        public void EditPost_ModelNotValidProjectIdSetRepoReturnsNull_ReturnBadRequest()
        {
            // Arrange
            const HttpStatusCode expectedHttpStatusCode = HttpStatusCode.BadRequest;
            const int projectId = 1;
            _projectRepo.GetByKey(projectId)
                .Returns((Project)null);
            _agendaPointController.ModelState.AddModelError("error", "error");

            // Act
            var ret = _agendaPointController.Edit(_editAgendaPointViewModel) as HttpStatusCodeResult;

            // Assert
            Assert.Equal((int)expectedHttpStatusCode, ret.StatusCode);
        }

        [Fact]
        public void EditPost_ModelValid_PropertyModifiedOn()
        {
            // Arrange
            const int id = 1;
            _editAgendaPointViewModel.Id = id;
            _agendaPointRepo.GetByKey(id).Returns(_agendaPoint);
            var dateTime = new DateTime(2015, 1, 1);
            _dateTime.Now.Returns(dateTime);

            // Act
            _agendaPointController.Edit(_editAgendaPointViewModel);

            // Assert
            _agendaPointRepo.Received(1).Update(Arg.Is<AgendaPoint>(a => a.ModifiedOn.Equals(dateTime)));
        }

        [Fact]
        public void EditPost_ModelValidRemovedMeetingId_MeetingRemovedFromAgendaPoint()
        {
            // Arrange
            const int id = 1;
            const int meetingId1 = 1;
            const int meetingId2 = 2;
            _editAgendaPointViewModel.Id = id;
            _editAgendaPointViewModel.MeetingIds = new List<string> { meetingId1.ToString(), meetingId2.ToString() };
            _agendaPoint.PreviousMeetings = new List<Meeting>
            {
                new Meeting {Id = meetingId1},
                new Meeting {Id = meetingId2}
            };
            _editAgendaPointViewModel.RemovedMeetingIds = meetingId2 + " " + meetingId2 + " ";
            _agendaPointRepo.GetByKey(id).Returns(_agendaPoint);

            // Act
            _agendaPointController.Edit(_editAgendaPointViewModel);

            // Assert
            Assert.Equal(1, _agendaPoint.PreviousMeetings.Count);
        }

        [Fact]
        public void EditPost_ModelValidRemovedMeetingIdNotValid_MeetingNotRemovedFromAgendaPoint()
        {
            // Arrange
            const int id = 1;
            const int meetingId1 = 1;
            const int meetingId2 = 2;
            const string nonValidId = "abc";
            _editAgendaPointViewModel.Id = id;
            _editAgendaPointViewModel.MeetingIds = new List<string> { meetingId1.ToString(), meetingId2.ToString() };
            _agendaPoint.PreviousMeetings = new List<Meeting>
            {
                new Meeting {Id = meetingId1},
                new Meeting {Id = meetingId2}
            };
            _editAgendaPointViewModel.RemovedMeetingIds = nonValidId + " ";
            _agendaPointRepo.GetByKey(id).Returns(_agendaPoint);

            // Act
            _agendaPointController.Edit(_editAgendaPointViewModel);

            // Assert
            Assert.Equal(2, _agendaPoint.PreviousMeetings.Count);
        }

        [Fact]
        public void EditPost_ModelValidAddedMeetingId_MeetingAddedToAgendaPoint()
        {
            // Arrange
            const int id = 1;
            const int meetingId1 = 1;
            const int meetingId2 = 2;
            const int newMeetingId = 3;
            _editAgendaPointViewModel.Id = id;
            _editAgendaPointViewModel.MeetingIds = new List<string> { meetingId1.ToString(), meetingId2.ToString(), newMeetingId.ToString() };
            _meetingRepo.GetByKey(newMeetingId).Returns(new Meeting { Id = newMeetingId });
            _agendaPoint.PreviousMeetings = new List<Meeting>
            {
                new Meeting {Id = meetingId1},
                new Meeting {Id = meetingId2}
            };
            _agendaPointRepo.GetByKey(id).Returns(_agendaPoint);

            // Act
            _agendaPointController.Edit(_editAgendaPointViewModel);

            // Assert
            Assert.Equal(3, _agendaPoint.PreviousMeetings.Count);
        }
        #endregion

        #region UploadFile

        [Fact]
        public void UploadFile_ValidState_UploadFileCalledInDocumentHelper()
        {
            // Arrange

            // Act
            _agendaPointController.SaveUploadedFile();

            // Assert
            _documentHelper.Received(1).UploadFile(Arg.Any<HttpRequestBase>(), Arg.Any<HttpServerUtilityBase>());
        }

        #endregion

        #region DownloadFile

        [Fact]
        public void DownloadFile_ValidState_DownloadCalledInDocumentHelper()
        {
            // Arrange
            var guid = new Guid();

            // Act
            _agendaPointController.Download(guid);

            // Assert
            _documentHelper.Received(1).Download(_agendaPointController.Server, guid);
        }

        #endregion

        #region RemoveFile

        [Fact]
        public void DeleteFile_ValidState_DeleteCalledInDocumentHelper()
        {
            // Arrange
            var guid = new Guid();
            var guidList = new List<Guid>
            {
                guid
            };

            // Act
            _agendaPointController.DeleteFilesFromServer(guidList);

            // Assert
            _documentHelper.Received(1).DeleteFiles(guidList, _agendaPointController.Server);
        }

        #endregion
    }
}
