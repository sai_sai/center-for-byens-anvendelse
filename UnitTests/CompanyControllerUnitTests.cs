﻿#region Head
// <copyright file="CompanyControllerUnitTests.cs" company="IT Minds" year="2016">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using Core.DomainModel;
using Core.DomainServices;
using NSubstitute;
using Presentation.Web.Controllers;
using Presentation.Web.Models.Company;
using Xunit;

namespace UnitTests
{
    public class CompanyControllerUnitTests
    {
        private readonly CompanyController _companyController;
        private readonly IGenericRepository<Company> _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMappingEngine _mapper;

        private readonly CreateCompanyViewModel _createCompanyViewModel;
        private readonly DeleteCompanyViewModel _deleteCompanyViewModel;
        private readonly EditCompanyViewModel _editCompanyViewModel;

        public CompanyControllerUnitTests()
        {
            _repo = Substitute.For<IGenericRepository<Company>>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _mapper = Substitute.For<IMappingEngine>();
            new UserMock().LogOn(); // setting user mock

            _createCompanyViewModel = new CreateCompanyViewModel();
            _deleteCompanyViewModel = new DeleteCompanyViewModel();
            _editCompanyViewModel = new EditCompanyViewModel();

            _mapper.Map<CreateCompanyViewModel>(Arg.Any<Company>())
                .Returns(_createCompanyViewModel);
            _mapper.Map<DeleteCompanyViewModel>(Arg.Any<Company>())
                .Returns(_deleteCompanyViewModel);
            _mapper.Map<EditCompanyViewModel>(Arg.Any<Company>())
                .Returns(_editCompanyViewModel);
            _mapper.Map<Company>(Arg.Any<EditCompanyViewModel>())
                .Returns(new Company());
            _mapper.Map<Company>(Arg.Any<CreateCompanyViewModel>())
                .Returns(new Company());

            _companyController = new CompanyController(_repo, _unitOfWork, _mapper);
        }

        #region Index
        [Fact]
        public void Index_Call_ReturnsView()
        {
            // Arrange
            // Act
            var ret = _companyController.Index();

            // Assert
            Assert.IsType(typeof(ViewResult), ret);
        }

        [Fact]
        public void Index_Call_ReturnsIndexCompanyViewModel()
        {
            // Arrange
            var companyList = new List<Company>
            {
                new Company(),
                new Company()
            };
            _repo.Get().Returns(companyList);

            // Act
            var ret = _companyController.Index() as ViewResult;

            // Assert
            Assert.NotNull(ret);
            Assert.IsAssignableFrom<IEnumerable<IndexCompanyViewModel>>(ret.Model);
        }

        [Fact]
        public void Index_Call_CallsCompanysInRepo()
        {
            // Arrange
            // Act
            _companyController.Index();

            // Assert
            _repo.Received(1).Get(Arg.Any<Expression<Func<Company, bool>>>());
        }
        #endregion

        #region Create Get
        [Fact]
        public void Create_Call_ReturnsView()
        {
            // Arrange
            // Act
            var ret = _companyController.Create();

            // Assert
            Assert.IsType(typeof(ViewResult), ret);
        }

        [Fact]
        public void Create_Call_ReturnsViewModel()
        {
            // Arrange
            // Act
            var ret = _companyController.Create() as ViewResult;

            // Assert
            Assert.NotNull(ret);
            Assert.NotNull(ret.Model);
        }
        #endregion

        #region Create Post
        [Fact]
        public void CreatePost_ModelValid_RedirectAction()
        {
            // Arrange
            var vm = new CreateCompanyViewModel();

            // Act
            var ret = _companyController.Create(vm);

            // Assert
            Assert.IsType(typeof(RedirectToRouteResult), ret);
        }

        [Fact]
        public void CreatePost_ModelInvalid_ReturnView()
        {
            // Arrange
            var vm = new CreateCompanyViewModel();
            _companyController.ModelState.AddModelError("error", "error");

            // Act
            var ret = _companyController.Create(vm) as ViewResult;

            // Assert
            Assert.NotNull(ret);
            Assert.IsType<CreateCompanyViewModel>(ret.Model);
        }

        [Fact]
        public void CreatePost_ModelValid_CallCreateInRepo()
        {
            // Arrange
            var vm = new CreateCompanyViewModel();

            // Act
            _companyController.Create(vm);

            // Assert
            _repo.Received(1).Insert(Arg.Any<Company>());
            _unitOfWork.Received(1).Save();
        }
        #endregion

        #region Edit Get
        [Fact]
        public void Edit_IdValid_ReturnView()
        {
            // Arrange
            var company = new Company();
            int? id = 1;
            _repo.GetByKey(id).Returns(company);

            // Act
            var ret = _companyController.Edit(id);

            // Assert
            Assert.IsAssignableFrom<ViewResult>(ret);
        }

        [Fact]
        public void Edit_IdValid_ReturnViewModel()
        {
            // Arrange
            var company = new Company();
            int? id = 1;
            _repo.GetByKey(id).Returns(company);

            // Act
            var ret = _companyController.Edit(id) as ViewResult;

            // Assert
            Assert.IsType<EditCompanyViewModel>(ret.Model);
        }

        [Fact]
        public void Edit_IdNull_ReturnBadRequest()
        {
            // Arrange
            int? id = null;
            const int expectedStatusCode = 400;

            // Act
            var ret = _companyController.Edit(id) as HttpStatusCodeResult;

            // Assert
            Assert.NotNull(ret);
            Assert.Equal(expectedStatusCode, ret.StatusCode);
        }

        [Fact]
        public void Edit_IdValid_CallGetByKeyInRepo()
        {
            // Arrange
            int? id = 1;

            // Act
            _companyController.Edit(id);

            // Assert
            _repo.Received(1).GetByKey(id);
        }

        [Fact]
        public void Edit_IdValidCallGetByKeyInRepoReturnsNull_ReturnNotFound()
        {
            // Arrange
            int? id = 1;
            _repo.GetByKey(id).Returns((Company)null);

            // Act
            var ret = _companyController.Edit(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void Edit_IdValidCompanyDeleted_ReturnNotFound()
        {
            // Arrange
            int? id = 1;
            var company = new Company {IsDeleted = true};
            _repo.GetByKey(id).Returns(company);

            // Act
            var ret = _companyController.Edit(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }
        #endregion

        #region Edit Post
        [Fact]
        public void EditPost_ModelValid_RedirectAction()
        {
            // Arrange
            var vm = new EditCompanyViewModel();

            // Act
            var ret = _companyController.Edit(vm);

            // Assert
            Assert.IsType<RedirectToRouteResult>(ret);
        }

        [Fact]
        public void EditPost_ModelInvalid_ReturnView()
        {
            // Arrange
            var vm = new EditCompanyViewModel();
            _companyController.ModelState.AddModelError("error", "error");

            // Act
            var ret = _companyController.Edit(vm) as ViewResult;

            // Assert
            Assert.NotNull(ret);
            Assert.IsType<EditCompanyViewModel>(ret.Model);
        }

        [Fact]
        public void EditPost_ModelValid_CallUpdateInRepo()
        {
            // Arrange
            var vm = new EditCompanyViewModel();

            // Act
            _companyController.Edit(vm);

            // Assert
            _repo.Received(1).Update(Arg.Any<Company>());
            _unitOfWork.Received(1).Save();
        }
        #endregion

        #region Delete
        [Fact]
        public void Delete_IdValid_ReturnView()
        {
            // Arrange
            var company = new Company();
            int? id = 1;
            _repo.GetByKey(id).Returns(company);

            // Act
            var ret = _companyController.Delete(id);

            // Assert
            Assert.IsType<ViewResult>(ret);
        }

        [Fact]
        public void Delete_IdValid_ReturnViewModel()
        {
            // Arrange
            var company = new Company();
            int? id = 1;
            _repo.GetByKey(id).Returns(company);

            // Act
            var ret = _companyController.Delete(id) as ViewResult;

            // Assert
            Assert.IsType<DeleteCompanyViewModel>(ret.Model);
        }

        [Fact]
        public void Delete_IdValid_CallGetByKeyInRepo()
        {
            // Arrange
            var company = new Company();
            int? id = 1;
            _repo.GetByKey(id).Returns(company);

            // Act
            _companyController.Delete(id);

            // Assert
            _repo.Received(1).GetByKey(id);
        }

        [Fact]
        public void Delete_IdValidRepoGetByKeyReturnsNull_ReturnHttpNotFound()
        {
            // Arrange
            int? id = 1;
            _repo.GetByKey(id).Returns((Company)null);

            // Act
            var ret = _companyController.Delete(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void Delete_IdValidCompanyDeleted_ReturnHttpNotFound()
        {
            // Arrange
            const int id = 1;
            var company = new Company { IsDeleted = true };
            _repo.GetByKey(id).Returns(company);

            // Act
            var ret = _companyController.Delete(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void Delete_IdNull_ReturnBadRequest()
        {
            // Arrange
            int? id = null;

            // Act
            var ret = _companyController.Delete(id) as HttpStatusCodeResult;

            // Assert
            _repo.DidNotReceive().GetByKey(Arg.Any<int?>());
            Assert.Equal((int)HttpStatusCode.BadRequest, ret.StatusCode);
        }
        #endregion

        #region DeleteConfirmed
        [Fact]
        public void DeleteConfirmed_IdValid_RepoUpdateCalled()
        {
            // Arrange
            const int id = 1;
            var company = new Company();
            _repo.GetByKey(id).Returns(company);

            // Act
            _companyController.DeleteConfirmed(id);

            // Assert
            _repo.Received(1).Update(company);
            _unitOfWork.Received(1).Save();
            Assert.True(company.IsDeleted);
        }

        [Fact]
        public void DeleteConfirmed_IdValid_RedirectAction()
        {
            // Arrange
            const int id = 1;
            var company = new Company();
            _repo.GetByKey(id).Returns(company);

            // Act
            var ret = _companyController.DeleteConfirmed(id);

            // Assert
            Assert.IsType<RedirectToRouteResult>(ret);
        }

        [Fact]
        public void DeleteConfirmed_IdNotValid_ReturnBadRequest()
        {
            // Arrange
            const int id = 0;
            _repo.GetByKey(id).Returns((Company) null);

            // Act
            var ret = _companyController.DeleteConfirmed(id) as HttpStatusCodeResult;

            // Assert
            Assert.Equal((int)HttpStatusCode.BadRequest, ret.StatusCode);
        }

        [Fact]
        public void DeleteConfirmed_IdNotFound_ReturnNotFound()
        {
            // Arrange
            const int id = 1;
            _repo.GetByKey(id).Returns((Company)null);

            // Act
            var ret = _companyController.DeleteConfirmed(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void DeleteConfirmed_IdFoundCompanyDeleted_ReturnNotFound()
        {
            // Arrange
            const int id = 1;
            var company = new Company { IsDeleted = true };
            _repo.GetByKey(id).Returns(company);

            // Act
            var ret = _companyController.DeleteConfirmed(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }
        #endregion
    }
}
