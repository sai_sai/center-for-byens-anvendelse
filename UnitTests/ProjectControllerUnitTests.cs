﻿#region Head
// <copyright file="ProjectUnitTests.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System.Web;
using AutoMapper;
using Core.DomainModel;
using Core.DomainServices;
using NSubstitute;
using Presentation.Web.Controllers;
using Presentation.Web.Helpers;
using Presentation.Web.Models;
using Presentation.Web.Models.Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Web.Mvc;
using Xunit;

namespace UnitTests
{
    /// <summary>
    /// For unittesting of ProjectController
    /// </summary>
    public class ProjectControllerUnitTests
    {
        private readonly IGenericRepository<Project> _projectRepository;
        private readonly ProjectController _controller;
        private readonly IDocumentHelper _documentHelper;
        private readonly IDateTime _dateTime;
        private readonly UserMock _userMock;

        public ProjectControllerUnitTests()
        {
            var mapper = Substitute.For<IMappingEngine>();
            var createProjectViewModel = new CreateProjectViewModel();
            var indexProjectViewModel = new IndexProjectViewModel();
            var project = new Project();
            var unitOfWork = Substitute.For<IUnitOfWork>();

            var documentRepository = Substitute.For<IGenericRepository<Document>>();

            // fill with dummy data
            var documentList = new List<AgendaPointDocument>
            {
                new AgendaPointDocument
                {
                    FileName = "lala"
                }
            };

            documentRepository.Get(Arg.Any<Expression<Func<Document, bool>>>()).Returns(documentList);

            _documentHelper = Substitute.For<IDocumentHelper>();
            _dateTime = Substitute.For<IDateTime>();
            var geoCoder = Substitute.For<IGeoCoder>();
            var dawaRepo = Substitute.For<IDawaRepository>();

            _projectRepository = Substitute.For<IGenericRepository<Project>>();
            var projectDocumentRepository = Substitute.For<IGenericRepository<ProjectDocument>>();

            _userMock = new UserMock();
            _userMock.LogOn();
            _controller = new ProjectController(unitOfWork,
                _projectRepository,
                mapper,
                projectDocumentRepository,
                _documentHelper,
                _dateTime,
                geoCoder,
                dawaRepo);

            // setup mappings
            mapper.Map<CreateProjectViewModel>(Arg.Any<Project>())
                .Returns(createProjectViewModel);
            mapper.Map<Project>(Arg.Any<CreateProjectViewModel>())
                .Returns(project);
            mapper.Map<IndexProjectViewModel>(Arg.Any<Project>())
            .Returns(indexProjectViewModel);
            mapper.Map<Project>(Arg.Any<IndexProjectViewModel>())
                .Returns(project);
        }
        #region Index
        [Fact]
        public void GetAllProjects_CallingTheMethod_ReturnProjects()
        {
            // Arrange
            // See constructor

            // Act
            _controller.Index();

            // Assert
            _projectRepository.Received().Get();
        }
        #endregion

        #region GetCreate
        [Fact]
        public void GetCreateProject_NullArguments_ViewReturned()
        {
            // Arrange

            // Act
            var res = _controller.Create();

            // Assert
            Assert.IsType(typeof(ViewResult), res);
        }
        #endregion

        #region PostCreate
        [Fact]
        public void CreateProject_ViewModelObjectAsArg_ProjectCreated()
        {
            // Arrange
            var projectVm = new CreateProjectViewModel();

            // Act
            _controller.Create(projectVm);

            // Assert
            _projectRepository.Received().Insert(Arg.Any<Project>());
        }

        [Fact]
        public void CreateProject_InvalidModelState_ReturnsViewModel()
        {
            // Arrange
            _controller.ModelState.AddModelError("Error","Error");
            var vm = new CreateProjectViewModel();

            // Act
            var ret = _controller.Create(vm) as ViewResult;

            // Assert
            Assert.Equal(vm, ret.Model);
        }
        #endregion

        #region Details
        [Fact]
        public void GetDetails_Id1AsArg_ViewIsReturned()
        {
            // Arrange
            var id = 1;
            _projectRepository.GetByKey(id).Returns(new Project());

            // Act
            var res = _controller.Details(id);

            // Assert
            Assert.IsType(typeof(ViewResult), res);
        }

        [Fact]
        public void GetDetails_IdNullAsParam_ReturnsBadRequest()
        {
            // Arrange

            // Act
            var res = _controller.Details(null) as HttpStatusCodeResult;

            // Assert
            Assert.Equal((int)HttpStatusCode.BadRequest,res.StatusCode);
        }

        [Fact]
        public void GetDetails_IdNotFound_ReturnsNotFound()
        {
            // Arrange
            var id = 1;
            _projectRepository.GetByKey(id).Returns(null as Project);

            // Act
            var res = _controller.Details(id) as HttpStatusCodeResult;

            // Assert
            Assert.Equal((int)HttpStatusCode.NotFound, res.StatusCode);

        }
        #endregion

        #region GetEdit

        [Fact]
        public void GetEdit_ValidState_ViewModelReturned()
        {
            // Arrange
            const int id = 1;
            _projectRepository.GetByKey(id).Returns(new Project());

            // Act
            var res = _controller.Edit(id) as ViewResult;

            // Assert
            Assert.IsType<CreateProjectViewModel>(res.Model);
        }

        [Fact]
        public void GetEdit_IdNullAsParam_ReturnsBadRequest()
        {
            // Arrange

            // Act
            var res = _controller.Edit(null as int?) as HttpStatusCodeResult;

            // Assert
            Assert.Equal((int)HttpStatusCode.BadRequest, res.StatusCode);
        }

        [Fact]
        public void GetEdit_ProjectNotFound_ReturnsNotFound()
        {
            // Arrange
            int id = 1;
            _projectRepository.GetByKey(id).Returns(null as Project);

            // Act
            var res = _controller.Edit(id) as HttpStatusCodeResult;

            // Assert
            Assert.Equal((int)HttpStatusCode.NotFound, res.StatusCode);
        }

        #endregion

        #region PostEdit
        [Fact]
        public void EditProject_ViewModelObjectAsArg_UpdateReceivedOnRepoMock()
        {
            // Arrange
            var projectVm = new CreateProjectViewModel();
            _projectRepository.GetByKey(projectVm.Id).Returns(new Project());

            // Act
            _controller.Edit(projectVm);

            // Assert
            _projectRepository.Received().Update(Arg.Any<Project>());
        }

        [Fact]
        public void EditProject_InvalidModelState_ReturnsViewModel()
        {
            // Assert
            var vm = new CreateProjectViewModel();
            _controller.ModelState.AddModelError("Error","Error");

            // Act
            var res = _controller.Edit(vm) as ViewResult;

            // Assert
            Assert.Equal(vm, res.Model);
        }
        #endregion

        #region GetDelete

        [Fact]
        public void GetDelete_Id1AsArg_DeleteViewReturned()
        {
            // Arrange
            var id = 1;
            _projectRepository.GetByKey(id).Returns(new Project());

            // Act
            var res = _controller.Delete(id);

            // Assert
            Assert.IsType(typeof(ViewResult), res);
        }

        [Fact]
        public void GetDelete_IdNullAsParam_ReturnsBadRequest()
        {
            // Arrange

            // Act
            var res = _controller.Delete(null) as HttpStatusCodeResult;

            // Assert
            Assert.Equal((int)HttpStatusCode.BadRequest, res.StatusCode);
        }
        #endregion

        #region PostDelete
        [Fact]
        public void DeleteProject_Int2AsArg_DeleteReceivedOnRepoMock()
        {
            // Arrange
            // See constructor

            // Act
            _controller.DeleteConfirmed(2);

            // Assert
            _projectRepository.Received().DeleteByKey(2);
        }
        #endregion

        #region GetProjectsById
        [Fact]
        public void GetProjectsById_ArgumentNull_ReturnsHttpBadRequest()
        {
            // Arrange
            int? id = null;
            const int httpStatusCode = (int)HttpStatusCode.BadRequest;

            // Act
            var ret = _controller.GetProjectsById(id);
            var code = ret as HttpStatusCodeResult;
            // Assert
            Assert.IsType<HttpStatusCodeResult>(ret);
            Assert.Equal(httpStatusCode, code.StatusCode);
        }

        [Fact]
        public void GetProjectsById_ArgumentValidRepoReturns1Project_Return1ProjectDto()
        {
            // Arrange
            int? id = 1;
            var project = new Project { Id = 1, Topic = "somestring" };
            var projects = new List<Project> { project };
            _projectRepository.Get(Arg.Any<Expression<Func<Project, bool>>>()).Returns(projects);

            // Act
            var ret = _controller.GetProjectsById(id) as JsonResult;
            var data = ret.Data as IEnumerable<ProjectDto>;

            // Assert
            Assert.Equal(project.Id, data.First().Id);
            Assert.Equal(project.Topic, data.First().Name);
        }
        #endregion

        #region UploadFile

        [Fact]
        public void UploadFile_ValidState_UploadFileCalledInDocumentHelper()
        {
            // Arrange

            // Act
            _controller.SaveUploadedFile();

            // Assert
            _documentHelper.Received(1).UploadFile(Arg.Any<HttpRequestBase>(), Arg.Any<HttpServerUtilityBase>());
        }

        #endregion

        #region DownloadFile

        [Fact]
        public void DownloadFile_ValidState_DownloadCalledInDocumentHelper()
        {
            // Arrange
            var guid = new Guid();

            // Act
            _controller.Download(guid);

            // Assert
            _documentHelper.Received(1).Download(_controller.Server, guid);
        }

        #endregion

        #region RemoveFile

        [Fact]
        public void DeleteFile_ValidState_DeleteCalledInDocumentHelper()
        {
            // Arrange
            var guid = new Guid();
            var guidList = new List<Guid>
            {
                guid
            };

            // Act
            _controller.DeleteFilesFromServer(guidList);

            // Assert
            _documentHelper.Received(1).DeleteFiles(guidList, _controller.Server);
        }

        #endregion
    }
}
