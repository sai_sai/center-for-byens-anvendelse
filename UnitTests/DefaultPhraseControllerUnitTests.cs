﻿#region Head
// <copyright file="DefaultPhraseControllerUnitTests.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using Core.DomainModel;
using Core.DomainServices;
using NSubstitute;
using Presentation.Web.Controllers;
using Presentation.Web.Models.DefaultPhrase;
using Xunit;

namespace UnitTests
{
    public class DefaultPhraseControllerUnitTests
    {
        private readonly DefaultPhraseController _defaultPhraseController;
        private readonly IGenericRepository<DefaultPhrase> _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMappingEngine _mapper;

        public DefaultPhraseControllerUnitTests()
        {
            _repo = Substitute.For<IGenericRepository<DefaultPhrase>>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _mapper = Substitute.For<IMappingEngine>();

            new UserMock().LogOn(); // setting user mock
            _mapper.Map<DefaultPhrase>(Arg.Any<DefaultPhraseViewModel>())
                .Returns(new DefaultPhrase());
            _mapper.Map<DefaultPhrase>(Arg.Any<DefaultPhraseViewModel>())
                .Returns(new DefaultPhrase());

            _mapper.Map<DefaultPhraseViewModel>(Arg.Any<DefaultPhrase>())
                .Returns(new DefaultPhraseViewModel());

            _defaultPhraseController = new DefaultPhraseController(_repo, _unitOfWork, _mapper);
        }

        #region Index
        [Fact]
        public void Index_Call_ReturnsView()
        {
            // Arrange
            // Act
            var ret = _defaultPhraseController.Index();

            // Assert
            Assert.IsType(typeof(ViewResult), ret);
        }

        [Fact]
        public void Index_Call_ReturnsIndexDefaultPhraseViewModel()
        {
            // Arrange
            var defaultPhraseList = new List<DefaultPhrase>
            {
                new DefaultPhrase(),
                new DefaultPhrase()
            };
            _repo.Get().Returns(defaultPhraseList);

            // Act
            var ret = _defaultPhraseController.Index() as ViewResult;

            // Assert
            Assert.NotNull(ret);
            Assert.IsAssignableFrom<IEnumerable<DefaultPhraseViewModel>>(ret.Model);
        }

        [Fact]
        public void Index_Call_CallsDefaultPhrasesInRepo()
        {
            // Arrange
            // Act
            _defaultPhraseController.Index();

            // Assert
            _repo.Received(1).Get(Arg.Any<Expression<Func<DefaultPhrase, bool>>>());
        }
        #endregion

        #region Create Get
        [Fact]
        public void Create_Call_ReturnsView()
        {
            // Arrange
            // Act
            var ret = _defaultPhraseController.Create();

            // Assert
            Assert.IsType<ViewResult>(ret);
        }

        [Fact]
        public void Create_Call_ReturnsNoViewModel()
        {
            // Arrange
            // Act
            var ret = _defaultPhraseController.Create() as ViewResult;

            // Assert
            Assert.NotNull(ret);
            Assert.Null(ret.Model);
        }
        #endregion

        #region Create Post
        [Fact]
        public void CreatePost_ModelValid_RedirectAction()
        {
            // Arrange
            var vm = new DefaultPhraseViewModel();

            // Act
            var ret = _defaultPhraseController.Create(vm);

            // Assert
            Assert.IsType<RedirectToRouteResult>(ret);
        }

        [Fact]
        public void CreatePost_ModelNotvalid_ReturnView()
        {
            // Arrange
            var vm = new DefaultPhraseViewModel();
            _defaultPhraseController.ModelState.AddModelError("error", "error");

            // Act
            var ret = _defaultPhraseController.Create(vm) as ViewResult;

            // Assert
            Assert.NotNull(ret);
            Assert.IsType<DefaultPhraseViewModel>(ret.Model);
        }

        [Fact]
        public void CreatePost_ModelValid_CallCreateInRepo()
        {
            // Arrange
            var vm = new DefaultPhraseViewModel();

            // Act
            _defaultPhraseController.Create(vm);

            // Assert
            _repo.Received(1).Insert(Arg.Any<DefaultPhrase>());
            _unitOfWork.Received(1).Save();
        }
        #endregion

        #region Edit Get
        [Fact]
        public void Edit_IdValid_ReturnView()
        {
            // Arrange
            var defaultPhrase = new DefaultPhrase();
            int? id = 1;
            _repo.GetByKey(id).Returns(defaultPhrase);

            // Act
            var ret = _defaultPhraseController.Edit(id);

            // Assert
            Assert.IsAssignableFrom<ViewResult>(ret);
        }

        [Fact]
        public void Edit_IdValid_ReturnViewModel()
        {
            // Arrange
            var defaultPhrase = new DefaultPhrase();
            int? id = 1;
            _repo.GetByKey(id).Returns(defaultPhrase);

            // Act
            var ret = _defaultPhraseController.Edit(id) as ViewResult;

            // Assert
            Assert.IsType<DefaultPhraseViewModel>(ret.Model);
        }

        [Fact]
        public void Edit_IdNull_ReturnBadRequest()
        {
            // Arrange
            int? id = null;

            // Act
            var ret = _defaultPhraseController.Edit(id) as HttpStatusCodeResult;

            // Assert
            Assert.NotNull(ret);
            Assert.Equal((int)HttpStatusCode.BadRequest, ret.StatusCode);
        }

        [Fact]
        public void Edit_IdValid_CallGetByKeyInRepo()
        {
            // Arrange
            int? id = 1;

            // Act
            _defaultPhraseController.Edit(id);

            // Assert
            _repo.Received(1).GetByKey(id);
        }

        [Fact]
        public void Edit_IdValidDefaultPhraseNotFound_ReturnNotFound()
        {
            // Arrange
            int? id = 1;
            _repo.GetByKey(id).Returns((DefaultPhrase)null);

            // Act
            var ret = _defaultPhraseController.Edit(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void Edit_IdValidDefaultPhraseDeleted_ReturnNotFound()
        {
            // Arrange
            int? id = 1;
            var defaultPhrase = new DefaultPhrase { IsDeleted = true };
            _repo.GetByKey(id).Returns(defaultPhrase);

            // Act
            var ret = _defaultPhraseController.Edit(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }
        #endregion

        #region Edit Post
        [Fact]
        public void EditPost_ModelValid_RedirectAction()
        {
            // Arrange
            var vm = new DefaultPhraseViewModel();

            // Act
            var ret = _defaultPhraseController.Edit(vm);

            // Assert
            Assert.IsType<RedirectToRouteResult>(ret);
        }

        [Fact]
        public void EditPost_ModelNotValid_ReturnView()
        {
            // Arrange
            var vm = new DefaultPhraseViewModel();
            _defaultPhraseController.ModelState.AddModelError("error", "error");

            // Act
            var ret = _defaultPhraseController.Edit(vm) as ViewResult;

            // Assert
            Assert.NotNull(ret);
            Assert.IsType<DefaultPhraseViewModel>(ret.Model);
        }

        [Fact]
        public void EditPost_ModelValid_CallUpdateInRepo()
        {
            // Arrange
            var vm = new DefaultPhraseViewModel();

            // Act
            _defaultPhraseController.Edit(vm);

            // Assert
            _repo.Received(1).Update(Arg.Any<DefaultPhrase>());
            _unitOfWork.Received(1).Save();
        }
        #endregion

        #region Delete
        [Fact]
        public void Delete_IdValid_ReturnView()
        {
            // Arrange
            var defaultPhrase = new DefaultPhrase();
            int? id = 1;
            _repo.GetByKey(id).Returns(defaultPhrase);

            // Act
            var ret = _defaultPhraseController.Delete(id);

            // Assert
            Assert.IsType<ViewResult>(ret);
        }

        [Fact]
        public void Delete_IdValid_ReturnViewModel()
        {
            // Arrange
            var defaultPhrase = new DefaultPhrase();
            int? id = 1;
            _repo.GetByKey(id).Returns(defaultPhrase);

            // Act
            var ret = _defaultPhraseController.Delete(id) as ViewResult;

            // Assert
            Assert.IsType<DefaultPhraseViewModel>(ret.Model);
        }

        [Fact]
        public void Delete_IdValid_CallGetByKeyInRepo()
        {
            // Arrange
            var defaultPhrase = new DefaultPhrase();
            int? id = 1;
            _repo.GetByKey(id).Returns(defaultPhrase);

            // Act
            _defaultPhraseController.Delete(id);

            // Assert
            _repo.Received(1).GetByKey(id);
        }

        [Fact]
        public void Delete_IdValidDefaultPhraseNotFound_ReturnHttpNotFound()
        {
            // Arrange
            int? id = 1;
            _repo.GetByKey(id).Returns((DefaultPhrase)null);

            // Act
            var ret = _defaultPhraseController.Delete(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void Delete_IdValidDefaultPhraseDeleted_ReturnHttpNotFound()
        {
            // Arrange
            int? id = 1;
            var defaultPhrase = new DefaultPhrase { IsDeleted = true };
            _repo.GetByKey(id).Returns(defaultPhrase);

            // Act
            var ret = _defaultPhraseController.Delete(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void Delete_IdNull_ReturnBadRequest()
        {
            // Arrange
            int? id = null;

            // Act
            var ret = _defaultPhraseController.Delete(id) as HttpStatusCodeResult;

            // Assert
            _repo.DidNotReceive().GetByKey(Arg.Any<int?>());
            Assert.NotNull(ret);
            Assert.Equal((int)HttpStatusCode.BadRequest, ret.StatusCode);
        }
        #endregion

        #region DeleteConfirmed
        [Fact]
        public void DeleteConfirmed_IdValid_UpdateDeletedDefaultPhraseInRepo()
        {
            // Arrange
            const int id = 1;
            var defaultPhrase = new DefaultPhrase();
            _repo.GetByKey(id).Returns(defaultPhrase);

            // Act
            _defaultPhraseController.DeleteConfirmed(id);

            // Assert
            _repo.Received(1).Update(defaultPhrase);
            _unitOfWork.Received(1).Save();
            Assert.True(defaultPhrase.IsDeleted);
        }

        [Fact]
        public void DeleteConfirmed_IdValid_RedirectAction()
        {
            // Arrange
            const int id = 1;
            var defaultPhrase = new DefaultPhrase();
            _repo.GetByKey(id).Returns(defaultPhrase);

            // Act
            var ret = _defaultPhraseController.DeleteConfirmed(id);

            // Assert
            Assert.IsType<RedirectToRouteResult>(ret);
        }

        [Fact]
        public void DeleteConfirmed_IdValidDefaultPhraseDeleted_ReturnHttpNotFound()
        {
            // Arrange
            const int id = 1;
            var defaultPhrase = new DefaultPhrase { IsDeleted = true };
            _repo.GetByKey(id).Returns(defaultPhrase);

            // Act
            var ret = _defaultPhraseController.DeleteConfirmed(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void DeleteConfirmed_IdNotValid_ReturnBadRequest()
        {
            // Arrange
            const int id = 0;

            // Act
            var ret = _defaultPhraseController.DeleteConfirmed(id) as HttpStatusCodeResult;

            // Assert
            _repo.DidNotReceive().DeleteByKey(Arg.Any<int>());
            Assert.NotNull(ret);
            Assert.Equal((int)HttpStatusCode.BadRequest, ret.StatusCode);
        }
        #endregion
    }
}
