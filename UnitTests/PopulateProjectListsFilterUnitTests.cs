﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;
using NSubstitute;
using Presentation.Web.Filters;
using Xunit;

namespace UnitTests
{
    public class PopulateProjectListsFilterUnitTests
    {
        [Fact]
        public void SaveAdvisorListInViewBag_ListContaining1Element_OnActionExecutedCalled()
        {
            // Arrange
            var fakeController = Substitute.For<Controller>();
            var context = Substitute.For<ActionExecutingContext>();
            context.Controller = fakeController;

            var advisorsRepository = Substitute.For<IGenericRepository<Advisor>>();
            var advisor = new Advisor() {CompanyName = "EvilCorp", Id = 0};
            var advisors = new List<Advisor> { advisor };
            advisorsRepository.Get().Returns(advisors);
            var expectedListItem = new SelectListItem {Text = advisor.CompanyName, Value = advisor.Id.ToString()};
            var expectedList = new List<SelectListItem> { expectedListItem };
            var populateAdvisorsFilter = new PopulateAdvisorsFilter(advisorsRepository);

            // Act
            populateAdvisorsFilter.OnActionExecuting(context);

            // Assert
            var viewBagAdvisorList = fakeController.ViewBag.AdvisorList as List<SelectListItem>;

            var res = CheckIfListsAreEqual(expectedList, viewBagAdvisorList);
            Assert.True(res);
        }

        [Fact]
        public void SaveConstructionProgramListInViewBag_NoRepoData_OnActionExecutedCalled()
        {
            // Arrange
            var fakeController = Substitute.For<Controller>();
            var context = Substitute.For<ActionExecutingContext>();
            context.Controller = fakeController;

            var constructionRepo = Substitute.For<IGenericRepository<ConstructionProgram>>();
            var constructionProgram = new ConstructionProgram() { YearOfProjectInvolvement = 2000, Id = 0 };
            var constructionPrograms = new List<ConstructionProgram> { constructionProgram };
            constructionRepo.Get().Returns(constructionPrograms);
            var expectedListItem = new SelectListItem { Text = constructionProgram.YearOfProjectInvolvement.ToString(), Value = constructionProgram.Id.ToString() };
            var expectedList = new List<SelectListItem> { expectedListItem };
            var populateConstructionProgramsFilter = new PopulateConstructionProgramsFilter(constructionRepo);

            // Act
            populateConstructionProgramsFilter.OnActionExecuting(context);

            // Assert
            var viewBagData = fakeController.ViewBag.ConstructionProgramList as List<SelectListItem>;

            var res = CheckIfListsAreEqual(expectedList, viewBagData);
            Assert.True(res);
        }

        [Fact]
        public void SaveGroupListInViewBag_NoRepoData_OnActionExecutedCalled()
        {
            // Arrange
            var fakeController = Substitute.For<Controller>();
            var context = Substitute.For<ActionExecutingContext>();
            context.Controller = fakeController;

            var groupRepo = Substitute.For<IGenericRepository<Group>>();
            var @group = new Group() { Name = "EvilGroup", Id = 0 };
            var groups = new List<Group> { @group };
            groupRepo.Get().Returns(groups);
            var expectedListItem = new SelectListItem { Text = @group.Name, Value = @group.Id.ToString() };
            var expectedList = new List<SelectListItem> { expectedListItem };
            var populateGroupsFilter = new PopulateGroupsFilter(groupRepo);

            // Act
            populateGroupsFilter.OnActionExecuting(context);

            // Assert
            var viewBagData = fakeController.ViewBag.GroupList as List<SelectListItem>;

            var res = CheckIfListsAreEqual(expectedList, viewBagData);
            Assert.True(res);
        }

        [Fact]
        public void SaveLocalCommunityListInViewBag_NoRepoData_OnActionExecutedCalled()
        {
            // Arrange
            var fakeController = Substitute.For<Controller>();
            var context = Substitute.For<ActionExecutingContext>();
            context.Controller = fakeController;

            var localCommunityRepo = Substitute.For<IGenericRepository<LocalCommunity>>();
            var localCommunity = new LocalCommunity() { Name = "EvilGroup", Id = 0 };
            var localCommunities = new List<LocalCommunity> { localCommunity };
            localCommunityRepo.Get().Returns(localCommunities);
            var expectedListItem = new SelectListItem { Text = localCommunity.Name, Value = localCommunity.Id.ToString() };
            var expectedList = new List<SelectListItem> { expectedListItem };
            var populateGroupsFilter = new PopulateLocalCommunitiesFilter(localCommunityRepo);

            // Act
            populateGroupsFilter.OnActionExecuting(context);

            // Assert
            var viewBagData = fakeController.ViewBag.LocalCommunityList as List<SelectListItem>;

            var res = CheckIfListsAreEqual(expectedList, viewBagData);
            Assert.True(res);
        }

        [Fact]
        public void SaveProjectCategoryListInViewBag_NoRepoData_OnActionExecutedCalled()
        {
            // Arrange
            var fakeController = Substitute.For<Controller>();
            var context = Substitute.For<ActionExecutingContext>();
            context.Controller = fakeController;

            var projectCategoryRepo = Substitute.For<IGenericRepository<ProjectCategory>>();
            var projectCategory = new ProjectCategory() { Category = "EvilCat", Id = 0 };
            var projectCategories = new List<ProjectCategory> { projectCategory };
            projectCategoryRepo.Get().Returns(projectCategories);
            var expectedListItem = new SelectListItem { Text = projectCategory.Category, Value = projectCategory.Id.ToString() };
            var expectedList = new List<SelectListItem> { expectedListItem };
            var populateGroupsFilter = new PopulateProjectCategoriesFilter(projectCategoryRepo);

            // Act
            populateGroupsFilter.OnActionExecuting(context);

            // Assert
            var viewBagData = fakeController.ViewBag.ProjectCategoryList as List<SelectListItem>;

            var res = CheckIfListsAreEqual(expectedList, viewBagData);
            Assert.True(res);
        }

        bool CheckIfListsAreEqual(IEnumerable<SelectListItem> expecteEnumerable, IEnumerable<SelectListItem> actualEnumerable)
        {
            foreach (var selectListItem in actualEnumerable)
            {
                if (!expecteEnumerable.Any(o => o.Text == selectListItem.Text && o.Value == selectListItem.Value))
                {
                    return false;
                }
            }
            return true;
        }
    }
}
