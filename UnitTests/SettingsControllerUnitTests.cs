﻿using System.Web.Mvc;
using Presentation.Web.Controllers;
using Xunit;

namespace UnitTests
{
    public class SettingsControllerUnitTests
    {
        private readonly SettingsController _settingsController;
        public SettingsControllerUnitTests()
        {
            _settingsController = new SettingsController();
        }

        #region Index
        [Fact]
        public void Index_Call_ReturnsView()
        {
            // Arrange
            // Act
            var ret = _settingsController.Index();

            // Assert
            Assert.IsType<ViewResult>(ret);
        }
        #endregion
    }
}
