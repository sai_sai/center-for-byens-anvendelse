﻿#region Head
// <copyright file="AutoMapperUnitTests.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using AutoMapper;
using Core.DomainModel;
using Presentation.Web.App_Start;
using Presentation.Web.Models.Advisor;
using Presentation.Web.Models.Group;
using Presentation.Web.Models.Topic;
using Presentation.Web.Models.ProjectCategory;
using Presentation.Web.Models.Meeting;
using Xunit;

namespace UnitTests
{
    public class AutoMapperUnitTests
    {
        public AutoMapperUnitTests()
        {
            MappingConfig.Start();
        }

        #region Advisor
        [Fact]
        public void Map_AdvisorIndexAdvisorViewModel_PropertiesFit()
        {
            // Arrange
            var advisor = CreateAdvisor();

            // Act
            var vm = Mapper.Map<IndexAdvisorViewModel>(advisor);

            // Assert
            Assert.Equal(vm.Id, advisor.Id);
            Assert.Equal(vm.ResponsiblePersonName, advisor.ResponsiblePersonName);
        }

        [Fact]
        public void Map_AdvisorAddAdvisorViewModel_PropertiesFit()
        {
            // Arrange
            var advisor = CreateAdvisor();

            // Act
            var vm = Mapper.Map<CreateAdvisorViewModel>(advisor);

            // Assert
            Assert.Equal(vm.CompanyId, advisor.Company.Id);
            Assert.Equal(vm.ResponsiblePersonName, advisor.ResponsiblePersonName);
            Assert.Equal(vm.ResponsiblePersonPhoneNumber, advisor.ResponsiblePersonPhoneNumber);
        }

        [Fact]
        public void Map_AdvisorEditAdvisorViewModel_PropertiesFit()
        {
            // Arrange
            var advisor = CreateAdvisor();

            // Act
            var vm = Mapper.Map<EditAdvisorViewModel>(advisor);

            // Assert
            Assert.Equal(vm.Id, advisor.Id);
            Assert.Equal(vm.CompanyId, advisor.Company.Id);
            Assert.Equal(vm.ResponsiblePersonName, advisor.ResponsiblePersonName);
            Assert.Equal(vm.ResponsiblePersonPhoneNumber, advisor.ResponsiblePersonPhoneNumber);
        }

        [Fact]
        public void Map_AdvisorDeleteAdvisorViewModel_PropertiesFit()
        {
            // Arrange
            var advisor = CreateAdvisor();

            // Act
            var vm = Mapper.Map<DeleteAdvisorViewModel>(advisor);

            // Assert
            Assert.Equal(vm.CompanyName, advisor.Company.Name);
            Assert.Equal(vm.ResponsiblePersonName, advisor.ResponsiblePersonName);
            Assert.Equal(vm.ResponsiblePersonPhoneNumber, advisor.ResponsiblePersonPhoneNumber);
        }

        private static Advisor CreateAdvisor()
        {
            var company = new Company
            {
                Id = 0,
                Name = "CompanyName"
            };
            return new Advisor()
            {
                Id = 1,
                Company = company,
                ResponsiblePersonName = "ResponsiblePersonName",
                ResponsiblePersonPhoneNumber = "12345678"
            };
        }
        #endregion

        #region Group
        [Fact]
        public void Map_GroupIndexGroupViewModel_PropertiesFit()
        {
            // Arrange
            var group = CreateGroup();

            // Act
            var vm = Mapper.Map<IndexGroupViewModel>(group);

            // Assert
            Assert.Equal(vm.Id, group.Id);
            Assert.Equal(vm.Name, group.Name);
        }

        [Fact]
        public void Map_GroupAddGroupViewModel_PropertiesFit()
        {
            // Arrange
            var group = CreateGroup();

            // Act
            var vm = Mapper.Map<CreateGroupViewModel>(group);

            // Assert
            Assert.Equal(vm.Name, group.Name);
        }

        [Fact]
        public void Map_GroupEditGroupViewModel_PropertiesFit()
        {
            // Arrange
            var group = CreateGroup();

            // Act
            var vm = Mapper.Map<EditGroupViewModel>(group);

            // Assert
            Assert.Equal(vm.Id, group.Id);
            Assert.Equal(vm.Name, group.Name);
        }

        [Fact]
        public void Map_GroupDeleteGroupViewModel_PropertiesFit()
        {
            // Arrange
            var group = CreateGroup();

            // Act
            var vm = Mapper.Map<DeleteGroupViewModel>(group);

            // Assert
            Assert.Equal(vm.Name, group.Name);
        }

        private static Group CreateGroup()
        {
            return new Group()
            {
                Id = 1,
                Name = "GroupName"
            };
        }
        #endregion

        #region Meeting
        [Fact]
        public void Map_MeetingAgendaPointMeetingCreateAgendaPointViewModel_PropertiesFit()
        {
            // Arrange
            var agendaPoint = CreateAgendaPoint();

            // Act
            var vm = Mapper.Map<AgendaPointViewModel>(agendaPoint);

            // Assert
            Assert.Equal(agendaPoint.Id, vm.Id);
            Assert.Equal(agendaPoint.AddressId, vm.AddressId);
            Assert.Equal(agendaPoint.Description, vm.Description);
        }

        [Fact]
        public void Map_MeetingCreateMeetingViewModel_PropertiesFit()
        {
            // Arrange
            var meeting = CreateMeeting();

            // Act
            var vm = Mapper.Map<CreateMeetingViewModel>(meeting);

            // Assert
            Assert.Equal(meeting.Date, vm.Date);
            Assert.Equal(meeting.Location, vm.Location);
        }

        [Fact]
        public void Map_MeetingIndexMeetingViewModel_PropertiesFit()
        {
            // Arrange
            var meeting = CreateMeeting();

            // Act
            var vm = Mapper.Map<IndexMeetingViewModel>(meeting);

            // Assert
            Assert.Equal(meeting.Id, vm.Id);
            Assert.Equal(meeting.Date, vm.Date);
            Assert.Equal(meeting.Location, vm.Location);
            Assert.Equal(meeting.CreatedOn, vm.CreatedOn);
        }

        private static AgendaPoint CreateAgendaPoint()
        {
            return new AgendaPoint
            {
                Id = 1,
                AddressId = new Guid(),
                Description = "Description"
            };
        }

        private static Meeting CreateMeeting()
        {
            var dateTime = new DateTime(2015, 10, 1);

            return new Meeting
            {
                Id = 1,
                Date = dateTime.ToString(),
                Location = "AddressId",
                CreatedOn = dateTime
            };
        }
        #endregion

        #region ProjectCategory
        [Fact]
        public void Map_ProjectCategoryIndexProjectCategoryViewModel_PropertiesFit()
        {
            // Arrange
            var projectCategory = CreateProjectCategory();

            // Act
            var vm = Mapper.Map<IndexProjectCategoryViewModel>(projectCategory);

            // Assert
            Assert.Equal(vm.Id, projectCategory.Id);
            Assert.Equal(vm.Category, projectCategory.Category);
        }

        [Fact]
        public void Map_ProjectCategoryAddProjectCategoryViewModel_PropertiesFit()
        {
            // Arrange
            var projectCategory = CreateProjectCategory();

            // Act
            var vm = Mapper.Map<CreateProjectCategoryViewModel>(projectCategory);

            // Assert
            Assert.Equal(vm.Category, projectCategory.Category);
        }

        [Fact]
        public void Map_ProjectCategoryEditProjectCategoryViewModel_PropertiesFit()
        {
            // Arrange
            var projectCategory = CreateProjectCategory();

            // Act
            var vm = Mapper.Map<EditProjectCategoryViewModel>(projectCategory);

            // Assert
            Assert.Equal(vm.Id, projectCategory.Id);
            Assert.Equal(vm.Category, projectCategory.Category);
        }

        [Fact]
        public void Map_ProjectCategoryDeleteProjectCategoryViewModel_PropertiesFit()
        {
            // Arrange
            var projectCategory = CreateProjectCategory();

            // Act
            var vm = Mapper.Map<DeleteProjectCategoryViewModel>(projectCategory);

            // Assert
            Assert.Equal(vm.Category, projectCategory.Category);
        }

        private static ProjectCategory CreateProjectCategory()
        {
            return new ProjectCategory()
            {
                Id = 1,
                Category = "ProjectCategoryName"
            };
        }
        #endregion

        #region Topic
        [Fact]
        public void Map_TopicIndexTopicViewModel_PropertiesFit()
        {
            // Arrange
            var topic = CreateTopic();

            // Act
            var vm = Mapper.Map<IndexTopicViewModel>(topic);

            // Assert
            Assert.Equal(vm.Id, topic.Id);
            Assert.Equal(vm.Name, topic.Name);
        }

        [Fact]
        public void Map_TopicAddTopicViewModel_PropertiesFit()
        {
            // Arrange
            var topic = CreateTopic();

            // Act
            var vm = Mapper.Map<CreateTopicViewModel>(topic);

            // Assert
            Assert.Equal(vm.Name, topic.Name);
        }

        [Fact]
        public void Map_TopicEditTopicViewModel_PropertiesFit()
        {
            // Arrange
            var topic = CreateTopic();

            // Act
            var vm = Mapper.Map<EditTopicViewModel>(topic);

            // Assert
            Assert.Equal(vm.Id, topic.Id);
            Assert.Equal(vm.Name, topic.Name);
        }

        [Fact]
        public void Map_TopicDeleteTopicViewModel_PropertiesFit()
        {
            // Arrange
            var topic = CreateTopic();

            // Act
            var vm = Mapper.Map<DeleteTopicViewModel>(topic);

            // Assert
            Assert.Equal(vm.Name, topic.Name);
        }

        private static Topic CreateTopic()
        {
            return new Topic()
            {
                Id = 1,
                Name = "TopicName"
            };
        }
        #endregion

    }
}
