﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using Core.DomainModel;
using Core.DomainServices;
using NSubstitute;
using Presentation.Web.Controllers;
using Presentation.Web.Models.Group;
using Xunit;

namespace UnitTests
{
    public class GroupControllerUnitTests
    {
        private readonly GroupController _groupController;
        private readonly IGenericRepository<Group> _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMappingEngine _mapper;

        private readonly CreateGroupViewModel _createGroupViewModel;
        private readonly DeleteGroupViewModel _deleteGroupViewModel;
        private readonly EditGroupViewModel _editGroupViewModel;
        private readonly IndexGroupViewModel _indexGroupViewModel;

        public GroupControllerUnitTests()
        {
            _repo = Substitute.For<IGenericRepository<Group>>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _mapper = Substitute.For<IMappingEngine>();

            _createGroupViewModel = new CreateGroupViewModel();
            _deleteGroupViewModel = new DeleteGroupViewModel();
            _editGroupViewModel = new EditGroupViewModel();
            _indexGroupViewModel = new IndexGroupViewModel();

            new UserMock().LogOn(); // setting user mock
            _mapper.Map<Group>(Arg.Any<CreateGroupViewModel>())
                .Returns(new Group());
            _mapper.Map<Group>(Arg.Any<EditGroupViewModel>())
                .Returns(new Group());

            _mapper.Map<CreateGroupViewModel>(Arg.Any<Group>())
                .Returns(_createGroupViewModel);
            _mapper.Map<DeleteGroupViewModel>(Arg.Any<Group>())
                .Returns(_deleteGroupViewModel);
            _mapper.Map<EditGroupViewModel>(Arg.Any<Group>())
                .Returns(_editGroupViewModel);
            _mapper.Map<IndexGroupViewModel>(Arg.Any<Group>())
                .Returns(_indexGroupViewModel);

            _groupController = new GroupController(_repo, _unitOfWork, _mapper);
        }

        #region Index
        [Fact]
        public void Index_Call_ReturnsView()
        {
            // Arrange
            // Act
            var ret = _groupController.Index();

            // Assert
            Assert.IsType(typeof(ViewResult), ret);
        }

        [Fact]
        public void Index_Call_ReturnsIndexGroupViewModel()
        {
            // Arrange
            var groupList = new List<Group>
            {
                new Group(),
                new Group()
            };
            _repo.Get().Returns(groupList);

            // Act
            var ret = _groupController.Index() as ViewResult;

            // Assert
            Assert.NotNull(ret);
            Assert.IsAssignableFrom<IEnumerable<IndexGroupViewModel>>(ret.Model);
        }

        [Fact]
        public void Index_Call_CallsGroupsInRepo()
        {
            // Arrange
            // Act
            _groupController.Index();

            // Assert
            _repo.Received(1).Get(Arg.Any<Expression<Func<Group, bool>>>());
        }
        #endregion

        #region Create Get
        [Fact]
        public void Create_Call_ReturnsView()
        {
            // Arrange
            // Act
            var ret = _groupController.Create();

            // Assert
            Assert.IsType<ViewResult>(ret);
        }

        [Fact]
        public void Create_Call_ReturnsViewModel()
        {
            // Arrange
            // Act
            var ret = _groupController.Create() as ViewResult;

            // Assert
            Assert.NotNull(ret);
            Assert.NotNull(ret.Model);
        }
        #endregion

        #region Create Post
        [Fact]
        public void CreatePost_ModelValid_RedirectAction()
        {
            // Arrange
            var vm = new CreateGroupViewModel();

            // Act
            var ret = _groupController.Create(vm);

            // Assert
            Assert.IsType<RedirectToRouteResult>(ret);
        }

        [Fact]
        public void CreatePost_ModelNotvalid_ReturnView()
        {
            // Arrange
            var vm = new CreateGroupViewModel();
            _groupController.ModelState.AddModelError("error", "error");

            // Act
            var ret = _groupController.Create(vm) as ViewResult;

            // Assert
            Assert.NotNull(ret);
            Assert.IsType<CreateGroupViewModel>(ret.Model);
        }

        [Fact]
        public void CreatePost_ModelValid_CallCreateInRepo()
        {
            // Arrange
            var vm = new CreateGroupViewModel();

            // Act
            _groupController.Create(vm);

            // Assert
            _repo.Received(1).Insert(Arg.Any<Group>());
            _unitOfWork.Received(1).Save();
        }
        #endregion

        #region Edit Get
        [Fact]
        public void Edit_IdValid_ReturnView()
        {
            // Arrange
            var group = new Group();
            int? id = 1;
            _repo.GetByKey(id).Returns(group);

            // Act
            var ret = _groupController.Edit(id);

            // Assert
            Assert.IsAssignableFrom<ViewResult>(ret);
        }

        [Fact]
        public void Edit_IdValid_ReturnViewModel()
        {
            // Arrange
            var group = new Group();
            int? id = 1;
            _repo.GetByKey(id).Returns(group);

            // Act
            var ret = _groupController.Edit(id) as ViewResult;

            // Assert
            Assert.IsType<EditGroupViewModel>(ret.Model);
        }

        [Fact]
        public void Edit_IdNull_ReturnBadRequest()
        {
            // Arrange
            int? id = null;

            // Act
            var ret = _groupController.Edit(id) as HttpStatusCodeResult;

            // Assert
            Assert.NotNull(ret);
            Assert.Equal((int)HttpStatusCode.BadRequest, ret.StatusCode);
        }

        [Fact]
        public void Edit_IdValid_CallGetByKeyInRepo()
        {
            // Arrange
            int? id = 1;

            // Act
            _groupController.Edit(id);

            // Assert
            _repo.Received(1).GetByKey(id);
        }

        [Fact]
        public void Edit_IdValidGroupNotFound_ReturnNotFound()
        {
            // Arrange
            int? id = 1;
            _repo.GetByKey(id).Returns((Group)null);

            // Act
            var ret = _groupController.Edit(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void Edit_IdValidGroupDeleted_ReturnNotFound()
        {
            // Arrange
            int? id = 1;
            var group = new Group {IsDeleted = true};
            _repo.GetByKey(id).Returns(group);

            // Act
            var ret = _groupController.Edit(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }
        #endregion

        #region Edit Post
        [Fact]
        public void EditPost_ModelValid_RedirectAction()
        {
            // Arrange
            var vm = new EditGroupViewModel();

            // Act
            var ret = _groupController.Edit(vm);

            // Assert
            Assert.IsType<RedirectToRouteResult>(ret);
        }

        [Fact]
        public void EditPost_ModelNotValid_ReturnView()
        {
            // Arrange
            var vm = new EditGroupViewModel();
            _groupController.ModelState.AddModelError("error", "error");

            // Act
            var ret = _groupController.Edit(vm) as ViewResult;

            // Assert
            Assert.NotNull(ret);
            Assert.IsType<EditGroupViewModel>(ret.Model);
        }

        [Fact]
        public void EditPost_ModelValid_CallUpdateInRepo()
        {
            // Arrange
            var vm = new EditGroupViewModel();

            // Act
            _groupController.Edit(vm);

            // Assert
            _repo.Received(1).Update(Arg.Any<Group>());
            _unitOfWork.Received(1).Save();
        }
        #endregion

        #region Delete Get
        [Fact]
        public void Delete_IdValid_ReturnView()
        {
            // Arrange
            var group = new Group();
            int? id = 1;
            _repo.GetByKey(id).Returns(group);

            // Act
            var ret = _groupController.Delete(id);

            // Assert
            Assert.IsType<ViewResult>(ret);
        }

        [Fact]
        public void Delete_IdValid_ReturnViewModel()
        {
            // Arrange
            var group = new Group();
            int? id = 1;
            _repo.GetByKey(id).Returns(group);

            // Act
            var ret = _groupController.Delete(id) as ViewResult;

            // Assert
            Assert.IsType<DeleteGroupViewModel>(ret.Model);
        }

        [Fact]
        public void Delete_IdValid_CallGetByKeyInRepo()
        {
            // Arrange
            var group = new Group();
            int? id = 1;
            _repo.GetByKey(id).Returns(group);

            // Act
            _groupController.Delete(id);

            // Assert
            _repo.Received(1).GetByKey(id);
        }

        [Fact]
        public void Delete_IdValidGroupNotFound_ReturnHttpNotFound()
        {
            // Arrange
            int? id = 1;
            _repo.GetByKey(id).Returns((Group)null);

            // Act
            var ret = _groupController.Delete(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void Delete_IdValidGroupDeleted_ReturnHttpNotFound()
        {
            // Arrange
            int? id = 1;
            var group = new Group {IsDeleted = true};
            _repo.GetByKey(id).Returns(group);

            // Act
            var ret = _groupController.Delete(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void Delete_IdNull_ReturnBadRequest()
        {
            // Arrange
            int? id = null;

            // Act
            var ret = _groupController.Delete(id) as HttpStatusCodeResult;

            // Assert
            _repo.DidNotReceive().GetByKey(Arg.Any<int?>());
            Assert.NotNull(ret);
            Assert.Equal((int)HttpStatusCode.BadRequest, ret.StatusCode);
        }
        #endregion

        #region DeleteConfirmed
        [Fact]
        public void DeleteConfirmed_IdValid_UpdateDeletedGroupInRepo()
        {
            // Arrange
            const int id = 1;
            var group = new Group();
            _repo.GetByKey(id).Returns(group);

            // Act
            _groupController.DeleteConfirmed(id);

            // Assert
            _repo.Received(1).Update(group);
            _unitOfWork.Received(1).Save();
            Assert.True(group.IsDeleted);
        }

        [Fact]
        public void DeleteConfirmed_IdValid_RedirectAction()
        {
            // Arrange
            const int id = 1;
            var group = new Group();
            _repo.GetByKey(id).Returns(group);

            // Act
            var ret = _groupController.DeleteConfirmed(id);

            // Assert
            Assert.IsType<RedirectToRouteResult>(ret);
        }

        [Fact]
        public void DeleteConfirmed_IdValidGroupDeleted_ReturnHttpNotFound()
        {
            // Arrange
            const int id = 1;
            var group = new Group {IsDeleted = true};
            _repo.GetByKey(id).Returns(group);

            // Act
            var ret = _groupController.DeleteConfirmed(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void DeleteConfirmed_IdNotValid_ReturnBadRequest()
        {
            // Arrange
            const int id = 0;

            // Act
            var ret = _groupController.DeleteConfirmed(id) as HttpStatusCodeResult;

            // Assert
            _repo.DidNotReceive().DeleteByKey(Arg.Any<int>());
            Assert.NotNull(ret);
            Assert.Equal((int)HttpStatusCode.BadRequest, ret.StatusCode);
        }
        #endregion
    }
}
