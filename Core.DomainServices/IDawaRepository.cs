﻿#region Head
// <copyright file="IDawaRepository.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System;

namespace Core.DomainServices
{
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Net.Http;
    using System.Threading.Tasks;
    using DomainModel;

    /// <summary>
    /// Interface for "Danmarks Adressers Web API" (DAWA) endpoints from the AWS suite
    /// http://dawa.aws.dk/
    /// </summary>
    [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Dawa", Justification = "Abbreviation."),
    SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Abbreviation.")]
    public interface IDawaRepository
    {
        /// <summary>
        /// Request a single Address from the repository
        /// </summary>
        /// <param name="id">GUID of address to request</param>
        /// <returns>Requested address</returns>
        Address GetAddress(Guid id);

        /// <summary>
        /// Get adresses. See http://dawa.aws.dk/adressedok for options.
        /// </summary>
        /// <param name="query">Options: "q" for mixed search, "fuzzy" to enable fuzzy search.
        /// See http://dawa.aws.dk/adressedok for options.</param>
        /// <returns>List of found addresses</returns>
        /// <exception cref="HttpRequestException">Thrown when HTTP request times out.</exception>
        IEnumerable<Address> GetAddresses(string query);

        /// <summary>
        /// Get adresses async. See http://dawa.aws.dk/adressedok for options.
        /// </summary>
        /// <param name="query">Options: "q" for mixed search, "fuzzy" to enable fuzzy search.
        /// See http://dawa.aws.dk/adressedok for options.</param>
        /// <returns>List of found addresses</returns>
        /// <exception cref="HttpRequestException">Thrown when HTTP request times out.</exception>
        Task<IEnumerable<Address>> GetAddressesAsync(string query);
    }
}
