﻿namespace Core.DomainModel
{
    /// <summary>
    /// Represents default properties in CBA lists
    /// </summary>
    public abstract class CbaListProperties : Logger
    {
        public bool IsActive { get; set; }
    }
}
