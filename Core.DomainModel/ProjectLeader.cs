﻿#region Head
// <copyright file="ProjectLeader.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
namespace Core.DomainModel
{
    public class ProjectLeader : CbaListProperties, IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Initials { get; set; }
        public bool IsDeleted { get; set; }
    }
}
