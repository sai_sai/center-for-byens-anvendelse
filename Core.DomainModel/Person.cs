﻿namespace Core.DomainModel
{
    /// <summary>
    /// Represents a Person.
    /// </summary>
    public class Person : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
