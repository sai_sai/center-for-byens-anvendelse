﻿namespace Core.DomainModel
{
    public class AgendaPointDocument : Document
    {
        public int? AgendaPointId { get; set; }
        public virtual AgendaPoint AgendaPoint { get; set; }
    }
}
