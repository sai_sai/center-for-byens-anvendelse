﻿namespace Core.DomainModel
{
    /// <summary>
    /// Represents a status.
    /// </summary>
    public enum ProjectStatusType
    {
        Awaiting,
        Accepted,
        Rejected
    }
}
