﻿using System;

namespace Core.DomainModel
{
    /// <summary>
    /// Attached documents to <see cref="AgendaPoint"/> and <see cref="Project"/>.
    /// </summary>
    public abstract class Document
    {
        public Guid Guid { get; set; }
        public string FileName { get; set; }
        public float FileSize { get; set; }
        public string Comment { get; set; }
    }
}
