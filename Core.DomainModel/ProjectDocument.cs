namespace Core.DomainModel
{
    public class ProjectDocument : Document
    {
        public int? ProjectId { get; set; }
        public virtual Project Project { get; set; }
    }
}
