﻿#region Head
// <copyright file="Company.cs" company="IT Minds" year="2016">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System.Collections.Generic;

namespace Core.DomainModel
{
    public class Company : CbaListProperties, IEntity
    {
        public Company()
        {
            Advisors = new HashSet<Advisor>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
        public virtual ICollection<Advisor> Advisors { get; set; }
    }
}
