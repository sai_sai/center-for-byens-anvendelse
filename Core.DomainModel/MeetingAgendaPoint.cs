﻿namespace Core.DomainModel
{
    public class MeetingAgendaPoint : IEntity
    {
        public int Id { get; set; }
        public int MeetingId { get; set; }
        public int Number { get; set; }

        public virtual Meeting Meeting { get; set; }
        public virtual AgendaPoint AgendaPoint { get; set; }
    }
}
