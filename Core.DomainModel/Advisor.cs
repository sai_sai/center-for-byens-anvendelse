﻿#region Head
// <copyright file="Advisor.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
namespace Core.DomainModel
{
    /// <summary>
    /// Represents an advisor used in a <see cref="Project"/>. An Advisor is a company with a responsible person.
    /// </summary>
    public class Advisor : CbaListProperties, IEntity
    {
        public int Id { get; set; }
        public virtual Company Company { get; set; }
        public int CompanyId { get; set; }
        public string ResponsiblePersonName { get; set; }
        public string ResponsiblePersonPhoneNumber { get; set; }
        public bool IsDeleted { get; set; }
    }
}
