﻿namespace Core.DomainModel
{
    /// <summary>
    /// Represents a status.
    /// </summary>
    public enum AgendaPointStatusType
    {
        Draft,
        Ready,
    }
}
