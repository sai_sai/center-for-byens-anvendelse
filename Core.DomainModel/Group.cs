﻿#region Head
// <copyright file="Group.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Core.DomainModel
{
    /// <summary>
    /// Represents a geographic group.
    /// </summary>
    public class Group : CbaListProperties, IEntity
    {
        public Group()
        {
            Councils = new HashSet<Council>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
        public virtual ICollection<Council> Councils { get; set; }
    }
}
