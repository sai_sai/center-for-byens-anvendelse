﻿namespace Core.DomainModel
{
    /// <summary>
    /// Represents a type of street.
    /// </summary>
    public enum StreetType
    {
        PrivateStreet,
        PublicStreet,
        SharedStreet
    }
}
