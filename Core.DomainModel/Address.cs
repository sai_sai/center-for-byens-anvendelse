#region Head
// <copyright file="Address.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Core.DomainModel
{
    /// <summary>
    /// DAWA address result. See http://dawa.aws.dk/adressedok#adressedata for details.
    /// Only needed properties implemented.
    /// </summary>
    public class Address
    {
        private string _addressName;
        private string _streetName;
        private string _number;

        public Guid Id { get; set; }

        public string AddressName
        {
            get { return _addressName; }
            set
            {
                _addressName = value;
                UpdateComponents();
            }
        }

        /// <summary>
        /// Extract street name from address.
        /// </summary>
        public string StreetName { get { return _streetName; } }

        /// <summary>
        /// Extract house number from address.
        /// </summary>
        public string Number { get { return _number; } }

        /// <summary>
        /// Locate the first number within AddressName.
        /// </summary>
        /// <returns>Index of the first number within the string</returns>
        private int FirstNumberIndex(string fullAddress)
        {
            return fullAddress.IndexOfAny("0123456789".ToCharArray());
        }

        /// <summary>
        /// Get each component of the full address string.
        /// </summary>
        /// <returns>Array of components</returns>
        private void UpdateComponents()
        {
            // Truncate everything after the last.
            // This removes zipcode and city from the address.
            var lastComma = _addressName.LastIndexOf(',');
            _addressName = _addressName.Substring(0, lastComma);

            // At this point we know that everything up untill the
            // first digit is the street name.
            var digitAt = FirstNumberIndex(_addressName);
            _streetName = _addressName.Substring(0, digitAt);
            _number = _addressName.Substring(digitAt);
        }
    }
}
