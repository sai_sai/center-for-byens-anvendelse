#region Head
// <copyright file="Configuration.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System.Diagnostics.CodeAnalysis;

namespace Infrastructure.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    [SuppressMessage("Microsoft.Performance", "CA1812", Justification = "Internal ASP.NET MVC class")]
    internal sealed class Configuration : DbMigrationsConfiguration<CbaContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(CbaContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
