﻿#region Head
// <copyright file="CbaContext.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;
using System.Diagnostics.CodeAnalysis;
using Core.DomainModel;
using System;

namespace Infrastructure.DataAccess
{
    [SuppressMessage("Microsoft.Naming", "CA1704", Justification = "Cba is project name")]
    public class CbaContext : DbContext
    {
        public CbaContext()
            : base("DefaultConnection")
        {
        }

        public static CbaContext Create()
        {
            return new CbaContext();
        }

        // Define you conceptual model here. Entity Framework will include these types and all their references.

        public IDbSet<Meeting> Meetings { get; set; }
        public IDbSet<AgendaPoint> AgendaPoints { get; set; }
        public IDbSet<Project> Projects { get; set; }
        public IDbSet<Person> People { get; set; }
        public IDbSet<Document> Documents { get; set; }
        public IDbSet<Topic> Topics { get; set; }
        public IDbSet<ProjectCategory> ProjectCategories { get; set; }
        public IDbSet<Advisor> Advisors { get; set; }
        public IDbSet<ConstructionProgram> ConstructionPrograms { get; set; }
        public IDbSet<Group> Groups { get; set; }
        public IDbSet<InvitedPerson> InvitedPeople { get; set; }
        public IDbSet<LocalCommunity> LocalCommunities { get; set; }
        public IDbSet<DefaultPhrase> DefaultPhrases { get; set; }
        public IDbSet<ProjectLeader> ProjectLeaders { get; set; }
        public IDbSet<Council> Councils { get; set; }
        public IDbSet<MeetingAgendaPoint> MeetingAgendaPoint { get; set; }
        public IDbSet<Company> Companies { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Consider setting maxlength on string properties.
            // http://dba.stackexchange.com/questions/48408/ef-code-first-uses-nvarcharmax-for-all-strings-will-this-hurt-query-performan

            // The DateTime type in .NET has the same range and precision as datetime2 in SQL Server.
            // Configure DateTime type to use SQL server datetime2 instead.
            modelBuilder.Properties<DateTime>().Configure(c => c.HasColumnType("datetime2"));

            modelBuilder.Entity<Document>().HasKey(t => t.Guid);

            modelBuilder.Entity<MeetingAgendaPoint>()
                .HasRequired(o => o.AgendaPoint)
                .WithOptional(o => o.MeetingAgendaPoint);

            modelBuilder.Entity<AgendaPoint>()
                .HasOptional(t => t.Project)
                .WithMany();

            modelBuilder.Entity<AgendaPoint>()
                .Property(a => a.AddressId)
                .IsRequired();

            modelBuilder.Entity<AgendaPoint>()
                .Property(t => t.AgendaPointStatus)
                .HasColumnName("Status");

            modelBuilder.Entity<MeetingAgendaPoint>()
                .Property(o => o.Id)
                .HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                    new IndexAnnotation(
                        new IndexAttribute("IX_UniqueMeetingAgendaPoint", 1) {IsUnique = true}));

            modelBuilder.Entity<MeetingAgendaPoint>()
                .Property(o => o.MeetingId)
                .HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                    new IndexAnnotation(
                        new IndexAttribute("IX_UniqueMeetingAgendaPoint", 2) { IsUnique = true }));

            modelBuilder.Entity<MeetingAgendaPoint>()
                .Property(o => o.Number)
                .HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                    new IndexAnnotation(
                        new IndexAttribute("IX_UniqueMeetingAgendaPoint", 3) { IsUnique = true }));


            modelBuilder.Entity<Project>()
                .Property(t => t.ProjectStatus)
                .HasColumnName("Status");
            modelBuilder.Entity<Project>()
                .Property(o => o.AddressLocation)
                    .HasColumnName("SP_GEOMETRY");
            modelBuilder.Entity<AgendaPoint>()
                .Property(o => o.AddressLocation)
                    .HasColumnName("SP_GEOMETRY");

            base.OnModelCreating(modelBuilder);
        }
    }
}
